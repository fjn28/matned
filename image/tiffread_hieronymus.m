function stack = tiffread(file_name, indices, varargin)
% 
% tiffread, version 4.00 - July 11, 2016
%
% stack = tiffread;
% stack = tiffread(filename);
% stack = tiffread(filename, indices);
%
% Options can be specified: 'ReadUnknownTags' and 'DistributeMetaData', e.g.:
% stack = tiffread(filename, [], 'ReadUnknownTags',1);
% stack = tiffread(filename, [], 'DistributeMetaData', 1);
% 
% List of all options
% ReadUnknownTags
% DistributeMetaData
% ConsolidateStrips
% SimilarImages
% TIFRationalRaw
% NoCellIfSingle
% 
% 
% Reads uncompressed grayscale images of arbitrary BitsPerSample and 
% (some) color tiff files,
% as well as stacks or multiple tiff images, for example those produced
% by metamorph, Zeiss LSM or NIH-image.
%
% The function can be called with a file name in the current directory,
% or without argument, in which case it pops up a file opening dialog
% to allow for a manual selection of the file.
% If the stacks contains multiples images, reading can be restricted by
% specifying the indices of the desired images (eg. 1:5), or just one index (eg. 2).
%
% The returned value 'stack' is a cell struct containing the images
% and their meta-data. The length of the cell is the number of images.
% The image pixels values are stored in a field .data, which is a simple
% matrix for gray-scale images, or a 3D-matrix for color images.
%
% The pixels values are returned in their native (usually integer) format,
% and must be converted to be used in most matlab functions.
%
% Example:
% im = tiffread('spindle.stk');
% imshow( double(im{5}.data) );
%
%
%
% Only a fraction of the TIFF standard is supported, but you may extend support
% by modifying this file. If you do so, please return your modification to us,
% such that the added functionality can be redistributed to everyone.
%
% ------------------------------------------------------------------------------
%
% If you would like to acknowledge tiffread2.m in a publication,
% please cite the article for which the macro was first written:
%
% Dynamic Concentration of Motors in Microtubule Arrays
% Francois Nedelec, Thomas Surrey and A.C. Maggs
% Physical Review Letters 86: 3192-3195; 2001.
% DOI: 10.1103/PhysRevLett.86.3192
%
% Thank you!
%
%
% Francois Nedelec
% nedelec -at- embl.de
% Cell Biology and Biophysics, EMBL; Meyerhofstrasse 1; 69117 Heidelberg; Germany
% http://www.embl.org
% http://www.cytosim.org
%
%
% ------------------------------------------------------------------------------
%
% Copyright (C) 1999-2014 Francois Nedelec,
% with contributions from:
%   Kendra Burbank for the waitbar
%   Hidenao Iwai for the code to read floating point images,
%   Stephen Lang to be more compliant with PlanarConfiguration
%   Jan-Ulrich Kreft for Zeiss LSM support
%   Elias Beauchanp and David Kolin for additional Metamorph support
%   Jean-Pierre Ghobril for requesting that image indices may be specified
%   Urs Utzinger for the better handling of color images, and LSM meta-data
%   O. Scott Sands for support of GeoTIFF tags
%   Benjamin Bratton for Andor tags and more
%   Linqing Feng for fixing a bug reading IMG.tags.PlanarConfiguration == 2
%   Jan Hieronymus for support of BitsPerSample unequal to 8, 16, 32 (as present in some RAW image data)
%                  and support of SubIFDs (as present in DNG)
%                  importing many further tag names
%
%
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, version 3 of the License.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details:
% <http://www.gnu.org/licenses/>.
%
% ------------------------------------------------------------------------------
%%

% without argument, we ask the user to choose a file:
if nargin < 1  ||  isempty(file_name)
  [file_name, pathname] = uigetfile('*.tif;*.stk;*.lsm;*.dng', 'select image file');
  file_name = fullfile(pathname, file_name);
end

if ~ischar(file_name)
  error('The first argument should be a file name');
end

if nargin < 2  ||  isempty(indices)
  indices = 1:100000;
end

if ~isnumeric(indices)
  error('The second argument should be a list of indices');
end


%% Global Options

% global opt;

parser = inputParser;
parser.addParamValue('ReadUnknownTags',    1);
parser.addParamValue('DistributeMetaData', 1);
parser.addParamValue('ConsolidateStrips',  1);
parser.addParamValue('SimilarImages',      0);
parser.addParamValue('TIFRationalRaw',     1);
parser.addParamValue('NoCellIfSingle',     0);
parser.parse(varargin{:});

opt = parser.Results;

%% set defaults values :

% the structure IMG is returned to the user, while TIF is not.
% so tags useful to the user should be stored as fields in IMG, while
% those used only internally can be stored in TIF.
% the structure ANDOR has additional header information which is added to
% each plane of the image eventually


stack     = {}; % the variable to be output


% global TIF;
TIF = struct('ByteOrder', 'ieee-le');          %byte order string

% obtain the full file path:
[status, file_attrib] = fileattrib(file_name);

if status == 0
  error(['File "',file_name,'" not found.']);
end

file_name = file_attrib.Name;

% open file for reading:
TIF.file = fopen(file_name,'r','l');

% obtain the short file name:
[p, name, ext] = fileparts(file_name);
image_name = [name, ext];

% get the file size in bytes:
m = dir(file_name);
% global filesize;
filesize = m.bytes;


%% ---- read byte order: II = little endian, MM = big endian

bos = fread(TIF.file, 2, '*char');
if ( strcmp(bos', 'II') )
  TIF.ByteOrder = 'ieee-le';    % Intel little-endian format
elseif ( strcmp(bos','MM') )
  TIF.ByteOrder = 'ieee-be';
else
  error('This is not a TIFF file (no MM or II).');
end


%% ---- read in a number which identifies TIFF format

tiff_id = fread(TIF.file, 1,'uint16', TIF.ByteOrder);

if (tiff_id ~= 42)
  error('This is not a TIFF file (missing 42).');
end


%% ---- read the image file directories (IFDs)

img_indx  = 0;

ifd_pos   = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
ifd_queue = [ifd_pos];
ifd_already_read = [];

% while  ifd_pos ~= 0 % this version can only handle chain-like IFDs
while (~ isempty(ifd_queue)) % this version can also handle tree-like structures in DNG files
  % retrieve first element of the IFD queue
  ifd_pos = ifd_queue(1);
  ifd_queue(1) = []; % and remove it from the queue
  
  if (ifd_pos == 0) % ifd starts at position "0" means: there is no further IFD, stop parsing
    continue; % actually this could also be break, but if the queue is not set up properly, we might miss something
  end
  if (ismember(ifd_pos, ifd_already_read)) % that
    warning('something seems to be strange with the structure of the IFDs');
    continue; % in order to avoid reading an IFD more than once
  end
  
  img_indx = img_indx + 1;
  % img_indx
  
  % clear -global IMG;
  clear IMG;
  % global IMG;
  IMG.tags = struct();
  IMG.file_name = file_name;
  IMG.image_name = image_name;
  IMG.index = img_indx;
  % set some default values
  IMG.tags.SamplesPerPixel = 1;
  IMG.tags.PlanarConfiguration = 1;
  IMG.manage.BitsPerSample_unusualValue = false;

  % move in the file to the next IFD:
  file_seek(ifd_pos);

  % read in the number of IFD entries:
  num_entries = fread(TIF.file,1,'uint16', TIF.ByteOrder);
  % fprintf('num_entries = %i\n', num_entries);

  % store current position:
  entry_pos = ifd_pos+2;
  
  % read the next IFD address:
  file_seek( entry_pos + 12*num_entries);
  ifd_pos_next = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
  
  if (ifd_pos_next >= filesize) % recover from insane IFD-endings (missing 0000-Offset)
    ifd_pos_next = 0;
    warning('found pointer to IFD beyond end of file, probably the file is corrupt')
  end
  
  % avoid infinite loops, if the next IFD has already been read...
  if (ismember(ifd_pos_next, ifd_already_read))
    ifd_pos_next = [];
    warning('not going to queue this IFD again. it has already been read. high probability of corrupt file.')
  end
  
  ifd_queue = [ ifd_queue(:) ; ifd_pos_next ];

  % we want to read the first IFD, but not any other one,
  % unless demanded in the indices:
  if img_indx > 1
    if all( img_indx > indices )
      break;
    end
    % skip images that we do not have to read:
    if all( img_indx ~= indices )
      continue;
    end
  end
  
  %fprintf('reading IFD %i at position %i\n', img_indx, ifd_pos);
  
  do_not_read_img_data_of_cur_ifd = false;
  % read all the IFD entries
  for inx = 1:num_entries
    
    % move to next IFD entry in the file
    file_seek(entry_pos+12*(inx-1));
    
    % read entry tag
    entry_tag = fread(TIF.file, 1, 'uint16', TIF.ByteOrder);
    % read entry
    entry = readIFDentry(entry_tag);
    
    %fprintf('found entry with tag %i\n', entry_tag);
    
    
    % not all valid tiff tags have been included, but tags can easily be added to this code
    % See the official list of tags:
    % http://partners.adobe.com/asn/developer/pdfs/tn/TIFF6.pdf
    
    % the following line adds the current tag to IMG.tags
    known_tag = tiff_tag(entry_tag, entry.val);
    
    % do something special with the current tag
    switch entry_tag
      case 256         % image width = number of column
        IMG.width = entry.val;
      case 257         % image height = number of row
        IMG.height = entry.val;
      case 258
        IMG.manage.BytesPerSample = IMG.tags.BitsPerSample / 8;
        % IMG.bits = IMG.tags.BitsPerSample(1); % unused
        %fprintf('BitsPerSample %i %i %i\n', entry.val);
      case 259         % compression
        if ( entry.val ~= 1 )
          warning(['Compression format ', num2str(entry.val),' not supported.']);
          do_not_read_img_data_of_cur_ifd = true;
        end
      case 262         % photometric interpretation
        if ( IMG.tags.PhotometricInterpretation == 3 )
          %warning('tiffread:LookUp', 'Ignoring TIFF look-up table');
          fprintf(2, 'Ignoring TIFF look-up table in %s\n', image_name);
        end
      case 273         % strip offset
        IMG.manage.StripNumber = entry.cnt;
        %fprintf('StripNumber = %i, size(StripOffsets) = %i %i\n', IMG.manage.StripNumber, size(IMG.tags.StripOffsets));
      case 274
        % orientation is read, but the matrix is not rotated
        if ( 1 <= entry.val ) && ( entry.val <= 8 )
          keys = {'TopLeft', 'TopRight', 'BottomRight', 'BottomLeft', 'LeftTop', 'RightTop', 'RightBottom', 'LeftBottom'};
          IMG.orientation_text = keys{entry.val};
        end
      case 277
        %fprintf('Color image: sample_per_pixel=%i\n',  IMG.tags.SamplesPerPixel);
      case 317        %predictor for compression
        if (entry.val ~= 1);
          error('unsupported predictor value');
        end
      case 320         % color map
        IMG.colors              = entry.cnt/3;
      case 330 % for tree like IFD structures
        ifd_queue          = [ entry.val(:) ; ifd_queue(:) ]; % read tree childs before other elements in the queue
      
      % ANDOR tags
      case 4864
        if ~exist('ANDOR', 'var')
          %the existence of the variable indicates that we are
          %handling a Andor generated file
          ANDOR = struct([]);
        end
      case 4869       %ANDOR tag: temperature in Celsius when stabilized
        if ~(entry.val == -999)
          ANDOR.temperature = entry.val;
        end
      case 4876       %exposure time in seconds
        ANDOR.exposureTime   = entry.val;
      case 4878
        ANDOR.kineticCycleTime = entry.val;
      case 4879       %number of accumulations
        ANDOR.nAccumulations = entry.val;
      case 4881
        ANDOR.acquisitionCycleTime = entry.val;
      case 4882       %Readout time in seconds, 1/readoutrate
        ANDOR.readoutTime = entry.val;
      case 4884
        if (entry.val == 9)
          ANDOR.isPhotonCounting = 1;
        else
          ANDOR.isPhotonCounting = 0;
        end
      case 4885         %EM DAC level
        ANDOR.emDacLevel = entry.val;
      case 4890
        ANDOR.nFrames = entry.val;
      case 4896
        ANDOR.isFlippedHorizontally = entry.val;
      case 4897
        ANDOR.isFlippedVertically = entry.val;
      case 4898
        ANDOR.isRotatedClockwise = entry.val;
      case 4899
        ANDOR.isRotatedAnticlockwise = entry.val;
      case 4904
        ANDOR.verticalClockVoltageAmplitude = entry.val;
      case 4905
        ANDOR.verticalShiftSpeed = entry.val;
      case 4907
        ANDOR.preAmpSetting = entry.val;
      case 4908         %Camera Serial Number
        ANDOR.serialNumber = entry.val;
      case 4911       %Actual camera temperature when not equal to -999
        if ~(entry.val == -999)
          ANDOR.unstabilizedTemperature = entry.val;
        end
      case 4912
        ANDOR.isBaselineClamped = entry.val;
      case 4913
        ANDOR.nPrescans = entry.val;
      case 4914
        ANDOR.model = entry.val;
      case 4915
        ANDOR.chipXSize = entry.val;
      case 4916
        ANDOR.chipYSize  = entry.val;
      case 4944
        ANDOR.baselineOffset = entry.val;
      
      case 33628       % Metamorph specific data
        IMG.tags.UIC1Tag          = entry.val;
      case 33629       % this tag identify the image as a Metamorph stack!
        IMG.tags.MM_stack         = entry.val;
        IMG.tags.MM_stackCnt      = entry.cnt;
      case 33630       % Metamorph stack data: wavelength
        IMG.tags.MM_wavelength    = entry.val;
      case 33631       % Metamorph stack data: gain/background?
        IMG.tags.MM_private2      = entry.val;
      
      case 34412       % Zeiss LSM data
        LSM_info                  = entry.val;

      case 50838       % Adobe
        IMG.meta_data_byte_counts = entry.val;
      case 50839       % Adobe
        IMG.meta_data             = entry.val;
        
      otherwise
        if opt.ReadUnknownTags
          if (~known_tag)
            % NOP, already done in tiff_tags
            % IMG.tags.(['tag', num2str(entry_tag)])=entry.val;
            % % eval(['IMG.Tag',num2str(entry_tag),'=',entry.val,';']);
          end
        else
          fprintf( 'Unknown TIFF entry with tag %i (cnt %i)\n', entry_tag, entry.cnt);
        end
    end
    
    % calculate bounding box, if you've got the stuff
    if isfield(IMG.tags, 'ModelPixelScaleTag') && isfield(IMG.tags, 'ModelTiePointTag') && isfield(IMG, 'height') && isfield(IMG, 'width'),
      IMG.North = IMG.tags.ModelTiePointTag(5) - IMG.tags.ModelPixelScaleTag(2) * IMG.tags.ModelTiePointTag(2);
      IMG.South = IMG.North                    - IMG.height                     * IMG.tags.ModelPixelScaleTag(2);
      IMG.West  = IMG.tags.ModelTiePointTag(4) + IMG.tags.ModelPixelScaleTag(1) * IMG.tags.ModelTiePointTag(1);
      IMG.East  = IMG.West                     + IMG.width                      * IMG.tags.ModelPixelScaleTag(1);
    end
    
  end
  
  
  % do not read image if not demanded
  if ~isfield(IMG.tags, 'MM_stack')  &&  all( img_indx ~= indices )
    continue;
  end
  
  if (~do_not_read_img_data_of_cur_ifd)
    %total number of bytes per image:
    switch IMG.tags.PlanarConfiguration
      case 1
        BytesPerPlane = IMG.width * IMG.height * IMG.tags.SamplesPerPixel * IMG.manage.BytesPerSample ;
      case 2
        BytesPerPlane = IMG.width * IMG.height                            * IMG.manage.BytesPerSample ;
    end
    
    % try to consolidate the TIFF strips if possible
    if opt.ConsolidateStrips
      consolidate_strips(BytesPerPlane);
    end
  end
  
  % Read pixel data
  if isfield(IMG.tags, 'MM_stack')
    
    sel = ( indices <= IMG.tags.MM_stackCnt );
    indices = indices(sel);
    
    %this loop reads metamorph stacks:
    for ii = indices
      
      IMG.manage.StripCnt = 1;
      
      %read the image data
      IMG.data = read_pixels(BytesPerPlane * (ii-1));
      
      [ IMG.MM_stack, IMG.MM_wavelength, IMG.MM_private2 ] = splitMetamorph(ii);
      
      IMG.index = ii;
      % stack = cat(2, stack, IMG); % classic behaviour up to v3.31
      stack = {stack{:}, IMG};
      
    end

    break;
    
  else
    % Normal TIFF
    
    if opt.SimilarImages && ~isempty(stack)
      if IMG.width ~= stack{1}.width || IMG.height ~= stack{1}.height
        % setting read_img=0 will skip dissimilar images:
        % comment-out the line below to allow dissimilar stacks
        fprintf('Tiffread skipped %ix%i image\n', IMG.width, IMG.height);
        continue;
      end
    end
    
    IMG.manage.StripCnt = 1;
    
    %read image data
    if (~do_not_read_img_data_of_cur_ifd)
      IMG.data = read_pixels(zeros(size(BytesPerPlane)));
    else
      IMG.data = [];
    end
    
    
    try
      if (isempty(stack))
        stack = {IMG};
      else
        % stack = cat(2, stack, IMG); % classic behaviour up to v3.31
        stack = {stack{:}, IMG};
      end
    catch
      try
        stack = rmfield(stack, 'meta_data');
        stack = rmfield(stack, 'meta_data_byte_counts');
        % stack = cat(2, stack, IMG); % classic behaviour up to v3.31
        stack = {stack{:}, IMG};
        fprintf('Tiffread had to discard some meta_data\n');
      catch
        fprintf('Tiffread could not load dissimilar image %i\n', img_indx);
      end
    end
    
  end
  
  % remember, which IFD we have read so far
  ifd_already_read = [ ifd_already_read(:) ; ifd_pos ];
  
end


%clean-up

fclose(TIF.file);
TIF.file = -1;


% TODO check: flattening seems to have become obsolete due to the change in function read_pixels_matrix
% up to v3.31 remove the cell structure if there is always only one channel
if (false)
  flat = true;
  for ii = 1:length(stack)
    % if length(stack{ii}.data) ~= 1
    if size(stack{ii}.data,3) > 1
      flat = false;
      break;
    end
  end

  if flat
    for ii = 1:length(stack)
      % stack{ii}.data = stack{ii}.data{1};
      stack{ii}.data = squeeze(stack{ii}.data(:,:,1));
    end
  end
end

if (opt.NoCellIfSingle)
  % remove cell structure, if there is only one entry
  if (numel(stack) == 1)
    stack = stack{1};
  end
end

% distribute Andor header to all planes.

if opt.DistributeMetaData  &&  exist('ANDOR', 'var')
  
  fieldNames = fieldnames(ANDOR);
  for ii = 1:length(stack)
    for jj = 1:size(fieldNames,1)
      stack{ii}.(fieldNames{jj})=ANDOR.(fieldNames{jj});
    end
    stack{ii}.planeNumber = ii;
  end
  % set nFrames if it doesn't exist
  if ~ isfield(stack,'nFrames')
    nFrames = length(stack);
    stack = setfield(stack, {1}, 'nFrames', nFrames);
  end
  
end

% distribute the MetaMorph info

if opt.DistributeMetaData
  
  if isfield(IMG.tags, 'MM_stack') && isfield(IMG.tags, 'ImageDescription') && ~isempty(IMG.tags.ImageDescription)
    
    % if (false) % new in version 3.21,  textscan maybe does not work in octave, textscan seems to be part of the io package
    %   warning (['maybe the following code does not work properly in octave any longer, if you see this message, goto line' num2str(__LINE__)]  )  ;
    IMG.tags.ImageDescription = regexprep(IMG.tags.ImageDescription, '\r\n|\0', '\n');
    lines = textscan(IMG.tags.ImageDescription, '%s', 'Delimiter', '\n');
    mmi = parseMetamorphInfo(lines{1}, IMG.tags.MM_stackCnt);
    for ii = 1:length(stack)
      stack{ii}.MM = mmi{stack{ii}.index};
    end
    % end
    
  end
  
  % duplicate the LSM info
  if exist('LSM_info', 'var')
    for ii = 1:length(stack)
      stack{ii}.lsm = LSM_info;
    end
  end
  
end

return

%% Sub Function

  function file_seek(fpos)
    % global TIF;
    status = fseek(TIF.file, fpos, -1);
    
    if status == -1
      error('Invalid file offset(invalid fseek)');
    end
  end


  function consolidate_strips(BytesPerPlane)
    %Try to consolidate the strips into a single one to speed-up reading:
    nBytes = IMG.tags.StripByteCounts(1);
    
    if nBytes < BytesPerPlane
      
      idx = 1;
      % accumulate contiguous strips that contribute to the plane
      while IMG.tags.StripOffsets(1) + nBytes == IMG.tags.StripOffsets(idx+1)
        nBytes = nBytes + IMG.tags.StripByteCounts(idx+1);
        idx = idx + 1; % TODO check: should this line be before or after the break statement
        if ( nBytes >= BytesPerPlane );
          break;
        end
      end
      
      %Consolidate the Strips
      if ( nBytes <= BytesPerPlane(1) ) && ( idx > 1 )
        %fprintf('Consolidating %i stripes out of %i', ConsolidateCnt, IMG.manage.StripNumber);
        IMG.tags.StripByteCounts = [nBytes; IMG.tags.StripByteCounts(idx+1:IMG.manage.StripNumber ) ];
        IMG.tags.StripOffsets = IMG.tags.StripOffsets( [1 , idx+1:IMG.manage.StripNumber] );
        IMG.manage.StripNumber  = 1 + IMG.manage.StripNumber - idx;
      end
    end
  end


  function pixels = read_pixels(offsets) % this is just a dispatcher function that redirects to either the old cell version or the new matrix version
    pixels = read_pixels_matrix(offsets);
    % pixels = read_pixels_cell(offsets); % classic behaviour up to v3.31
  end
  
  function pixels = read_pixels_matrix(offsets)
    n_pix = IMG.width * IMG.height;
    bytes = read_plane(offsets(1), 1, IMG.tags.SamplesPerPixel*n_pix);
    pixels = zeros(IMG.height, IMG.width, IMG.tags.SamplesPerPixel, class(bytes));
    if IMG.tags.PlanarConfiguration == 2 % RRRGGGBBB
      for c = 1:IMG.tags.SamplesPerPixel
        pixels(:,:,c) = reshape(bytes((1+n_pix*(c-1)):(n_pix*c)), IMG.width, IMG.height)';
      end
    else % IMG.tags.PlanarConfiguration == 1 % RGBRGBRGB
      spp = IMG.tags.SamplesPerPixel;
      for c = 1:IMG.tags.SamplesPerPixel
        pixels(:,:,c) = reshape(bytes(c:spp:c+spp*(n_pix-1)), IMG.width, IMG.height)';
      end
    end
  end

  function pixels = read_pixels_cell(offsets)
    n_pix = IMG.width * IMG.height;
    bytes = read_plane(offsets(1), 1, IMG.tags.SamplesPerPixel*n_pix);
    pixels = cell(IMG.tags.SamplesPerPixel, 1);
    if IMG.tags.PlanarConfiguration == 2 % RRRGGGBBB
      for c = 1:IMG.tags.SamplesPerPixel
        pixels{c} = reshape(bytes((1+n_pix*(c-1)):(n_pix*c)), IMG.width, IMG.height)';
      end
    else % IMG.tags.PlanarConfiguration == 1 % RGBRGBRGB
      spp = IMG.tags.SamplesPerPixel;
      for c = 1:IMG.tags.SamplesPerPixel
        pixels{c} = reshape(bytes(c:spp:c+spp*(n_pix-1)), IMG.width, IMG.height)';
      end
    end
  end


  function bytes = read_plane(offset, plane_nb, bytes_nb) % plane_nb == 1 (due to calls)
    % TODO "bytes" should be renamed to something like raw_values, as they are not always bytes, but often words, 4-bytes, float, double
    %return an empty array if the sample format has zero bits
    if ( IMG.tags.BitsPerSample(plane_nb) == 0 )
      bytes=[];
      return;
    end
    
    % fprintf('reading plane %i size %i %i\n', plane_nb, width, height);
    
    % determine the type of data stored in the pixels:
    SampleFormat = 1;
    % if isfield(IMG.tags, 'SampleFormat')  &&  ( plane_nb < length(IMG.tags.SampleFormat) )
    if isfield(IMG.tags, 'SampleFormat')  &&  ( plane_nb <= length(IMG.tags.SampleFormat) ) % TODO check <= instead of < 
      SampleFormat = IMG.tags.SampleFormat(plane_nb);
    end
    
    switch( SampleFormat )
      case 1
        if ( any (IMG.tags.BitsPerSample(plane_nb) == [8 16 32 64]) )
          classname = sprintf('uint%i', IMG.tags.BitsPerSample(plane_nb));
        else
          IMG.manage.BitsPerSample_unusualValue = true;
          classname = sprintf('uint%i', 2^ceil(log2(IMG.tags.BitsPerSample(plane_nb))));
        end
      case 2
        classname = sprintf('int%i', IMG.tags.BitsPerSample(plane_nb));
      case 3
        if ( IMG.tags.BitsPerSample(plane_nb) == 32 )
          classname = 'single';
        else
          classname = 'double';
        end
      otherwise
        error('unsupported TIFF sample format %i', SampleFormat);
    end
    
    % Preallocate memory:
    try
      bytes = zeros(bytes_nb, 1, classname);
    catch
      %compatibility with older matlab versions:
      eval(['bytes = ', classname, '(zeros(bytes_nb, 1));']);
    end
    
    % consolidate all strips:
    pos = 0;
    while 1
      
      strip = read_next_strip(offset, plane_nb, classname);
      
      if isempty(strip)
        break
      end
      
      bytes( (1:length(strip)) + pos ) = strip;
      pos = pos + length(strip);
    
    end

    if pos < bytes_nb
      warning('tiffread: %s','found fewer bytes than needed');
    end
  end


  function strip = read_next_strip(offset, plane_nb, classname)
    % global TIF;
    % global IMG;
    
    if ( IMG.manage.StripCnt > IMG.manage.StripNumber )
      strip = [];
      return;
    end
    
    %fprintf('reading strip at position %i\n',IMG.tags.StripOffsets(IMG.manage.StripCnt) + offset);
    if ( ~ IMG.manage.BitsPerSample_unusualValue )
      %fprintf('reading strip at position %i\n',IMG.tags.StripOffsets(stripCnt) + offset);
      StripLength = IMG.tags.StripByteCounts(IMG.manage.StripCnt) ./ IMG.manage.BytesPerSample(plane_nb);
      readclassname = classname;
    else
      % read the whole strip as byte-array
      StripLength = IMG.tags.StripByteCounts(IMG.manage.StripCnt);
      readclassname = 'uint8';
    end
    
    
    
    %fprintf( 'reading strip %i\n', IMG.manage.StripCnt);
    file_seek(IMG.tags.StripOffsets(IMG.manage.StripCnt) + offset);
      
    % strip = fread(TIF.file, StripLength, classname, TIF.ByteOrder);
    rawStripData = fread( TIF.file, StripLength, ['*' readclassname], TIF.ByteOrder );
      
    if any( length(rawStripData) ~= StripLength )
      error('End of file reached unexpectedly.');
    end
    
    IMG.manage.StripCnt = IMG.manage.StripCnt + 1;
    if ( ~ IMG.manage.BitsPerSample_unusualValue )
      % strip = reshape(rawStripData, width, StripLength / width);
      strip = rawStripData;
    else
      NumBitsPerRow = uint32(IMG.width*IMG.tags.BitsPerSample(plane_nb));
      NumBytesPerRow = ceil(double(NumBitsPerRow)/8);
      NumBitsPerRowRaw = NumBytesPerRow*8;
      
      NumBitsPerContainer = 2^ceil(log2(IMG.tags.BitsPerSample(plane_nb)));
      NumPadBits = NumBitsPerContainer-IMG.tags.BitsPerSample(plane_nb);
      
      NumCols = IMG.tags.StripByteCounts(IMG.manage.StripCnt-1) ./ IMG.manage.BytesPerSample(plane_nb) ./ IMG.width;

      % Bits entpacken
      bitarr = bitunpack(rawStripData);
      % Byteweise darstellen (LeastSig Bits sind in den obere Zeilen)
      bitarr = reshape(bitarr, 8, StripLength);
      % LSB <-> MSB
      bitarr = flipud(bitarr);
      % jetzt sind MSB in den oberen Zeilen
      % Zeilenweise darstellen (transponiert!)
      bitarr = reshape(bitarr, NumBitsPerRowRaw, NumCols);
      % Endstaendige Padding-Bits abschneiden
      bitarr = bitarr([1:NumBitsPerRow],:);
      % Alle Zahlen einzeln darstellen
      bitarr = reshape(bitarr,IMG.tags.BitsPerSample(plane_nb), IMG.width*NumCols);
      % LSB wieder nach oben bringen
      bitarr = flipud(bitarr);
      % MSB-Padding Bits (unten) auffuellen, damit die Zahl in den Container passt
      bitarr = [ bitarr ; zeros(NumPadBits, IMG.width*NumCols, 'logical') ];
      RawGrayValues = bitpack(bitarr, classname);
      % strip = reshape( RawGrayValues, width, NumCols );
      strip =  RawGrayValues;
    end
    
  end


%% ==================sub-functions that reads an IFD entry:===================


  function [nbBytes, matlabType] = convertType(tiffType)
    switch (tiffType)
      case 1 %byte
        nbBytes=1;
        matlabType='uint8';
      case 2 %ascii string
        nbBytes=1;
        matlabType='uchar';
      case 3 % word
        nbBytes=2;
        matlabType='uint16';
      case 4 %dword/uword
        nbBytes=4;
        matlabType='uint32';
      case 5 % rational
        nbBytes=8;
        matlabType='uint32';
      case 7
        nbBytes=1;
        % matlabType='uchar';
        matlabType='uint8';
      case 8 % SSHORT
        nbBytes=2;
        matlabType='int16';
      case 9 % SLONG
        nbBytes=4;
        matlabType='int32';
      case 10 % SRATIONAL
        nbBytes=8;
        matlabType='int32';
      case 11
        nbBytes=4;
        matlabType='float32';
      case 12
        nbBytes=8;
        matlabType='float64';
      otherwise
        error('tiff type %i not supported', tiffType)
    end
  end

%% ==================sub-functions that reads an IFD entry:===================

  function  entry = readIFDentry(entry_tag)
    % global TIF;
    % global opt;
    % global filesize;
    entry.tiffType = fread(TIF.file, 1, 'uint16', TIF.ByteOrder);
    entry.cnt      = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    
    % disp(['tiffType =', num2str(entry.tiffType),', cnt = ',num2str(entry.cnt)]);
    
    [ entry.nbBytes, entry.matlabType ] = convertType(entry.tiffType);
    
    if entry.nbBytes * entry.cnt > 4
      %next field contains an offset:
      fpos = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
      %disp(strcat('offset = ', num2str(offset)));
      if (fpos < filesize)
        file_seek(fpos);
      else
        entry.val = NaN;
      end
    end
    
    
    if entry_tag == 33629   % metamorph stack plane specifications
      entry.val = fread(TIF.file, 6*entry.cnt, entry.matlabType, TIF.ByteOrder);
    elseif entry_tag == 34412  %TIF_CZ_LSMINFO
      entry.val = readLSMinfo;
    else % any other tag
      if ( (entry.tiffType == 5) || (entry.tiffType == 10) ) % rational tag types need to read twice the base datatype
        val = fread(TIF.file, 2*entry.cnt, entry.matlabType, TIF.ByteOrder);
        if (opt.TIFRationalRaw) % display raw-values for rationals, compatibility to TIFFREAD version 3.1
          entry.val = val;
        else
          entry.val = val(1:2:length(val)) ./ val(2:2:length(val));
        end
      else
        entry.val = fread(TIF.file, entry.cnt, entry.matlabType, TIF.ByteOrder);
      end
    end
    
    if ( entry.tiffType == 2 );
      entry.val = char(entry.val');
    end
    
  end


%% =============distribute the metamorph infos to each frame:

  function [MMstack, MMwavelength, MMprivate2] = splitMetamorph(imgCnt)
    
    % global IMG;
    
    MMstack = [];
    MMwavelength = [];
    MMprivate2 = [];
    
    if IMG.tags.MM_stackCnt == 1
      return;
    end
    
    left  = imgCnt - 1;
    
    if isfield( IMG.tags, 'MM_stack' )
      S = length(IMG.tags.MM_stack) / IMG.tags.MM_stackCnt;
      MMstack = IMG.tags.MM_stack(S*left+1:S*left+S);
    end
    
    if isfield( IMG.tags, 'MM_wavelength' )
      S = length(IMG.tags.MM_wavelength) / IMG.tags.MM_stackCnt;
      MMwavelength = IMG.tags.MM_wavelength(S*left+1:S*left+S);
    end
    
    if isfield( IMG.tags, 'MM_private2' )
      S = length(IMG.tags.MM_private2) / IMG.tags.MM_stackCnt;
      MMprivate2 = IMG.tags.MM_private2(S*left+1:S*left+S);
    end
    
  end


%% %%  Parse the Metamorph camera info tag into respective fields
% EVBR 2/7/2005, FJN Dec. 2007

  function mmi = parseMetamorphInfoLine(line, mmi)
    
    [tok, val] = strtok(line, ':');
    
    tok = regexprep(tok, ' ', '');
    val = strtrim(val(2:length(val)));
    
    if isempty(val)
      return;
    end
    
    %fprintf( '"%s" : "%s"\n', tok, val);
    
    if strcmp(tok, 'Exposure')
      [v, c, e, pos] = sscanf(val, '%i');
      unit = val(pos:length(val));
      %return the exposure in milli-seconds
      switch( unit )
        case 'ms'
          mmi.Exposure = v;
        case 's'
          mmi.Exposure = v * 1000;
        otherwise
          warning('tiffread:MetaMorphExposure', 'Exposure unit not recognized');
          mmi.Exposure = v;
      end
    else
      switch tok
        case 'Binning'
          % Binning: 1 x 1 -> [1 1]
          mmi.Binning = sscanf(val, '%d x %d')';
        case 'Region'
          mmi.Region = sscanf(val, '%d x %d, offset at (%d, %d)')';
        otherwise
          try
            if strcmp(val, 'Off')
              mmi.(tok) = 0;
              %eval(['mm.',tok,'=0;']);
            elseif strcmp(val, 'On')
              mmi.(tok) = 1;
              %eval(['mm.',tok,'=1;']);
            elseif isstrprop(val,'digit')
              mmi.(tok) = val;
              %eval(['mm.',tok,'=str2num(val)'';']);
            else
              mmi.(tok) = val;
              %eval(['mm.',tok,'=val;']);
            end
          catch
            warning('tiffread:MetaMorph', ['Invalid token "' tok '"']);
          end
      end
    end
  end


  function res = parseMetamorphInfo(lines, cnt)
    chk = length(lines) / cnt;
    res = cell(cnt, 1);
    length(lines)
    for n = 1:cnt
      mmi = [];
      for k = 1:chk
        mmi = parseMetamorphInfoLine(lines{chk*(n-1)+k}, mmi);
      end
      res{n} = mmi;
    end
  end


%% ==============partial-parse of LSM info:

  function R = readLSMinfo()
    
    % Read part of the LSM info table version 2
    % this provides only very partial information, since the offset indicate that
    % additional data is stored in the file
    
    % global TIF;
    
    R.MagicNumber          = sprintf('0x%09X',fread(TIF.file, 1, 'uint32', TIF.ByteOrder));
    S.StructureSize        = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.DimensionX           = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.DimensionY           = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.DimensionZ           = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.DimensionChannels    = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.DimensionTime        = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.IntensityDataType    = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.ThumbnailX           = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.ThumbnailY           = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.VoxelSizeX           = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
    R.VoxelSizeY           = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
    R.VoxelSizeZ           = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
    R.OriginX              = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
    R.OriginY              = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
    R.OriginZ              = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
    R.ScanType             = fread(TIF.file, 1, 'uint16', TIF.ByteOrder);
    R.SpectralScan         = fread(TIF.file, 1, 'uint16', TIF.ByteOrder);
    R.DataType             = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetVectorOverlay  = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetInputLut       = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetOutputLut      = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetChannelColors  = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    R.TimeInterval         = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
    S.OffsetChannelDataTypes = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetScanInformatio = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetKsData         = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetTimeStamps     = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetEventList      = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetRoi            = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetBleachRoi      = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    S.OffsetNextRecording  = fread(TIF.file, 1, 'uint32', TIF.ByteOrder);
    
    % There is more information stored in this table, which is skipped here
    
    %read real acquisition times:
    if ( S.OffsetTimeStamps > 0 )
      
      status =  fseek(TIF.file, S.OffsetTimeStamps, -1);
      if status == -1
        warning('tiffread:TimeStamps', 'Could not locate LSM TimeStamps');
        return;
      end
      
      StructureSize          = fread(TIF.file, 1, 'int32', TIF.ByteOrder);
      NumberTimeStamps       = fread(TIF.file, 1, 'int32', TIF.ByteOrder);
      for i=1:NumberTimeStamps
        R.TimeStamp(i)     = fread(TIF.file, 1, 'float64', TIF.ByteOrder);
      end
      
      %calculate elapsed time from first acquisition:
      R.TimeOffset = R.TimeStamp - R.TimeStamp(1);
      
    end
    
    % anything else assigned to S is discarded
    
  end

  function known_tag = tiff_tag(entry_tag, entry_val)
    % as found on 05.07.2016 on http://www.digitalpreservation.gov/formats/content/tiff_tags.shtml
    % global IMG;
    known_tag = true;
    switch entry_tag
      case 254 % hex2dec('00FE') % A general indication of the kind of data contained in this subfile.
        IMG.tags.NewSubfileType = entry_val;
        
      case 255 % hex2dec('00FF') % A general indication of the kind of data contained in this subfile.
        IMG.tags.SubfileType = entry_val;
        
      case 256 % hex2dec('0100') % The number of columns in the image, i.e., the number of pixels per row.
        IMG.tags.ImageWidth = entry_val;
        
      case 257 % hex2dec('0101') % The number of rows of pixels in the image.
        IMG.tags.ImageLength = entry_val;
        
      case 258 % hex2dec('0102') % Number of bits per component.
        IMG.tags.BitsPerSample = entry_val;
        
      case 259 % hex2dec('0103') % Compression scheme used on the image data.
        IMG.tags.Compression = entry_val;
        
      case 262 % hex2dec('0106') % The color space of the image data.
        IMG.tags.PhotometricInterpretation = entry_val;
        
      case 263 % hex2dec('0107') % For black and white TIFF files that represent shades of gray, the technique used to convert from gray to black and white pixels.
        IMG.tags.Threshholding = entry_val;
        
      case 264 % hex2dec('0108') % The width of the dithering or halftoning matrix used to create a dithered or halftoned bilevel file.
        IMG.tags.CellWidth = entry_val;
        
      case 265 % hex2dec('0109') % The length of the dithering or halftoning matrix used to create a dithered or halftoned bilevel file.
        IMG.tags.CellLength = entry_val;
        
      case 266 % hex2dec('010A') % The logical order of bits within a byte.
        IMG.tags.FillOrder = entry_val;
        
      case 269 % hex2dec('010D') % The name of the document from which this image was scanned.
        IMG.tags.DocumentName = entry_val;
        
      case 270 % hex2dec('010E') % A string that describes the subject of the image.
        IMG.tags.ImageDescription = entry_val;
        
      case 271 % hex2dec('010F') % The scanner manufacturer.
        IMG.tags.Make = entry_val;
        
      case 272 % hex2dec('0110') % The scanner model name or number.
        IMG.tags.Model = entry_val;
        
      case 273 % hex2dec('0111') % For each strip, the byte offset of that strip.
        IMG.tags.StripOffsets = entry_val;
        
      case 274 % hex2dec('0112') % The orientation of the image with respect to the rows and columns.
        IMG.tags.Orientation = entry_val;
        
      case 277 % hex2dec('0115') % The number of components per pixel.
        IMG.tags.SamplesPerPixel = entry_val;
        
      case 278 % hex2dec('0116') % The number of rows per strip.
        IMG.tags.RowsPerStrip = entry_val;
        
      case 279 % hex2dec('0117') % For each strip, the number of bytes in the strip after compression.
        IMG.tags.StripByteCounts = entry_val;
        
      case 280 % hex2dec('0118') % The minimum component value used.
        IMG.tags.MinSampleValue = entry_val;
        
      case 281 % hex2dec('0119') % The maximum component value used.
        IMG.tags.MaxSampleValue = entry_val;
        
      case 282 % hex2dec('011A') % The number of pixels per ResolutionUnit in the ImageWidth direction.
        IMG.tags.XResolution = entry_val;
        
      case 283 % hex2dec('011B') % The number of pixels per ResolutionUnit in the ImageLength direction.
        IMG.tags.YResolution = entry_val;
        
      case 284 % hex2dec('011C') % How the components of each pixel are stored.
        IMG.tags.PlanarConfiguration = entry_val;
        
      case 285 % hex2dec('011D') % The name of the page from which this image was scanned.
        IMG.tags.PageName = entry_val;
        
      case 286 % hex2dec('011E') % X position of the image.
        IMG.tags.XPosition = entry_val;
        
      case 287 % hex2dec('011F') % Y position of the image.
        IMG.tags.YPosition = entry_val;
        
      case 288 % hex2dec('0120') % For each string of contiguous unused bytes in a TIFF file, the byte offset of the string.
        IMG.tags.FreeOffsets = entry_val;
        
      case 289 % hex2dec('0121') % For each string of contiguous unused bytes in a TIFF file, the number of bytes in the string.
        IMG.tags.FreeByteCounts = entry_val;
        
      case 290 % hex2dec('0122') % The precision of the information contained in the GrayResponseCurve.
        IMG.tags.GrayResponseUnit = entry_val;
        
      case 291 % hex2dec('0123') % For grayscale data, the optical density of each possible pixel value.
        IMG.tags.GrayResponseCurve = entry_val;
        
      case 292 % hex2dec('0124') % Options for Group 3 Fax compression
        IMG.tags.T4Options = entry_val;
        
      case 293 % hex2dec('0125') % Options for Group 4 Fax compression
        IMG.tags.T6Options = entry_val;
        
      case 296 % hex2dec('0128') % The unit of measurement for XResolution and YResolution.
        IMG.tags.ResolutionUnit = entry_val;
        
      case 297 % hex2dec('0129') % The page number of the page from which this image was scanned.
        IMG.tags.PageNumber = entry_val;
        
      case 301 % hex2dec('012D') % Describes a transfer function for the image in tabular style.
        IMG.tags.TransferFunction = entry_val;
        
      case 305 % hex2dec('0131') % Name and version number of the software package(s) used to create the image.
        IMG.tags.Software = entry_val;
        
      case 306 % hex2dec('0132') % Date and time of image creation.
        IMG.tags.DateTime = entry_val;
        
      case 315 % hex2dec('013B') % Person who created the image.
        IMG.tags.Artist = entry_val;
        
      case 316 % hex2dec('013C') % The computer and/or operating system in use at the time of image creation.
        IMG.tags.HostComputer = entry_val;
        
      case 317 % hex2dec('013D') % A mathematical operator that is applied to the image data before an encoding scheme is applied.
        IMG.tags.Predictor = entry_val;
        
      case 318 % hex2dec('013E') % The chromaticity of the white point of the image.
        IMG.tags.WhitePoint = entry_val;
        
      case 319 % hex2dec('013F') % The chromaticities of the primaries of the image.
        IMG.tags.PrimaryChromaticities = entry_val;
        
      case 320 % hex2dec('0140') % A color map for palette color images.
        IMG.tags.ColorMap = entry_val;
        
      case 321 % hex2dec('0141') % Conveys to the halftone function the range of gray levels within a colorimetrically-specified image that should retain tonal detail.
        IMG.tags.HalftoneHints = entry_val;
        
      case 322 % hex2dec('0142') % The tile width in pixels. This is the number of columns in each tile.
        IMG.tags.TileWidth = entry_val;
        
      case 323 % hex2dec('0143') % The tile length (height) in pixels. This is the number of rows in each tile.
        IMG.tags.TileLength = entry_val;
        
      case 324 % hex2dec('0144') % For each tile, the byte offset of that tile, as compressed and stored on disk.
        IMG.tags.TileOffsets = entry_val;
        
      case 325 % hex2dec('0145') % For each tile, the number of (compressed) bytes in that tile.
        IMG.tags.TileByteCounts = entry_val;
        
      case 326 % hex2dec('0146') % Used in the TIFF-F standard, denotes the number of 'bad' scan lines encountered by the facsimile device.
        IMG.tags.BadFaxLines = entry_val;
        
      case 327 % hex2dec('0147') % Used in the TIFF-F standard, indicates if 'bad' lines encountered during reception are stored in the data, or if 'bad' lines have been replaced by the receiver.
        IMG.tags.CleanFaxData = entry_val;
        
      case 328 % hex2dec('0148') % Used in the TIFF-F standard, denotes the maximum number of consecutive 'bad' scanlines received.
        IMG.tags.ConsecutiveBadFaxLines = entry_val;
        
      case 330 % hex2dec('014A') % Offset to child IFDs.
        IMG.tags.SubIFDs = entry_val;
        
      case 332 % hex2dec('014C') % The set of inks used in a separated (PhotometricInterpretation=5) image.
        IMG.tags.InkSet = entry_val;
        
      case 333 % hex2dec('014D') % The name of each ink used in a separated image.
        IMG.tags.InkNames = entry_val;
        
      case 334 % hex2dec('014E') % The number of inks.
        IMG.tags.NumberOfInks = entry_val;
        
      case 336 % hex2dec('0150') % The component values that correspond to a 0% dot and 100% dot.
        IMG.tags.DotRange = entry_val;
        
      case 337 % hex2dec('0151') % A description of the printing environment for which this separation is intended.
        IMG.tags.TargetPrinter = entry_val;
        
      case 338 % hex2dec('0152') % Description of extra components.
        IMG.tags.ExtraSamples = entry_val;
        
      case 339 % hex2dec('0153') % Specifies how to interpret each data sample in a pixel.
        IMG.tags.SampleFormat = entry_val;
        
      case 340 % hex2dec('0154') % Specifies the minimum sample value.
        IMG.tags.SMinSampleValue = entry_val;
        
      case 341 % hex2dec('0155') % Specifies the maximum sample value.
        IMG.tags.SMaxSampleValue = entry_val;
        
      case 342 % hex2dec('0156') % Expands the range of the TransferFunction.
        IMG.tags.TransferRange = entry_val;
        
      case 343 % hex2dec('0157') % Mirrors the essentials of PostScript's path creation functionality.
        IMG.tags.ClipPath = entry_val;
        
      case 344 % hex2dec('0158') % The number of units that span the width of the image, in terms of integer ClipPath coordinates.
        IMG.tags.XClipPathUnits = entry_val;
        
      case 345 % hex2dec('0159') % The number of units that span the height of the image, in terms of integer ClipPath coordinates.
        IMG.tags.YClipPathUnits = entry_val;
        
      case 346 % hex2dec('015A') % Aims to broaden the support for indexed images to include support for any color space.
        IMG.tags.Indexed = entry_val;
        
      case 347 % hex2dec('015B') % JPEG quantization and/or Huffman tables.
        IMG.tags.JPEGTables = entry_val;
        
      case 351 % hex2dec('015F') % OPI-related.
        IMG.tags.OPIProxy = entry_val;
        
      case 400 % hex2dec('0190') % Used in the TIFF-FX standard to point to an IFD containing tags that are globally applicable to the complete TIFF file.
        IMG.tags.GlobalParametersIFD = entry_val;
        
      case 401 % hex2dec('0191') % Used in the TIFF-FX standard, denotes the type of data stored in this file or IFD.
        IMG.tags.ProfileType = entry_val;
        
      case 402 % hex2dec('0192') % Used in the TIFF-FX standard, denotes the 'profile' that applies to this file.
        IMG.tags.FaxProfile = entry_val;
        
      case 403 % hex2dec('0193') % Used in the TIFF-FX standard, indicates which coding methods are used in the file.
        IMG.tags.CodingMethods = entry_val;
        
      case 404 % hex2dec('0194') % Used in the TIFF-FX standard, denotes the year of the standard specified by the FaxProfile field.
        IMG.tags.VersionYear = entry_val;
        
      case 405 % hex2dec('0195') % Used in the TIFF-FX standard, denotes the mode of the standard specified by the FaxProfile field.
        IMG.tags.ModeNumber = entry_val;
        
      case 433 % hex2dec('01B1') % Used in the TIFF-F and TIFF-FX standards, holds information about the ITULAB (PhotometricInterpretation = 10) encoding.
        IMG.tags.Decode = entry_val;
        
      case 434 % hex2dec('01B2') % Defined in the Mixed Raster Content part of RFC 2301, is the default color needed in areas where no image is available.
        IMG.tags.DefaultImageColor = entry_val;
        
      case 512 % hex2dec('0200') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGProc = entry_val;
        
      case 513 % hex2dec('0201') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGInterchangeFormat = entry_val;
        
      case 514 % hex2dec('0202') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGInterchangeFormatLength = entry_val;
        
      case 515 % hex2dec('0203') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGRestartInterval = entry_val;
        
      case 517 % hex2dec('0205') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGLosslessPredictors = entry_val;
        
      case 518 % hex2dec('0206') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGPointTransforms = entry_val;
        
      case 519 % hex2dec('0207') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGQTables = entry_val;
        
      case 520 % hex2dec('0208') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGDCTables = entry_val;
        
      case 521 % hex2dec('0209') % Old-style JPEG compression field. TechNote2 invalidates this part of the specification.
        IMG.tags.JPEGACTables = entry_val;
        
      case 529 % hex2dec('0211') % The transformation from RGB to YCbCr image data.
        IMG.tags.YCbCrCoefficients = entry_val;
        
      case 530 % hex2dec('0212') % Specifies the subsampling factors used for the chrominance components of a YCbCr image.
        IMG.tags.YCbCrSubSampling = entry_val;
        
      case 531 % hex2dec('0213') % Specifies the positioning of subsampled chrominance components relative to luminance samples.
        IMG.tags.YCbCrPositioning = entry_val;
        
      case 532 % hex2dec('0214') % Specifies a pair of headroom and footroom image data values (codes) for each pixel component.
        IMG.tags.ReferenceBlackWhite = entry_val;
        
      case 559 % hex2dec('022F') % Defined in the Mixed Raster Content part of RFC 2301, used to replace RowsPerStrip for IFDs with variable-sized strips.
        IMG.tags.StripRowCounts = entry_val;
        
      case 700 % hex2dec('02BC') % XML packet containing XMP metadata
        IMG.tags.XMP = entry_val;
        
      case 18246 % hex2dec('4746') % Ratings tag used by Windows
        IMG.tags.Image.Rating = entry_val;
        
      case 18249 % hex2dec('4749') % Ratings tag used by Windows, value as percent
        IMG.tags.Image.RatingPercent = entry_val;
        
      case 32781 % hex2dec('800D') % OPI-related.
        IMG.tags.ImageID = entry_val;
        
      case 32932 % hex2dec('80A4') % Annotation data, as used in 'Imaging for Windows'.
        IMG.tags.Wang_Annotation = entry_val;
        
      case 33421 % hex2dec('828D') % For camera raw files from sensors with CFA overlay.
        IMG.tags.CFARepeatPatternDim = entry_val;
        
      case 33422 % hex2dec('828E') % For camera raw files from sensors with CFA overlay.
        IMG.tags.CFAPattern = entry_val;
        
      case 33423 % hex2dec('828F') % Encodes camera battery level at time of image capture.
        IMG.tags.BatteryLevel = entry_val;
        
      case 33432 % hex2dec('8298') % Copyright notice.
        IMG.tags.Copyright = entry_val;
        
      case 33434 % hex2dec('829A') % Exposure time, given in seconds.
        IMG.tags.ExposureTime = entry_val;
        
      case 33437 % hex2dec('829D') % The F number.
        IMG.tags.FNumber = entry_val;
        
      case 33445 % hex2dec('82A5') % Specifies the pixel data format encoding in the Molecular Dynamics GEL file format.
        IMG.tags.MD_FileTag = entry_val;
        
      case 33446 % hex2dec('82A6') % Specifies a scale factor in the Molecular Dynamics GEL file format.
        IMG.tags.MD_ScalePixel = entry_val;
        
      case 33447 % hex2dec('82A7') % Used to specify the conversion from 16bit to 8bit in the Molecular Dynamics GEL file format.
        IMG.tags.MD_ColorTable = entry_val;
        
      case 33448 % hex2dec('82A8') % Name of the lab that scanned this file, as used in the Molecular Dynamics GEL file format.
        IMG.tags.MD_LabName = entry_val;
        
      case 33449 % hex2dec('82A9') % Information about the sample, as used in the Molecular Dynamics GEL file format.
        IMG.tags.MD_SampleInfo = entry_val;
        
      case 33450 % hex2dec('82AA') % Date the sample was prepared, as used in the Molecular Dynamics GEL file format.
        IMG.tags.MD_PrepDate = entry_val;
        
      case 33451 % hex2dec('82AB') % Time the sample was prepared, as used in the Molecular Dynamics GEL file format.
        IMG.tags.MD_PrepTime = entry_val;
        
      case 33452 % hex2dec('82AC') % Units for data in this file, as used in the Molecular Dynamics GEL file format.
        IMG.tags.MD_FileUnits = entry_val;
        
      case 33550 % hex2dec('830E') % Used in interchangeable GeoTIFF_1_0 files.
        IMG.tags.ModelPixelScaleTag = entry_val;
        
      case 33723 % hex2dec('83BB') % IPTC-NAA (International Press Telecommunications Council-Newspaper Association of America) metadata.
        IMG.tags.IPTC_NAA = entry_val;
        
      case 33918 % hex2dec('847E') % Intergraph Application specific storage.
        IMG.tags.INGR_Packet_Data_Tag = entry_val;
        
      case 33919 % hex2dec('847F') % Intergraph Application specific flags.
        IMG.tags.INGR_Flag_Registers = entry_val;
        
      case 33920 % hex2dec('8480') % Originally part of Intergraph's GeoTIFF tags, but likely understood by IrasB only.
        IMG.tags.IrasB_Transformation_Matrix = entry_val;
        
      case 33922 % hex2dec('8482') % Originally part of Intergraph's GeoTIFF tags, but now used in interchangeable GeoTIFF_1_0 files.
        IMG.tags.ModelTiepointTag = entry_val;
        
      case 34016 % hex2dec('84E0') % Site where image created.
        IMG.tags.Site = entry_val;
        
      case 34017 % hex2dec('84E1') % Sequence of colors if other than CMYK.
        IMG.tags.ColorSequence = entry_val;
        
      case 34018 % hex2dec('84E2') % Certain inherited headers.
        IMG.tags.IT8Header = entry_val;
        
      case 34019 % hex2dec('84E3') % Type of raster padding used, if any.
        IMG.tags.RasterPadding = entry_val;
        
      case 34020 % hex2dec('84E4') % Number of bits for short run length encoding.
        IMG.tags.BitsPerRunLength = entry_val;
        
      case 34021 % hex2dec('84E5') % Number of bits for long run length encoding.
        IMG.tags.BitsPerExtendedRunLength = entry_val;
        
      case 34022 % hex2dec('84E6') % Color value in a color pallette.
        IMG.tags.ColorTable = entry_val;
        
      case 34023 % hex2dec('84E7') % Indicates if image (foreground) color or transparency is specified.
        IMG.tags.ImageColorIndicator = entry_val;
        
      case 34024 % hex2dec('84E8') % Background color specification.
        IMG.tags.BackgroundColorIndicator = entry_val;
        
      case 34025 % hex2dec('84E9') % Specifies image (foreground) color.
        IMG.tags.ImageColorValue = entry_val;
        
      case 34026 % hex2dec('84EA') % Specifies background color.
        IMG.tags.BackgroundColorValue = entry_val;
        
      case 34027 % hex2dec('84EB') % Specifies data values for 0 percent and 100 percent pixel intensity.
        IMG.tags.PixelIntensityRange = entry_val;
        
      case 34028 % hex2dec('84EC') % Specifies if transparency is used in HC file.
        IMG.tags.TransparencyIndicator = entry_val;
        
      case 34029 % hex2dec('84ED') % Specifies ASCII table or other reference per ISO 12641 and ISO 12642.
        IMG.tags.ColorCharacterization = entry_val;
        
      case 34030 % hex2dec('84EE') % Indicates the type of information in an HC file.
        IMG.tags.HCUsage = entry_val;
        
      case 34031 % hex2dec('84EF') % Indicates whether or not trapping has been applied to the file.
        IMG.tags.TrapIndicator = entry_val;
        
      case 34032 % hex2dec('84F0') % Specifies CMYK equivalent for specific separations.
        IMG.tags.CMYKEquivalent = entry_val;
        
      case 34033 % hex2dec('84F1') % For future TIFF/IT use
        IMG.tags.Reserved = entry_val;
        
      case 34034 % hex2dec('84F2') % For future TIFF/IT use
        IMG.tags.Reserved = entry_val;
        
      case 34035 % hex2dec('84F3') % For future TIFF/IT use
        IMG.tags.Reserved = entry_val;
        
      case 34264 % hex2dec('85D8') % Used in interchangeable GeoTIFF_1_0 files.
        IMG.tags.ModelTransformationTag = entry_val;
        
      case 34377 % hex2dec('8649') % Collection of Photoshop 'Image Resource Blocks'.
        IMG.tags.Photoshop = entry_val;
        
      case 34665 % hex2dec('8769') % A pointer to the Exif IFD.
        IMG.tags.Exif_IFD = entry_val;
        
      case 34675 % hex2dec('8773') % ICC profile data.
        IMG.tags.InterColorProfile = entry_val;
        
      case 34732 % hex2dec('87AC') % Defined in the Mixed Raster Content part of RFC 2301, used to denote the particular function of this Image in the mixed raster scheme.
        IMG.tags.ImageLayer = entry_val;
        
      case 34735 % hex2dec('87AF') % Used in interchangeable GeoTIFF_1_0 files.
        IMG.tags.GeoKeyDirectoryTag = entry_val;
        
      case 34736 % hex2dec('87B0') % Used in interchangeable GeoTIFF_1_0 files.
        IMG.tags.GeoDoubleParamsTag = entry_val;
        
      case 34737 % hex2dec('87B1') % Used in interchangeable GeoTIFF_1_0 files.
        IMG.tags.GeoAsciiParamsTag = entry_val;
        
      case 34850 % hex2dec('8822') % The class of the program used by the camera to set exposure when the picture is taken.
        IMG.tags.ExposureProgram = entry_val;
        
      case 34852 % hex2dec('8824') % Indicates the spectral sensitivity of each channel of the camera used.
        IMG.tags.SpectralSensitivity = entry_val;
        
      case 34853 % hex2dec('8825') % A pointer to the Exif-related GPS Info IFD.
        IMG.tags.GPSInfo = entry_val;
        
      case 34855 % hex2dec('8827') % Indicates the ISO Speed and ISO Latitude of the camera or input device as specified in ISO 12232.
        IMG.tags.ISOSpeedRatings = entry_val;
        
      case 34856 % hex2dec('8828') % Indicates the Opto-Electric Conversion Function (OECF) specified in ISO 14524.
        IMG.tags.OECF = entry_val;
        
      case 34857 % hex2dec('8829') % Indicates the field number of multifield images.
        IMG.tags.Interlace = entry_val;
        
      case 34858 % hex2dec('882A') % Encodes time zone of camera clock relative to GMT.
        IMG.tags.TimeZoneOffset = entry_val;
        
      case 34859 % hex2dec('882B') % Number of seconds image capture was delayed from button press.
        IMG.tags.SelfTimeMode = entry_val;
        
      case 34864 % hex2dec('8830') % The SensitivityType tag indicates PhotographicSensitivity tag, which one of the parameters of ISO 12232. Although it is an optional tag, it should be recorded when a PhotographicSensitivity tag is recorded. Value = 4, 5, 6, or 7 may be used in case that the values of plural parameters are the same.
        IMG.tags.SensitivityType = entry_val;
        
      case 34865 % hex2dec('8831') % This tag indicates the standard output sensitivity value of a camera or input device defined in ISO 12232. When recording this tag, the PhotographicSensitivity and SensitivityType tags shall also be recorded.
        IMG.tags.StandardOutputSensitivity = entry_val;
        
      case 34866 % hex2dec('8832') % This tag indicates the recommended exposure index value of a camera or input device defined in ISO 12232. When recording this tag, the PhotographicSensitivity and SensitivityType tags shall also be recorded.
        IMG.tags.RecommendedExposureIndex = entry_val;
        
      case 34867 % hex2dec('8833') % This tag indicates the ISO speed value of a camera or input device that is defined in ISO 12232. When recording this tag, the PhotographicSensitivity and SensitivityType tags shall also be recorded.
        IMG.tags.ISOSpeed = entry_val;
        
      case 34868 % hex2dec('8834') % This tag indicates the ISO speed latitude yyy value of a camera or input device that is defined in ISO 12232. However, this tag shall not be recorded without ISOSpeed and ISOSpeedLatitudezzz.
        IMG.tags.ISOSpeedLatitudeyyy = entry_val;
        
      case 34869 % hex2dec('8835') % This tag indicates the ISO speed latitude zzz value of a camera or input device that is defined in ISO 12232. However, this tag shall not be recorded without ISOSpeed and ISOSpeedLatitudeyyy.
        IMG.tags.ISOSpeedLatitudezzz = entry_val;
        
      case 34908 % hex2dec('885C') % Used by HylaFAX.
        IMG.tags.HylaFAX_FaxRecvParams = entry_val;
        
      case 34909 % hex2dec('885D') % Used by HylaFAX.
        IMG.tags.HylaFAX_FaxSubAddress = entry_val;
        
      case 34910 % hex2dec('885E') % Used by HylaFAX.
        IMG.tags.HylaFAX_FaxRecvTime = entry_val;
        
      case 36864 % hex2dec('9000') % The version of the supported Exif standard.
        IMG.tags.ExifVersion = entry_val;
        
      case 36867 % hex2dec('9003') % The date and time when the original image data was generated.
        IMG.tags.DateTimeOriginal = entry_val;
        
      case 36868 % hex2dec('9004') % The date and time when the image was stored as digital data.
        IMG.tags.DateTimeDigitized = entry_val;
        
      case 37121 % hex2dec('9101') % Specific to compressed data; specifies the channels and complements PhotometricInterpretation
        IMG.tags.ComponentsConfiguration = entry_val;
        
      case 37122 % hex2dec('9102') % Specific to compressed data; states the compressed bits per pixel.
        IMG.tags.CompressedBitsPerPixel = entry_val;
        
      case 37377 % hex2dec('9201') % Shutter speed.
        IMG.tags.ShutterSpeedValue = entry_val;
        
      case 37378 % hex2dec('9202') % The lens aperture.
        IMG.tags.ApertureValue = entry_val;
        
      case 37379 % hex2dec('9203') % The value of brightness.
        IMG.tags.BrightnessValue = entry_val;
        
      case 37380 % hex2dec('9204') % The exposure bias.
        IMG.tags.ExposureBiasValue = entry_val;
        
      case 37381 % hex2dec('9205') % The smallest F number of the lens.
        IMG.tags.MaxApertureValue = entry_val;
        
      case 37382 % hex2dec('9206') % The distance to the subject, given in meters.
        IMG.tags.SubjectDistance = entry_val;
        
      case 37383 % hex2dec('9207') % The metering mode.
        IMG.tags.MeteringMode = entry_val;
        
      case 37384 % hex2dec('9208') % The kind of light source.
        IMG.tags.LightSource = entry_val;
        
      case 37385 % hex2dec('9209') % Indicates the status of flash when the image was shot.
        IMG.tags.Flash = entry_val;
        
      case 37386 % hex2dec('920A') % The actual focal length of the lens, in mm.
        IMG.tags.FocalLength = entry_val;
        
      case 37387 % hex2dec('920B') % Amount of flash energy (BCPS).
        IMG.tags.FlashEnergy = entry_val;
        
      case 37388 % hex2dec('920C') % SFR of the camera.
        IMG.tags.SpatialFrequencyResponse = entry_val;
        
      case 37389 % hex2dec('920D') % Noise measurement values.
        IMG.tags.Noise = entry_val;
        
      case 37390 % hex2dec('920E') % Number of pixels per FocalPlaneResolutionUnit (37392) in ImageWidth direction for main image.
        IMG.tags.FocalPlaneXResolution = entry_val;
        
      case 37391 % hex2dec('920F') % Number of pixels per FocalPlaneResolutionUnit (37392) in ImageLength direction for main image.
        IMG.tags.FocalPlaneYResolution = entry_val;
        
      case 37392 % hex2dec('9210') % Unit of measurement for FocalPlaneXResolution(37390) and FocalPlaneYResolution(37391).
        IMG.tags.FocalPlaneResolutionUnit = entry_val;
        
      case 37393 % hex2dec('9211') % Number assigned to an image, e.g., in a chained image burst.
        IMG.tags.ImageNumber = entry_val;
        
      case 37394 % hex2dec('9212') % Security classification assigned to the image.
        IMG.tags.SecurityClassification = entry_val;
        
      case 37395 % hex2dec('9213') % Record of what has been done to the image.
        IMG.tags.ImageHistory = entry_val;
        
      case 37396 % hex2dec('9214') % Indicates the location and area of the main subject in the overall scene.
        IMG.tags.SubjectLocation = entry_val;
        
      case 37397 % hex2dec('9215') % Encodes the camera exposure index setting when image was captured.
        IMG.tags.ExposureIndex = entry_val;
        
      case 37398 % hex2dec('9216') % For current spec, tag value equals 1 0 0 0.
        IMG.tags.TIFF_EPStandardID = entry_val;
        
      case 37399 % hex2dec('9217') % Type of image sensor.
        IMG.tags.SensingMethod = entry_val;
        
      case 37500 % hex2dec('927C') % Manufacturer specific information.
        IMG.tags.MakerNote = entry_val;
        
      case 37510 % hex2dec('9286') % Keywords or comments on the image; complements ImageDescription.
        IMG.tags.UserComment = entry_val;
        
      case 37520 % hex2dec('9290') % A tag used to record fractions of seconds for the DateTime tag.
        IMG.tags.SubsecTime = entry_val;
        
      case 37521 % hex2dec('9291') % A tag used to record fractions of seconds for the DateTimeOriginal tag.
        IMG.tags.SubsecTimeOriginal = entry_val;
        
      case 37522 % hex2dec('9292') % A tag used to record fractions of seconds for the DateTimeDigitized tag.
        IMG.tags.SubsecTimeDigitized = entry_val;
        
      case 37724 % hex2dec('935C') % Used by Adobe Photoshop.
        IMG.tags.ImageSourceData = entry_val;
        
      case 40091 % hex2dec('9C9B') % Title tag used by Windows, encoded in UCS2
        IMG.tags.XPTitle = entry_val;
        
      case 40092 % hex2dec('9C9C') % Comment tag used by Windows, encoded in UCS2
        IMG.tags.XPComment = entry_val;
        
      case 40093 % hex2dec('9C9D') % Author tag used by Windows, encoded in UCS2
        IMG.tags.XPAuthor = entry_val;
        
      case 40094 % hex2dec('9C9E') % Keywords tag used by Windows, encoded in UCS2
        IMG.tags.XPKeywords = entry_val;
        
      case 40095 % hex2dec('9C9F') % Subject tag used by Windows, encoded in UCS2
        IMG.tags.XPSubject = entry_val;
        
      case 40960 % hex2dec('A000') % The Flashpix format version supported by a FPXR file.
        IMG.tags.FlashpixVersion = entry_val;
        
      case 40961 % hex2dec('A001') % The color space information tag is always recorded as the color space specifier.
        IMG.tags.ColorSpace = entry_val;
        
      case 40962 % hex2dec('A002') % Specific to compressed data; the valid width of the meaningful image.
        IMG.tags.PixelXDimension = entry_val;
        
      case 40963 % hex2dec('A003') % Specific to compressed data; the valid height of the meaningful image.
        IMG.tags.PixelYDimension = entry_val;
        
      case 40964 % hex2dec('A004') % Used to record the name of an audio file related to the image data.
        IMG.tags.RelatedSoundFile = entry_val;
        
      case 40965 % hex2dec('A005') % A pointer to the Exif-related Interoperability IFD.
        IMG.tags.Interoperability_IFD = entry_val;
        
      case 41483 % hex2dec('A20B') % Indicates the strobe energy at the time the image is captured, as measured in Beam Candle Power Seconds
        IMG.tags.FlashEnergy = entry_val;
        
      case 41484 % hex2dec('A20C') % Records the camera or input device spatial frequency table and SFR values in the direction of image width, image height, and diagonal direction, as specified in ISO 12233.
        IMG.tags.SpatialFrequencyResponse = entry_val;
        
      case 41486 % hex2dec('A20E') % Indicates the number of pixels in the image width (X) direction per FocalPlaneResolutionUnit on the camera focal plane.
        IMG.tags.FocalPlaneXResolution = entry_val;
        
      case 41487 % hex2dec('A20F') % Indicates the number of pixels in the image height (Y) direction per FocalPlaneResolutionUnit on the camera focal plane.
        IMG.tags.FocalPlaneYResolution = entry_val;
        
      case 41488 % hex2dec('A210') % Indicates the unit for measuring FocalPlaneXResolution and FocalPlaneYResolution.
        IMG.tags.FocalPlaneResolutionUnit = entry_val;
        
      case 41492 % hex2dec('A214') % Indicates the location of the main subject in the scene.
        IMG.tags.SubjectLocation = entry_val;
        
      case 41493 % hex2dec('A215') % Indicates the exposure index selected on the camera or input device at the time the image is captured.
        IMG.tags.ExposureIndex = entry_val;
        
      case 41495 % hex2dec('A217') % Indicates the image sensor type on the camera or input device.
        IMG.tags.SensingMethod = entry_val;
        
      case 41728 % hex2dec('A300') % Indicates the image source.
        IMG.tags.FileSource = entry_val;
        
      case 41729 % hex2dec('A301') % Indicates the type of scene.
        IMG.tags.SceneType = entry_val;
        
      case 41730 % hex2dec('A302') % Indicates the color filter array (CFA) geometric pattern of the image sensor when a one-chip color area sensor is used.
        IMG.tags.CFAPattern = entry_val;
        
      case 41985 % hex2dec('A401') % Indicates the use of special processing on image data, such as rendering geared to output.
        IMG.tags.CustomRendered = entry_val;
        
      case 41986 % hex2dec('A402') % Indicates the exposure mode set when the image was shot.
        IMG.tags.ExposureMode = entry_val;
        
      case 41987 % hex2dec('A403') % Indicates the white balance mode set when the image was shot.
        IMG.tags.WhiteBalance = entry_val;
        
      case 41988 % hex2dec('A404') % Indicates the digital zoom ratio when the image was shot.
        IMG.tags.DigitalZoomRatio = entry_val;
        
      case 41989 % hex2dec('A405') % Indicates the equivalent focal length assuming a 35mm film camera, in mm.
        IMG.tags.FocalLengthIn35mmFilm = entry_val;
        
      case 41990 % hex2dec('A406') % Indicates the type of scene that was shot.
        IMG.tags.SceneCaptureType = entry_val;
        
      case 41991 % hex2dec('A407') % Indicates the degree of overall image gain adjustment.
        IMG.tags.GainControl = entry_val;
        
      case 41992 % hex2dec('A408') % Indicates the direction of contrast processing applied by the camera when the image was shot.
        IMG.tags.Contrast = entry_val;
        
      case 41993 % hex2dec('A409') % Indicates the direction of saturation processing applied by the camera when the image was shot.
        IMG.tags.Saturation = entry_val;
        
      case 41994 % hex2dec('A40A') % Indicates the direction of sharpness processing applied by the camera when the image was shot.
        IMG.tags.Sharpness = entry_val;
        
      case 41995 % hex2dec('A40B') % This tag indicates information on the picture-taking conditions of a particular camera model.
        IMG.tags.DeviceSettingDescription = entry_val;
        
      case 41996 % hex2dec('A40C') % Indicates the distance to the subject.
        IMG.tags.SubjectDistanceRange = entry_val;
        
      case 42016 % hex2dec('A420') % Indicates an identifier assigned uniquely to each image.
        IMG.tags.ImageUniqueID = entry_val;
        
      case 42032 % hex2dec('A430') % Camera owner name as ASCII string.
        IMG.tags.CameraOwnerName = entry_val;
        
      case 42033 % hex2dec('A431') % Camera body serial number as ASCII string.
        IMG.tags.BodySerialNumber = entry_val;
        
      case 42034 % hex2dec('A432') % This tag notes minimum focal length, maximum focal length, minimum F number in the minimum focal length, and minimum F number in the maximum focal length, which are specification information for the lens that was used in photography. When the minimum F number is unknown, the notation is 0/0.
        IMG.tags.LensSpecification = entry_val;
        
      case 42035 % hex2dec('A433') % Lens manufacturer name as ASCII string.
        IMG.tags.LensMake = entry_val;
        
      case 42036 % hex2dec('A434') % Lens model name and number as ASCII string.
        IMG.tags.LensModel = entry_val;
        
      case 42037 % hex2dec('A435') % Lens serial number as ASCII string.
        IMG.tags.LensSerialNumber = entry_val;
        
      case 42112 % hex2dec('A480') % Used by the GDAL library, holds an XML list of name=value 'metadata' values about the image as a whole, and about specific samples.
        IMG.tags.GDAL_METADATA = entry_val;
        
      case 42113 % hex2dec('A481') % Used by the GDAL library, contains an ASCII encoded nodata or background pixel value.
        IMG.tags.GDAL_NODATA = entry_val;
        
      case 48129 % hex2dec('BC01') % A 128-bit Globally Unique Identifier (GUID) that identifies the image pixel format.
        IMG.tags.PixelFormat = entry_val;
        
      case 48130 % hex2dec('BC02') % Specifies the transformation to be applied when decoding the image to present the desired representation.
        IMG.tags.Transformation = entry_val;
        
      case 48131 % hex2dec('BC03') % Specifies that image data is uncompressed.
        IMG.tags.Uncompressed = entry_val;
        
      case 48132 % hex2dec('BC04') % Specifies the image type of each individual frame in a multi-frame file.
        IMG.tags.ImageType = entry_val;
        
      case 48256 % hex2dec('BC80') % Specifies the number of columns in the transformed photo, or the number of pixels per scan line.
        IMG.tags.ImageWidth = entry_val;
        
      case 48257 % hex2dec('BC81') % Specifies the number of pixels or scan lines in the transformed photo.
        IMG.tags.ImageHeight = entry_val;
        
      case 48258 % hex2dec('BC82') % Specifies the horizontal resolution of a transformed image expressed in pixels per inch.
        IMG.tags.WidthResolution = entry_val;
        
      case 48259 % hex2dec('BC83') % Specifies the vertical resolution of a transformed image expressed in pixels per inch.
        IMG.tags.HeightResolution = entry_val;
        
      case 48320 % hex2dec('BCC0') % Specifies the byte offset pointer to the beginning of the photo data, relative to the beginning of the file.
        IMG.tags.ImageOffset = entry_val;
        
      case 48321 % hex2dec('BCC1') % Specifies the size of the photo in bytes.
        IMG.tags.ImageByteCount = entry_val;
        
      case 48322 % hex2dec('BCC2') % Specifies the byte offset pointer the beginning of the planar alpha channel data, relative to the beginning of the file.
        IMG.tags.AlphaOffset = entry_val;
        
      case 48323 % hex2dec('BCC3') % Specifies the size of the alpha channel data in bytes.
        IMG.tags.AlphaByteCount = entry_val;
        
      case 48324 % hex2dec('BCC4') % Signifies the level of data that has been discarded from the image as a result of a compressed domain transcode to reduce the file size.
        IMG.tags.ImageDataDiscard = entry_val;
        
      case 48325 % hex2dec('BCC5') % Signifies the level of data that has been discarded from the planar alpha channel as a result of a compressed domain transcode to reduce the file size.
        IMG.tags.AlphaDataDiscard = entry_val;
        
      case 48132 % hex2dec('BC04') % Specifies the image type of each individual frame in a multi-frame file.
        IMG.tags.ImageType = entry_val;
        
      case 50215 % hex2dec('C427') % Used in the Oce scanning process.
        IMG.tags.Oce_Scanjob_Description = entry_val;
        
      case 50216 % hex2dec('C428') % Used in the Oce scanning process.
        IMG.tags.Oce_Application_Selector = entry_val;
        
      case 50217 % hex2dec('C429') % Used in the Oce scanning process.
        IMG.tags.Oce_Identification_Number = entry_val;
        
      case 50218 % hex2dec('C42A') % Used in the Oce scanning process.
        IMG.tags.Oce_ImageLogic_Characteristics = entry_val;
        
      case 50341 % hex2dec('C4A5') % Description needed.
        IMG.tags.PrintImageMatching = entry_val;
        
      case 50706 % hex2dec('C612') % Encodes DNG four-tier version number; for version 1.1.0.0, the tag contains the bytes 1, 1, 0, 0. Used in IFD 0 of DNG files.
        IMG.tags.DNGVersion = entry_val;
        
      case 50707 % hex2dec('C613') % Defines oldest version of spec with which file is compatible. Used in IFD 0 of DNG files.
        IMG.tags.DNGBackwardVersion = entry_val;
        
      case 50708 % hex2dec('C614') % Unique, non-localized nbame for camera model. Used in IFD 0 of DNG files.
        IMG.tags.UniqueCameraModel = entry_val;
        
      case 50709 % hex2dec('C615') % Similar to 50708, with localized camera name. Used in IFD 0 of DNG files.
        IMG.tags.LocalizedCameraModel = entry_val;
        
      case 50710 % hex2dec('C616') % Mapping between values in the CFAPattern tag and the plane numbers in LinearRaw space. Used in Raw IFD of DNG files.
        IMG.tags.CFAPlaneColor = entry_val;
        
      case 50711 % hex2dec('C617') % Spatial layout of the CFA. Used in Raw IFD of DNG files.
        IMG.tags.CFALayout = entry_val;
        
      case 50712 % hex2dec('C618') % Lookup table that maps stored values to linear values. Used in Raw IFD of DNG files.
        IMG.tags.LinearizationTable = entry_val;
        
      case 50713 % hex2dec('C619') % Repeat pattern size for BlackLevel tag. Used in Raw IFD of DNG files.
        IMG.tags.BlackLevelRepeatDim = entry_val;
        
      case 50714 % hex2dec('C61A') % Specifies the zero light encoding level.Used in Raw IFD of DNG files.
        IMG.tags.BlackLevel = entry_val;
        
      case 50715 % hex2dec('C61B') % Specifies the difference between zero light encoding level for each column and the baseline zero light encoding level. Used in Raw IFD of DNG files.
        IMG.tags.BlackLevelDeltaH = entry_val;
        
      case 50716 % hex2dec('C61C') % Specifies the difference between zero light encoding level for each row and the baseline zero light encoding level. Used in Raw IFD of DNG files.
        IMG.tags.BlackLevelDeltaV = entry_val;
        
      case 50717 % hex2dec('C61D') % Specifies the fully saturated encoding level for the raw sample values. Used in Raw IFD of DNG files.
        IMG.tags.WhiteLevel = entry_val;
        
      case 50718 % hex2dec('C61E') % For cameras with non-square pixels, specifies the default scale factors for each direction to convert the image to square pixels. Used in Raw IFD of DNG files.
        IMG.tags.DefaultScale = entry_val;
        
      case 50719 % hex2dec('C61F') % Specifies the origin of the final image area, ignoring the extra pixels at edges used to prevent interpolation artifacts. Used in Raw IFD of DNG files.
        IMG.tags.DefaultCropOrigin = entry_val;
        
      case 50720 % hex2dec('C620') % Specifies size of final image area in raw image coordinates. Used in Raw IFD of DNG files.
        IMG.tags.DefaultCropSize = entry_val;
        
      case 50721 % hex2dec('C621') % Defines a transformation matrix that converts XYZ values to reference camera native color space values, under the first calibration illuminant. Used in IFD 0 of DNG files.
        IMG.tags.ColorMatrix1 = entry_val;
        
      case 50722 % hex2dec('C622') % Defines a transformation matrix that converts XYZ values to reference camera native color space values, under the second calibration illuminant. Used in IFD 0 of DNG files.
        IMG.tags.ColorMatrix2 = entry_val;
        
      case 50723 % hex2dec('C623') % Defines a calibration matrix that transforms reference camera native space values to individual camera native space values under the first calibration illuminant. Used in IFD 0 of DNG files.
        IMG.tags.CameraCalibration1 = entry_val;
        
      case 50724 % hex2dec('C624') % Defines a calibration matrix that transforms reference camera native space values to individual camera native space values under the second calibration illuminant. Used in IFD 0 of DNG files.
        IMG.tags.CameraCalibration2 = entry_val;
        
      case 50725 % hex2dec('C625') % Defines a dimensionality reduction matrix for use as the first stage in converting color camera native space values to XYZ values, under the first calibration illuminant. Used in IFD 0 of DNG files.
        IMG.tags.ReductionMatrix1 = entry_val;
        
      case 50726 % hex2dec('C626') % Defines a dimensionality reduction matrix for use as the first stage in converting color camera native space values to XYZ values, under the second calibration illuminant. Used in IFD 0 of DNG files.
        IMG.tags.ReductionMatrix2 = entry_val;
        
      case 50727 % hex2dec('C627') % Pertaining to white balance, defines the gain, either analog or digital, that has been applied to the stored raw values. Used in IFD 0 of DNG files.
        IMG.tags.AnalogBalance = entry_val;
        
      case 50728 % hex2dec('C628') % Specifies the selected white balance at the time of capture, encoded as the coordinates of a perfectly neutral color in linear reference space values. Used in IFD 0 of DNG files.
        IMG.tags.AsShotNeutral = entry_val;
        
      case 50729 % hex2dec('C629') % Specifies the selected white balance at the time of capture, encoded as x-y chromaticity coordinates. Used in IFD 0 of DNG files.
        IMG.tags.AsShotWhiteXY = entry_val;
        
      case 50730 % hex2dec('C62A') % Specifies in EV units how much to move the zero point for exposure compensation. Used in IFD 0 of DNG files.
        IMG.tags.BaselineExposure = entry_val;
        
      case 50731 % hex2dec('C62B') % Specifies the relative noise of the camera model at a baseline ISO value of 100, compared to reference camera model. Used in IFD 0 of DNG files.
        IMG.tags.BaselineNoise = entry_val;
        
      case 50732 % hex2dec('C62C') % Specifies the relative amount of sharpening required for this camera model, compared to reference camera model. Used in IFD 0 of DNG files.
        IMG.tags.BaselineSharpness = entry_val;
        
      case 50733 % hex2dec('C62D') % For CFA images, specifies, in arbitrary units, how closely the values of the green pixels in the blue/green rows track the values of the green pixels in the red/green rows. Used in Raw IFD of DNG files.
        IMG.tags.BayerGreenSplit = entry_val;
        
      case 50734 % hex2dec('C62E') % Specifies the fraction of the encoding range above which the response may become significantly non-linear. Used in IFD 0 of DNG files.
        IMG.tags.LinearResponseLimit = entry_val;
        
      case 50735 % hex2dec('C62F') % Serial number of camera. Used in IFD 0 of DNG files.
        IMG.tags.CameraSerialNumber = entry_val;
        
      case 50736 % hex2dec('C630') % Information about the lens. Used in IFD 0 of DNG files.
        IMG.tags.LensInfo = entry_val;
        
      case 50737 % hex2dec('C631') % Normally for non-CFA images, provides a hint about how much chroma blur ought to be applied. Used in Raw IFD of DNG files.
        IMG.tags.ChromaBlurRadius = entry_val;
        
      case 50738 % hex2dec('C632') % Provides a hint about the strength of the camera's anti-aliasing filter. Used in Raw IFD of DNG files.
        IMG.tags.AntiAliasStrength = entry_val;
        
      case 50739 % hex2dec('C633') % Used by Adobe Camera Raw to control sensitivity of its shadows slider. Used in IFD 0 of DNG files.
        IMG.tags.ShadowScale = entry_val;
        
      case 50740 % hex2dec('C634') % Provides a way for camera manufacturers to store private data in DNG files for use by their own raw convertors. Used in IFD 0 of DNG files.
        IMG.tags.DNGPrivateData = entry_val;
        
      case 50741 % hex2dec('C635') % Lets the DNG reader know whether the Exif MakerNote tag is safe to preserve. Used in IFD 0 of DNG files.
        IMG.tags.MakerNoteSafety = entry_val;
        
      case 50778 % hex2dec('C65A') % Illuminant used for first set of calibration tags. Used in IFD 0 of DNG files.
        IMG.tags.CalibrationIlluminant1 = entry_val;
        
      case 50779 % hex2dec('C65B') % Illuminant used for second set of calibration tags. Used in IFD 0 of DNG files.
        IMG.tags.CalibrationIlluminant2 = entry_val;
        
      case 50780 % hex2dec('C65C') % Specifies the amount by which the values of the DefaultScale tag need to be multiplied to achieve best quality image size. Used in Raw IFD of DNG files.
        IMG.tags.BestQualityScale = entry_val;
        
      case 50781 % hex2dec('C65D') % Contains a 16-byte unique identifier for the raw image file in the DNG file. Used in IFD 0 of DNG files.
        IMG.tags.RawDataUniqueID = entry_val;
        
      case 50784 % hex2dec('C660') % Alias Sketchbook Pro layer usage description.
        IMG.tags.Alias_Layer_Metadata = entry_val;
        
      case 50827 % hex2dec('C68B') % Name of original file if the DNG file results from conversion from a non-DNG raw file. Used in IFD 0 of DNG files.
        IMG.tags.OriginalRawFileName = entry_val;
        
      case 50828 % hex2dec('C68C') % If the DNG file was converted from a non-DNG raw file, then this tag contains the original raw data. Used in IFD 0 of DNG files.
        IMG.tags.OriginalRawFileData = entry_val;
        
      case 50829 % hex2dec('C68D') % Defines the active (non-masked) pixels of the sensor. Used in Raw IFD of DNG files.
        IMG.tags.ActiveArea = entry_val;
        
      case 50830 % hex2dec('C68E') % List of non-overlapping rectangle coordinates of fully masked pixels, which can optimally be used by DNG readers to measure the black encoding level. Used in Raw IFD of DNG files.
        IMG.tags.MaskedAreas = entry_val;
        
      case 50831 % hex2dec('C68F') % Contains ICC profile that, in conjunction with the AsShotPreProfileMatrix tag, specifies a default color rendering from camera color space coordinates (linear reference values) into the ICC profile connection space. Used in IFD 0 of DNG files.
        IMG.tags.AsShotICCProfile = entry_val;
        
      case 50832 % hex2dec('C690') % Specifies a matrix that should be applied to the camera color space coordinates before processing the values through the ICC profile specified in the AsShotICCProfile tag. Used in IFD 0 of DNG files.
        IMG.tags.AsShotPreProfileMatrix = entry_val;
        
      case 50833 % hex2dec('C691') % The CurrentICCProfile and CurrentPreProfileMatrix tags have the same purpose and usage as
        IMG.tags.CurrentICCProfile = entry_val;
        
      case 50834 % hex2dec('C692') % the AsShotICCProfile and AsShotPreProfileMatrix tag pair, except they are for use by raw file
        IMG.tags.CurrentPreProfileMatrix = entry_val;
        
      case 50879 % hex2dec('C6BF') % editors rather than camera manufacturers. Used in IFD 0 of DNG files.
        IMG.tags.ColorimetricReference = entry_val;
        
      case 50931 % hex2dec('C6F3') % The CurrentICCProfile and CurrentPreProfileMatrix tags have the same purpose and usage as
        IMG.tags.CameraCalibrationSignature = entry_val;
        
      case 50932 % hex2dec('C6F4') % the AsShotICCProfile and AsShotPreProfileMatrix tag pair, except they are for use by raw file
        IMG.tags.ProfileCalibrationSignature = entry_val;
        
      case 50933 % hex2dec('C6F5') % editors rather than camera manufacturers. Used in IFD 0 of DNG files.
        IMG.tags.ExtraCameraProfiles = entry_val;
        
      case 50934 % hex2dec('C6F6') % The DNG color model documents a transform between camera colors and CIE XYZ values. This tag describes the colorimetric reference for the CIE XYZ values. 0 = The XYZ values are scene-referred. 1 = The XYZ values are output-referred, using the ICC profile perceptual dynamic range. Used in IFD 0 of DNG files.
        IMG.tags.AsShotProfileName = entry_val;
        
      case 50935 % hex2dec('C6F7') % A UTF-8 encoded string associated with the CameraCalibration1 and CameraCalibration2 tags. Used in IFD 0 of DNG files.
        IMG.tags.NoiseReductionApplied = entry_val;
        
      case 50936 % hex2dec('C6F8') % A UTF-8 encoded string associated with the camera profile tags. Used in IFD 0 or CameraProfile IFD of DNG files.
        IMG.tags.ProfileName = entry_val;
        
      case 50937 % hex2dec('C6F9') % A list of file offsets to extra Camera Profile IFDs. The format of a camera profile begins with a 16-bit byte order mark (MM or II) followed by a 16-bit "magic" number equal to 0x4352 (CR), a 32-bit IFD offset, and then a standard TIFF format IFD. Used in IFD 0 of DNG files.
        IMG.tags.ProfileHueSatMapDims = entry_val;
        
      case 50938 % hex2dec('C6FA') % A UTF-8 encoded string containing the name of the "as shot" camera profile, if any. Used in IFD 0 of DNG files.
        IMG.tags.ProfileHueSatMapData1 = entry_val;
        
      case 50939 % hex2dec('C6FB') % Indicates how much noise reduction has been applied to the raw data on a scale of 0.0 to 1.0. A 0.0 value indicates that no noise reduction has been applied. A 1.0 value indicates that the "ideal" amount of noise reduction has been applied, i.e. that the DNG reader should not apply additional noise reduction by default. A value of 0/0 indicates that this parameter is unknown. Used in Raw IFD of DNG files.
        IMG.tags.ProfileHueSatMapData2 = entry_val;
        
      case 50940 % hex2dec('C6FC') % A UTF-8 encoded string containing the name of the camera profile. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.ProfileToneCurve = entry_val;
        
      case 50941 % hex2dec('C6FD') % Specifies the number of input samples in each dimension of the hue/saturation/value mapping tables. The data for these tables are stored in ProfileHueSatMapData1 and ProfileHueSatMapData2 tags. Allowed values include the following: HueDivisions >= 1; SaturationDivisions >= 2; ValueDivisions >=1. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.ProfileEmbedPolicy = entry_val;
        
      case 50942 % hex2dec('C6FE') % Contains the data for the first hue/saturation/value mapping table. Each entry of the table contains three 32-bit IEEE floating-point values. The first entry is hue shift in degrees; the second entry is saturation scale factor; and the third entry is a value scale factor. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.ProfileCopyright = entry_val;
        
      case 50964 % hex2dec('C714') % Contains the data for the second hue/saturation/value mapping table. Each entry of the table contains three 32-bit IEEE floating-point values. The first entry is hue shift in degrees; the second entry is saturation scale factor; and the third entry is a value scale factor. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.ForwardMatrix1 = entry_val;
        
      case 50965 % hex2dec('C715') % Contains a default tone curve that can be applied while processing the image as a starting point for user adjustments. The curve is specified as a list of 32-bit IEEE floating-point value pairs in linear gamma. Each sample has an input value in the range of 0.0 to 1.0, and an output value in the range of 0.0 to 1.0. The first sample is required to be (0.0, 0.0), and the last sample is required to be (1.0, 1.0). Interpolated the curve using a cubic spline. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.ForwardMatrix2 = entry_val;
        
      case 50966 % hex2dec('C716') % Contains information about the usage rules for the associated camera profile. The valid values and meanings are: 0 = "allow copying"; 1 = "embed if used"; 2 = "embed never"; and 3 = "no restrictions". Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.PreviewApplicationName = entry_val;
        
      case 50967 % hex2dec('C717') % Contains information about the usage rules for the associated camera profile. The valid values and meanings are: 0 = "allow copying"; 1 = "embed if used"; 2 = "embed never"; and 3 = "no restrictions". Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.PreviewApplicationVersion = entry_val;
        
      case 50968 % hex2dec('C718') % Defines a matrix that maps white balanced camera colors to XYZ D50 colors. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.PreviewSettingsName = entry_val;
        
      case 50969 % hex2dec('C719') % Defines a matrix that maps white balanced camera colors to XYZ D50 colors. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.PreviewSettingsDigest = entry_val;
        
      case 50970 % hex2dec('C71A') % A UTF-8 encoded string containing the name of the application that created the preview stored in the IFD. Used in Preview IFD of DNG files.
        IMG.tags.PreviewColorSpace = entry_val;
        
      case 50971 % hex2dec('C71B') % A UTF-8 encoded string containing the version number of the application that created the preview stored in the IFD. Used in Preview IFD of DNG files.
        IMG.tags.PreviewDateTime = entry_val;
        
      case 50972 % hex2dec('C71C') % A UTF-8 encoded string containing the name of the conversion settings (for example, snapshot name) used for the preview stored in the IFD. Used in Preview IFD of DNG files.
        IMG.tags.RawImageDigest = entry_val;
        
      case 50973 % hex2dec('C71D') % A unique ID of the conversion settings (for example, MD5 digest) used to render the preview stored in the IFD. Used in Preview IFD of DNG files.
        IMG.tags.OriginalRawFileDigest = entry_val;
        
      case 50974 % hex2dec('C71E') % This tag specifies the color space in which the rendered preview in this IFD is stored. The valid values include: 0 = Unknown; 1 = Gray Gamma 2.2; 2 = sRGB; 3 = Adobe RGB; and 4 = ProPhoto RGB. Used in Preview IFD of DNG files.
        IMG.tags.SubTileBlockSize = entry_val;
        
      case 50975 % hex2dec('C71F') % This tag is an ASCII string containing the name of the date/time at which the preview stored in the IFD was rendered, encoded using ISO 8601 format. Used in Preview IFD of DNG files.
        IMG.tags.RowInterleaveFactor = entry_val;
        
      case 50981 % hex2dec('C725') % MD5 digest of the raw image data. All pixels in the image are processed in row-scan order. Each pixel is zero padded to 16 or 32 bits deep (16-bit for data less than or equal to 16 bits deep, 32-bit otherwise). The data is processed in little-endian byte order. Used in IFD 0 of DNG files.
        IMG.tags.ProfileLookTableDims = entry_val;
        
      case 50982 % hex2dec('C726') % MD5 digest of the data stored in the OriginalRawFileData tag. Used in IFD 0 of DNG files.
        IMG.tags.ProfileLookTableData = entry_val;
        
      case 51008 % hex2dec('C740') % Normally, pixels within a tile are stored in simple row-scan order. This tag specifies that the pixels within a tile should be grouped first into rectangular blocks of the specified size. These blocks are stored in row-scan order. Within each block, the pixels are stored in row-scan order. Used in Raw IFD of DNG files.
        IMG.tags.OpcodeList1 = entry_val;
        
      case 51009 % hex2dec('C741') % Specifies that rows of the image are stored in interleaved order. The value of the tag specifies the number of interleaved fields. Used in Raw IFD of DNG files.
        IMG.tags.OpcodeList2 = entry_val;
        
      case 51022 % hex2dec('C74E') % Specifies the number of input samples in each dimension of a default "look" table. The data for this table is stored in the ProfileLookTableData tag. Allowed values include: HueDivisions >= 1; SaturationDivisions >= 2; and ValueDivisions >= 1. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.OpcodeList3 = entry_val;
        
      case 51041 % hex2dec('C761') % Default "look" table that can be applied while processing the image as a starting point for user adjustment. This table uses the same format as the tables stored in the ProfileHueSatMapData1 and ProfileHueSatMapData2 tags, and is applied in the same color space. However, it should be applied later in the processing pipe, after any exposure compensation and/or fill light stages, but before any tone curve stage. Each entry of the table contains three 32-bit IEEE floating-point values. The first entry is hue shift in degrees, the second entry is a saturation scale factor, and the third entry is a value scale factor. Used in IFD 0 or Camera Profile IFD of DNG files.
        IMG.tags.NoiseProfile = entry_val;
        
      case 51089 % hex2dec('C791') % Specifies the list of opcodes (image processing operation codes) that should be applied to the raw image, as read directly from the file. Used in Raw IFD of DNG files.
        IMG.tags.OriginalDefaultFinalSize = entry_val;
        
      case 51090 % hex2dec('C792') % Specifies the list of opcodes (image processing operation codes) that should be applied to the raw image, just after it has been mapped to linear reference values. Used in Raw IFD of DNG files.
        IMG.tags.OriginalBestQualityFinalSize = entry_val;
        
      case 51091 % hex2dec('C793') % Specifies the list of opcodes (image processing operation codes) that should be applied to the raw image, just after it has been demosaiced. Used in Raw IFD of DNG files.
        IMG.tags.OriginalDefaultCropSize = entry_val;
        
      case 51107 % hex2dec('C7A3') % Describes the amount of noise in a raw image; models the amount of signal-dependent photon (shot) noise and signal-independent sensor readout noise, two common sources of noise in raw images. Used in Raw IFD of DNG files.
        IMG.tags.ProfileHueSatMapEncoding = entry_val;
        
      case 51108 % hex2dec('C7A4') % If this file is a proxy for a larger original DNG file, this tag specifics the default final size of the larger original file from which this proxy was generated. The default value for this tag is default final size of the current DNG file, which is DefaultCropSize * DefaultScale.
        IMG.tags.ProfileLookTableEncoding = entry_val;
        
      case 51109 % hex2dec('C7A5') % If this file is a proxy for a larger original DNG file, this tag specifics the best quality final size of the larger original file from which this proxy was generated. The default value for this tag is the OriginalDefaultFinalSize, if specified. Otherwise the default value for this tag is the best quality size of the current DNG file, which is DefaultCropSize * DefaultScale * BestQualityScale.
        IMG.tags.BaselineExposureOffset = entry_val;
        
      case 51110 % hex2dec('C7A6') % If this file is a proxy for a larger original DNG file, this tag specifics the DefaultCropSize of the larger original file from which this proxy was generated. The default value for this tag is the OriginalDefaultFinalSize, if specified. Otherwise, the default value for this tag is the DefaultCropSize of the current DNG file.
        IMG.tags.DefaultBlackRender = entry_val;
        
      case 51111 % hex2dec('C7A7') % Provides a way for color profiles to specify how indexing into a 3D HueSatMap is performed during raw conversion. This tag is not applicable to 2.5D HueSatMap tables (i.e., where the Value dimension is 1). The currently defined values are: 0 = Linear encoding (method described in DNG spec); 1 = sRGB encoding (method described in DNG spec).
        IMG.tags.NewRawImageDigest = entry_val;
        
      case 51112 % hex2dec('C7A8') % Provides a way for color profiles to specify how indexing into a 3D LookTable is performed during raw conversion. This tag is not applicable to a 2.5D LookTable (i.e., where the Value dimension is 1). The currently defined values are: 0 = Linear encoding (method described in DNG spec); 1 = sRGB encoding (method described in DNG spec).
        IMG.tags.RawToPreviewGain = entry_val;
        
      case 51125 % hex2dec('C7B5') % Provides a way for color profiles to increase or decrease exposure during raw conversion. BaselineExposureOffset specifies the amount (in EV units) to add to th e BaselineExposure tag during image rendering. For example, if the BaselineExposure value fo r a given camera model is +0.3, and the BaselineExposureOffset value for a given camera profile used to render an image for that camera model is -0.7, then th e actual default exposure value used during rendering will be +0.3 - 0.7 = -0.4.
        IMG.tags.DefaultUserCrop = entry_val;
        
      otherwise
        IMG.tags.(['tag' num2str(entry_tag)]) = entry_val;
        known_tag = false;
    end
  end

end


