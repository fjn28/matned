function level = otsu_threshold(arg)
%% OTSU automatic thresholding
% (adapted from faulty code on wikipedia page)
% F Nedelec, 27.04.2019

if ( ismatrix(arg) )
    if ( min(min(arg)) < 0 )
        warning('image has negative pixel values');
    end
    top = max(max(arg));
    val = reshape(arg, numel(arg), 1);
    histogramCounts = histcounts(val, 0:top);
else
    top = length(arg);
    histogramCounts = arg;
end

total = sum(histogramCounts);
sumB = 0;
wB = 0;
maximum = 0.0;
sum1 = dot(0:top-1, histogramCounts);
for ii = 1:top
    wF = total - wB;
    if wB > 0 && wF > 0
        mF = (sum1 - sumB) / wF;
        val = wB * wF * ((sumB / wB) - mF) * ((sumB / wB) - mF);
        if ( val >= maximum )
            level = ii;
            maximum = val;
        end
    end
    wB = wB + histogramCounts(ii);
    sumB = sumB + (ii-1) * histogramCounts(ii);
end
end
