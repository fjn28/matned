function [im_flat, back, err] = flatten_background(im, tile, range)

%function [im_flat, back] = flatten_background(im)
%function [im_flat, back] = flatten_background(im, tile)
%function [im_flat, back] = flatten_background(im, tile, range)
%
% Corrects uneven exposure/acquisition of confocal images
%
% tile (2 elements) specifies a vertical and horizontal tiling
% range (2 elements) specifies the range of pixel values used for the correction
%
% F. Nedelec, April 2008, March 2013, April 2019

if isfield(im, 'data')
    im = im.data;
end

if nargin < 3
    [b, s] = image_background(im, 1);
    range = [ b-s*2, b+s ];
    fprintf(' flatten_background range is [%.1f %.1f]\n', range(1), range(2));
end

if nargin < 2  || isempty(tile)
    
    iii = double(im);
    msk = ( range(1) < iii ) & ( iii < range(2) );
    quad = quadratic_fit(iii, msk);
    fprintf(' Background fit: %.5f XX %+.5f XY %+.5f YY %+.5f X %+.5f Y %+.5f\n', quad);
    back = quadratic_value(size(im), quad);
    
    for i = 1:16
        msk = ( back-s*2 < iii ) & ( iii < back+s );
        quad = quadratic_fit(iii, msk);
        fprintf(' Background fit: %.5f XX %+.5f XY %+.5f YY %+.5f X %+.5f Y %+.5f\n', quad);
        back = quadratic_value(size(im), quad);
    end
    im_flat = im - back;
    return;
end


imz = size(im) ./ tile;
im_flat = zeros(size(im));
back = zeros(size(im));

if any( imz ~= round(imz) )
    error('The image cannot be divided in the provided number of tiles');
end

err = 0;
nbx = 0;

for i = 0:tile(1)-1
for j = 0:tile(2)-1
    
    irange = ( 1:imz(1) ) + i*imz(1);
    jrange = ( 1:imz(2) ) + j*imz(2);
    
    iii = double(im(irange, jrange));
    
    %calculate the best quadratic fit:
    %quad = quadratic_iso_fit(iii, ( iii < level ));
    [b, s] = image_background(iii);
    range = [ 0, b+s ];
    msk = ( range(1) < iii ) & ( iii < range(2) );
    
    quad = quadratic_fit(iii, msk);
    fprintf(' Quadratic fit: %.3f XX %+.3f XY %+.3f YY %+.3f X %+.3f Y %+.3f\n', quad);
    
    qval = quadratic_value(imz, quad);
    
    err = err + sum(sum(msk.*(iii-qval).^2));
    nbx = nbx + sum(sum(msk));
    %show_overlay_mask(qval, msk, 'magnification', 1);

    im_flat(irange, jrange) = iii - qval;
    back(irange, jrange) = qval;
end
end

err = sqrt( err / nbx );

end

