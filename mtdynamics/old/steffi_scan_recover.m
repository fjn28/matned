function steffi_scan_recover;



parent_dir = pwd;
files      = dir('*');

%get the file where the center is recorded:
for u=1:size(files, 1)
   
    cd( parent_dir );
    dirname = files(u).name;

    if ( 1 == files(u).isdir ) & ( dirname(1) ~= '.' )

        fprintf(1, 'scan in %s\n', dirname );
        cd( dirname );

        steffi_recover_data( dirname );
        
    end
end

cd( parent_dir );
