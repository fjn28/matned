function [ frame, dist ] = steffi_calculate_length( center, tip, frame_range, filename )

global micrometer_per_pixel;

nblines = size( tip, 1 );
%dist    = zeros( nblines, 1 );
%frame   = zeros( nblines, 1 );

warn = 1;
if ( tip( 1, 1 ) ~= 1 )
    fprintf(1, 'warning: index of frame do not start at 1: %s\n', filename);
    warn = 0;
end

for line = 1: nblines 
        
    x = tip( line, 2 );
    y = tip( line, 3 );
    
    F = line + frame_range(1) - 1;
    cline = find( center(:,1) == F );
    
    if warn & ( tip( line, 1 ) ~= line )
        fprintf(1, 'warning: jump in index of frame: %s\n', filename);
        warn = 0;
    end

    otherFrame = F;
    while isempty( cline )
        %error('steffi: no center-click found for frame %i', F);
        %try the next frame:
        otherFrame = otherFrame + 1;
        cline = find( center(:,1) == otherFrame );
        if ( otherFrame > 151 )
            return;
        end
    end
    
    if size( cline, 1 ) > 1
        error('steffi: more than one center-click found for frame %i', F);
    end
    
    cx = center( cline, 2 );
    cy = center( cline, 3 );
    
    frame( line, 1 ) = F;
    dist( line, 1 )  = sqrt( ( x - cx ) ^2 + ( y - cy )^2 );
    
end

if ( micrometer_per_pixel == 0 )
    micrometer_per_pixel = 0.1301;
    %error('micrometer_per_pixel == 0 : please calibrate');
end

dist = dist * micrometer_per_pixel * 72;