function steffi_recover_data(directory_name)
%steffi() extract the mt-length history from messed-up files

if ( nargin < 1 )
    directory_name = pwd;
end


%find files where the mt-tips are recorded:
files = dir('*.txt');

%get the file where the center is recorded:
for u=1:size(files, 1)
   
   filename = files(u).name;
   
   if ( ~isempty( strfind( filename, 'cntr' ) ) )
       if ( exist( 'center', 'var' ) )
           disp('warning: more than one file named  *cntr*.txt');
       else
           center = load( filename );
       end
   end        
end

if ( ~exist( 'center', 'var' ))
    error('I did not find a file for the center-clicks');
end

dcnt  = 0;
mtcnt = 0;

%process all other files:
for u=1:size(files, 1)
   
   filename = files(u).name;
   
   if ( isempty( strfind( filename, 'cntr' ) ) )
      
       frame_range = steffi_frame_range( filename );

       tip = load( filename );
              
       [frame, length] = steffi_calculate_length( center, tip, frame_range, filename );
       time = frame * 2;

       if ( size(time,1) ~= size(length,1) )
           error('internal error');
       end

       mtcnt = mtcnt + 1;

       fprintf(1, 'file %20s range [ %2i %2i ] ', filename, frame_range );

       filename=sprintf('%i', mtcnt);
       file=fopen(filename, 'w');
       for ii = 1 : size(time,1)
          fprintf(file, '%i  %.4f\n', time(ii), length(ii)); 
       end
       fclose(file);

       fprintf(1, ': file %s\n', filename);

   end
end
