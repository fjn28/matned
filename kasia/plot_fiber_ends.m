function plot_fiber_ends(pth)
%
%
% Francois's function to plot the profile of simulated spindles
% Updated Heidelberg 10.7.2015

global max_pos;

if nargin < 1
    pth = '.';
end

%% only use data after 4000 sec

pile = load_chunks(fullfile(pth, '/fibers.txt'));

if numel(pile) == 0
    error('Could not load data from <fibers.txt>');
end

data = pile{1};

% idxL = column index for length;
% idxM = X-position of MINUS_END
% idxP = X-position of PLUS_END
if size(data, 2) == 9
    idxL = 3;
    idxM = 5;
    idxP = 8;
elseif size(data, 2) == 11
    idxL = 2;
    idxM = 3;
    idxP = 9;
elseif size(data, 2) == 14
    idxL = 2;
    idxM = 3;
    idxP = 9;
elseif size(data, 2) == 15
    idxL = 3;
    idxM = 4;
    idxP = 10;
elseif size(data, 2) == 17
    idxL = 3;
    idxM = 5;
    idxP = 12;
else
    error('unsupported format in <fibers.txt>');
end

%%
figure('Name', pth);
hold on;

for n = 1 : length(pile)
    
    data = pile{n};

    endP = data(:, idxP);
    endM = data(:, idxM);

    selR = logical( endP > endM );
    selL = logical( endP < endM );

    plot(endM(selR), n, 'b.');
    plot(endM(selL), n, 'r.');
    
end

end

%% LOAD_CHUNKS

function pile = load_chunks(filename)

[status, val] = system(['grep -n "^% time " ', filename]);

heads = strsplit(val, '\n');

sln = '1';
for n = 2 : length(heads)
    x = strfind(heads{n}, ':');
    if ~ isempty(x)
        eln = heads{n}(1:x-1);
        cmd = sprintf('sed -n %s,%sp %s > cut.txt', sln, eln, filename);
        system(cmd);
        pile{n-1} = load('cut.txt');
        sln = eln;
    end
end


end

