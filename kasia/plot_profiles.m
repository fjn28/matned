function plot_profiles(pth)
%
%
% Francois's function to plot the profile of simulated spindles
% Last updated in Strasbourg, 3 April 2015

if nargin < 1
    pth = '.';
end

%% use all data

data = load(fullfile(pth, '/profile.txt'));

mxx = -data(1,1);
nbb = find(data(:,1)==mxx, 1, 'first');
pos = data(1:nbb, 1);

nbr = fix(size(data,1)/nbb);
lst = nbr * nbb;

lft = reshape(data(1:lst,2), nbb, nbr);
rit = reshape(data(1:lst,3), nbb, nbr);
pol = ( rit - lft ) ./ ( rit + lft );

figure('Name', pth);
plot(pos, pol, 'LineWidth', 0.5);
title('Polarity');
ylim([-1 1]);
xlabel('position / um');

end
