function [ data, cnt ] = load_after(filename, start)

% Extract part of the data from a file, which has time information
%
% F. Nedelec, 29.10.2015

% extract times and line numbers:
status = system(['grep -n "^% time " ', filename, ' > tmp.txt']);

if status
    error('grep failed');
end

[status, val] = system('cut -f 1 -d : tmp.txt');

lines = str2num(val);

[status, val] = system('cut -f 3 -d " " tmp.txt');

times = str2num(val);

if numel(times) ~= numel(times)
    fprintf(2, ['data file ',filename,' contains duplicates of some time points\n']);
end

if any( size(times) ~= size(lines) )
    error('size missmatch');
end

line = lines(1);

try
    idx = 1;
    time = times(idx);
    while idx < size(times, 1) && times(idx) < start
        if times(idx) > time
            line = lines(idx);
        end
        idx = idx + 1;
    end
catch
    fprintf(2, 'could not read times\n');
end

status = system(['tail -n +', int2str(line), ' ', filename, ' > cut.txt']);

cnt = sum( lines >= line );

if status == 0
    data = load('cut.txt');
else
    data = [];
end

end
