function [ints,rms,lengs,SintensL] = analyze_intens_onespind( state , center , spindle , image , opt ,n_sp)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
if state<1
    return
end
if ~isfield(opt,'nclickon');
    nc=3;
else
    nc=opt.nclickon;
end
if nargin<1 || isempty(image)
   error('You must provide an image');
end

% Image size
sI=size(image);
Nx=sI(1);
Ny=sI(2);



% Finding the distances from point to segment
Distsegs=zeros(Nx,Ny,ns);
Signsegs=zeros(Nx,Ny,ns);
lengs=zeros(state,nc);
for i=1:state
    % For all the poles
    for j=1:nc
        % For all the points
        k=(i-1)*nc+j;
        [Distsegs(:,:,k) Signsegs(:,:,k)]=dist_pointsIM_segment(Nx,Ny,clicks(j,:,i),clicks(j+1,:,i));
        % length of the segments
        lengs(i,j)=norm(clicks(j,:,i)-clicks(j+1,:,i));
    end
end

% Correct image bg
minim=min(min(image));
dimage=double(image-minim);

% Now computing the interesting information
ints=zeros(state,nc);
var=zeros(state,nc);
mean=zeros(state,nc);
rms=zeros(state,nc);
wheight=zeros(Nx,Ny,ns);
w_im=zeros(Nx,Ny,ns);
win=zeros(Nx,Ny,ns);
intD=floor(Distsegs);
%for x=1:Nx
%    for y=1:Ny
%        [dis,or]=sort(Distsegs(x,y,:));
%        neq=sum(Distsegs(x,y,:)==dis(1));
%        wheight(x,y,or(1:neq))=1/neq;
%    end
%end
mindist=min(Distsegs,[],3);
for i=1:ns
    wheight(:,:,i)= ( Distsegs(:,:,i)==mindist);
end
sumwheight=sum(wheight,3);
for i=1:ns
    win(:,:,i)=wheight(:,:,i).*dimage;
    wheight(:,:,i)= wheight(:,:,i)./sumwheight;
end


md=max(max(max(intD)));
nl=2*md+1;
%intensL=zeros(1,md,ns);
SintensL=zeros(1,nl,ns);
signedD=intD.*Signsegs;

for l=1:md
    %isd=(intD==l);
    %intensL(1,l,:)=sum(sum(isd.*win,1),2)./max(sum(sum(isd,1),2),1);
    isd=(signedD==l);
    SintensL(1,md+l,:)=sum(sum(isd.*win,1),2)./max(sum(sum(isd,1),2),1);
    msd=(signedD==-l);
    SintensL(1,md-l+1,:)=sum(sum(msd.*win,1),2)./max(sum(sum(msd,1),2),1);
end

for i=1:ns
    figure
    plot(-md:md,SintensL(1,:,i))
    y=smooth(SintensL(1,:,i),5);
    hold all
    plot(-md:md,y)
    xlabel(['segment ' num2str(i)]);
end

sqnorm=Distsegs(:,:,:).^2;
Distsegs(:,:,:)=Distsegs(:,:,:).*Signsegs(:,:,:);

for i=1:state
    for j=1:nc
        k=(i-1)*nc+j;
        w_im(:,:,k)=double(image).*wheight(:,:,k);
        w_im(:,:,k)=w_im(:,:,k)-min(min(w_im(:,:,k)));
        ints(i,j)=sum(sum(w_im(:,:,k)));
        mean(i,j)=sum(sum(w_im(:,:,k).*Distsegs(:,:,k)));
        var(i,j)=sum(sum(w_im(:,:,k).*sqnorm(:,:,k)));
    end
end
mean(:,:)=mean(:,:)./ints(:,:);
var(:,:)=var(:,:)./ints(:,:);
rms(:,:)=sqrt(var(:,:)-mean(:,:).^2);
        
end

