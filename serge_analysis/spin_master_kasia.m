function spin_master_kasia

% Open a dialog to easily start the macros used to analyse
% spindle morphologies
%
%
% F. Nedelec, Feb., March 2008 - August 2010

rehash;
%default options:
opt = spin_default_options;

%size of the buttons
bwidth   = 160;
bheight  = 38;
FontSize = 12;

%number of buttons
nButtons = [ 8, 4 ];

sFig = [200 200 nButtons(2)*bwidth+20 nButtons(1)*(bheight+5)+50];

%pick a random color
color = rand(1,3);
while sum(color) < 1
    color = rand(1,3);
end

hFig = figure('Position', sFig,...
    'Name', 'Spin-Master', 'MenuBar', 'none', 'Resize', 'off', ...
    'NumberTitle', 'off', 'NextPlot', 'new',...
    'HandleVisibility', 'callback', 'Color', color);



%% the 'Close' button
uicontrol(hFig, 'Style', 'pushbutton', 'Tag', 'close-button', ...
    'Position', [ 15 sFig(4)-bheight-5 bwidth-10 bheight ],...
    'String', 'Close All', 'FontSize', FontSize, ...
    'Callback',{@callback_close});

%% the auto_center checkbox
posX = bwidth+30;
posY = sFig(4) - 10 - 20 * (1:4);

uicontrol(hFig, 'Style', 'checkbox', 'BackgroundColor', color, ...
    'Position', [ posX posY(1) bwidth 25 ], 'Value', opt.auto_center,...
    'String', 'Auto-Center DNA', 'FontSize', FontSize, ...
    'Callback',{@callback_center});

%% the use_mask checkbox
uicontrol(hFig, 'Style', 'checkbox', 'BackgroundColor', color, ...
    'Position', [ posX posY(2) bwidth 25 ], 'Value', opt.use_mask,...
    'String', 'Use Circular Mask','FontSize', FontSize, ...
    'Callback',{@callback_mask});

%% the backgroud checkbox
uicontrol(hFig, 'Style', 'checkbox', 'BackgroundColor', color, ...
    'Position', [ posX posY(3) bwidth 25 ], 'Value', opt.local_background,...
    'String', 'Local Background', 'FontSize', FontSize, ...
    'Callback',{@callback_background});


%% the flatten_image checkbox
uicontrol(hFig, 'Style', 'checkbox', 'BackgroundColor', color, ...
    'Position', [ posX posY(4) bwidth 25 ], 'Value', opt.flatten_image,...
    'String', 'Flatten Image (3x3)', 'FontSize', FontSize, ...
    'Callback',{@callback_flatten});


%% the 'Radius' field
fwidth = 80;
posX = 3*bwidth + [ 0 bwidth-fwidth ];
posY = sFig(4) - 30 * (1:3);

rPos = [ 3*bwidth, 4*bwidth-60, sFig(4)-30, sFig(4)-60, sFig(4)-90 ];

uicontrol(hFig, 'Style', 'edit', ...
    'Position', [ posX(1) posY(1) fwidth 25 ],...
    'String', num2str(opt.radius), 'FontSize', FontSize,...
    'Callback',{@callback_radius});

uicontrol(hFig, 'Style', 'text', 'String', 'Radius',...
    'Position', [ posX(2) posY(1) fwidth 25 ],...
    'FontSize', FontSize, 'BackgroundColor', color);



%% the 'Time-shift' field

uicontrol(hFig, 'Style', 'edit', ...
    'Position', [ posX(1) posY(2) fwidth 25 ],...
    'String', num2str(opt.time_shift), 'FontSize', 10,...
    'Callback',{@callback_time_shift});

uicontrol(hFig, 'Style', 'text', 'String', 'Time Shift',...
    'Position', [ posX(2) posY(2) fwidth 25 ],...
    'FontSize', FontSize, 'BackgroundColor', color);

%% The 'visual' menu

uicontrol(hFig, 'Style', 'popupmenu', ...
    'Position', [ posX(1) posY(3) fwidth 25 ],...
    'String', '0|1|2|3|4', 'Value', opt.visual+1, 'FontSize', FontSize, ...
    'Callback',{@callback_visual});

uicontrol(hFig, 'Style', 'text', 'String', 'Visual Level',...
    'Position', [ posX(2) posY(3) fwidth 25 ],...
    'FontSize', FontSize, 'BackgroundColor', color);


%% create buttons, and define the associated actions:

button(9,1, 'Select image',     'opt=select_image(opt);');

button(7,1, 'Set Poles',        'spin_click_poles(image_base,opt);');
button(7,2, 'Analyze poles',    'analyze_spindles(image_base,opt);');
button(7,3, 'Edit pole',        'edit_pole(image_base(),opt);');
button(7,4, 'Show Poles',        'show_poles(opt);');

button(5,1, 'Create List',       'spin_set_images;');
button(5,2, 'Edit List',        'edit image_list.m;');
button(5,3, 'Select Base',       'edit image_base.m;');
button(5,4, 'Show Images',      'spin_show_images(opt);');

button(4,1, 'Set Regions',      'set_regions_grid(image_base, opt.region_filename);');
button(4,2, 'Edit Regions',     'edit_regions(image_base,opt.region_filename);');
button(4,3, 'Show Regions',     'show_regions(image_base,load_regions(opt.regions_filename));');
button(4,4, 'Set DNA regions',  'opt=select_dna(opt);');





button(2,1, 'Measure DNA',  'opt=spin_measure_dna(opt);');
button(2,2, 'Measure Mass', 'spin_measure(''mass'', opt);');
button(2,3, 'Measure Area', 'spin_measure_area(opt);');
button(2,4, 'Plot Mass',    'spin_plot_mass(opt);');

button(1,1, 'Count Beads',  'spin_measure(''beads'', opt);');
button(1,2, 'Plot Beads',   'spin_plot_beads(opt);');
button(1,3, 'Plot Calib.',  'spin_calibrated_dna(''DNA fluorescence'', 1);');
button(1,4, 'Export Mass',  'spin_export_mass(opt);');


drawnow

%% create function buttons:

    function button(i, j, name, action)
        h = uicontrol(hFig, 'Style', 'pushbutton',...
            'Position',[ 15+bwidth*(j-1) 10+bheight*(i-1) bwidth-10 bheight ],...
            'String', name, 'FontSize', FontSize, 'Callback',{@callback, action});
        if strncmp(name, 'Plot', 4)  ||  strncmp(name, 'Show', 4)
            set(h, 'ForegroundColor', [0 0 1]);
        end
    end


%% Callbacks

    function callback_close(hObject, eventdata)
        if strcmp(get(hObject, 'String'), 'Close All')
            figs = setdiff( get(0,'Children'), hFig );
            delete(figs);
        end
        set_state(1);
    end


    function callback_radius(hObject, eventdata)
        opt.radius = str2num( get(hObject, 'String') );
        %fprintf('radius = %f\n', opt.radius);
    end

    function callback_time_shift(hObject, eventdata)
        opt.time_shift = str2num( get(hObject, 'String') );
        %fprintf('time_shift = %f\n', opt.time_shift);
    end

    function callback_visual(hObject, eventdata)
        opt.visual = get(hObject, 'Value') - 1;
        %fprintf('visual = %i\n', opt.visual);
    end

    function callback_center(hObject, eventdata)
        opt.auto_center = get(hObject, 'Value');
        %fprintf('auto_center = %i\n', opt.auto_center);
    end

    function callback_background(hObject, eventdata)
        opt.local_background = get(hObject, 'Value');
        %fprintf('local_background = %i\n', opt.local_background);
    end

    function callback_mask(hObject, eventdata)
        opt.use_mask = get(hObject, 'Value');
        %fprintf('use_mask = %i\n', opt.use_mask);
    end

    function callback_flatten(hObject, eventdata)
        opt.flatten_image = get(hObject, 'Value');
        %fprintf('flatten_image = %i\n', opt.flatten_image);
    end

    function callback(hObject, eventdata, action)
        set_state(0);
        %commandwindow;
        try
            eval(action);
        catch ME
            fprintf('Error executing %s\n', action);
            rethrow(ME);
        end
        set_state(1);
    end


    function set_state(state)
        % disable all user-controls
        hlist = findobj(get(hFig, 'Children'), 'Type', 'uicontrol');
        for ii = 1:size(hlist,1)
            h = hlist(ii);
            if strcmp(get(h, 'Tag'), 'close-button')
                if state
                    set(h, 'String', 'Close All');
                else
                    set(h, 'String', 'Reset');
                end
            else
                if state
                    set(h, 'Enable', 'on');
                else
                    set(h, 'Enable', 'off');
                end
            end
        end
        
        drawnow;
    end

end


