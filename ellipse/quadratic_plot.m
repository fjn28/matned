function h = quadratic_plot(quad, flip)
% Draw the ellipse specified by `quad` on current plot
% q(1)*X^2 + q(2)*X*Y + q(3)*Y^2 + q(4)*X + q(5)*Y + q(6)
%  
% function h = quadratic_plot(q, flip)
%
% if flip=1, the X and Y coordinates are flipped
%
% F. Nedelec, April 2014


if nargin < 2
    flip = 0;
end

try
    [cen, axs, angle] = quadratic_to_ellipse(quad);
    
    cx = cen(1);
    cy = cen(2);
    
    C = cos(angle);
    S = sin(angle);
    
    T = 0 : pi/180 : 2*pi;
    
    X = cx + C*axs(1)*cos(T) - S*axs(2)*sin(T);
    Y = cy + S*axs(1)*cos(T) + C*axs(2)*sin(T);
    
    if ( flip )
        h = plot(Y, X, 'y-');
        hold on;
        plot(cy, cy, 'yo');
        plot([cy+S*axs(1), cy-S*axs(1)], [cx+C*axs(1), cx-C*axs(1)], 'y-');
    else
        h = plot(X, Y, 'm-');
        hold on;
        plot(cx, cy, 'mo');
        plot([cx+C*axs(1), cx-C*axs(1)], [cy+S*axs(1), cy-S*axs(1)], 'm-');
    end
catch
end

end

