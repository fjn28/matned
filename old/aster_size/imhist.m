function h = imhist( im, mask )

% h = imhist( im, mask )
%
% calculate the histogram of the given image, optionally restricted on the mask
% warning: h(1) is the number of pixels of value 0, etc.

if ( isfield(im,'data') ) 
    im = double( im.data ); 
end

if ( nargin < 2 )
    
    max_val = max( max( im ) );
    max_val = round( double( max_val ));
    
    h = sum( histc( round( double( im ) ), 0:max_val ), 2);
    %h = hist( reshape( double(im), size(im,1) * size(im,2),1 ), 1:maxval);
        
else

    max_val = max(max( im .* mask ));
    min_val = min(min( im + max_val * ( 1 - mask ) ));
    
    h = zeros( max_val+1, 1 );
    
    for vp = min_val  : max_val
        
        which  = find( im == vp );
        if ~ isempty( which )
            which  = which( find( mask( which ) == 1 ) );
            h(vp+1) = size( which, 1 );
        end
        
    end
    
end

return;
