function [ figid , im_handle ] = showim ( im, varargin )
% function [ figid , im_handle ] = showim ( im, varargin )
%
% show the picture < im > in gray levels
% F. Nedelec 2001


if ( isfield(im, 'data') ) 
    
    if ( length(im) > 1 ) 
        disp('showim thinks it is a movie file : showing first picture');
        im = im( 1 );
    end

    im = double( im.data ); 
    
end

%=============set default values

imsize        = size( im );

%=============parse input commands

[FigName, MakeNewFigure, TrueRatio, AxisTicks,...
        FigScale, FigOffset, Clim, HIsaturated, HIvalue ] = ParseInput( varargin );

%=============do the job:

if MakeNewFigure

    if  FigScale == 0
        
        scrsz = get(0,'ScreenSize');
        scrsz = scrsz(3:4) - [ 10 20 ];
        FigScale  = min( scrsz(2:-1:1)  ./ imsize );
        
        if ( FigScale > 1 )  FigScale = floor( FigScale ); end
        if ( min(imsize) > 100 ) & ( FigScale > 1 ) FigScale = 1; end
        if ( FigScale > 8 )  FigScale = 8; end
        if ( FigScale < 1 )  FigScale = 1 / ceil( 1/FigScale ); end
        
    end
    
    figsize = FigScale * imsize(2:-1:1);
    figpos  = [ FigOffset(1), scrsz(2)-FigOffset(2) - figsize(2), figsize ];
    FigName = [FigName, ' magn.x', num2str( FigScale ) ];
    figid  = figure('Name', FigName, 'Position', figpos, 'MenuBar','None');
    set(figid, 'Units', 'pixels');
    set(gca, 'Position', [0 0 1 1]);

elseif TrueRatio
    
    drawnow;
    figsz = get(gcf, 'Position');
    axesz = get(gca, 'Position');
    pixsz = figsz(3:4) .* axesz(3:4);
    if ( FigScale == 0 )
        FigScale   = min( pixsz(2:-1:1)  ./ imsize );
    end
    newsz      = FigScale .* imsize(2:-1:1) ./ figsz(3:4);
    axesz(1:2) = axesz(1:2) + 0.5 * ( axesz(3:4) - newsz );
    axesz(3:4) = newsz;
    set( gca, 'Position', axesz );
    
end

figid = gcf;

%================================scaling image, Clim(1) -> 1,  and Clim(2) -> 256

im = double( im );

infinites = find( 0 == isfinite( im ) );
im( infinites ) = 0;

if Clim == [ 0 0 ]
    Clim = [ min( min( im )), max( max( im )) ];
    %disp(sprintf('autoscale min = %.2f, max = %.2f', Clim(1), Clim(2)));
end

scaled = uint8( ( im - Clim( 1 ) ) * ( 255 / ( Clim(2) - Clim(1) ) ) );

%================================draw the image:

cmap      = gray( 256 );
im_handle = image( scaled );
colormap( cmap );

if AxisTicks
    set( gca, 'xtick', [ 0:100:imsize(2) ] );
    set( gca, 'ytick', [ 0:100:imsize(1) ] );    
else
    axis off; 
end
hold on;


%=========================================highlight saturated pixels:

if HIsaturated
   %detect potential saturation:
   pmax = max( max( im ) );
   if ( pmax == 2^12-1 ) | ( pmax == 2^16-1 )
       %cmap(2, :) = [ 0 0 0.8 ];
       cmap(255, :) = [ 0.8 0 0 ];
       cmap(256, :) = [ 0 0 1 ];
       colormap( cmap );
       nsat = length( find( im == pmax ) ) ;
       disp(sprintf('%i pixels saturated  ( %.5f%% ) ', nsat, 100 * nsat / ( imsize(1) * imsize(2) )));
   end
end

%=========================================highlight one particular pixel value:

if HIvalue > 0 
   cmap(HIvalue, :) = [ 0 1 0 ];
   colormap( cmap );
end

drawnow;
return;


%========================================Input parsing function

function [ FigName, MakeNewFigure, TrueRatio, AxisTicks, ...
        FigScale, FigOffset, Clim, HIsaturated, HIvalue ] = ParseInput( varargin );

FigName       = '';
MakeNewFigure = 1;
TrueRatio     = 0;
AxisTicks     = 0;
FigScale      = 0;
FigOffset     = [ 10 10 ];
Clim          = [ 0 0 ];
HIsaturated   = 0;
HIvalue       = -1;

varargin = varargin{ 1 };

for i = 1 : length( varargin )
    cmd = varargin( i );
    cmd = cmd{ 1 };
    if ischar( cmd )
        switch cmd
            case 'figname'
                x=varargin( i+1 );
                FigName = x{ 1 }; 
                i = i + 1;
            case 'nofigure'
                MakeNewFigure = 0;
            case 'trueratio'
                TrueRatio = 1;
            case 'axisticks'
                AxisTicks = 1;
            case 'saturated'
                HIsaturated = 1;
            case 'small'
                FigScale=1;
            case 'clim'
                x=varargin( i+1 );
                Clim=x{ 1 };
                i = i + 1;
            otherwise
                disp( ['unknown command <', varargin{ i }, '>'] );
        end
    end
    
end
