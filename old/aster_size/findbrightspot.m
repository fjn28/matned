function c = findbrightspot( im, search_radius, debug )
% function c = findbrightspot( im, search_radius, debug )
%
% finds a bright spot in the image, 
% the algorithms relies on looking in circular regions of decreasing size,
% at each step, the barycenter of points in the region is calculated, and
% used as center of the next region.
%
% nedelec@embl-heidelberg.de    last modified 7 / 9 / 2002

if nargin < 3 
    debug = 0;
end

if nargin < 2 
    search_radius = [ 4, 64 ];
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

c  = barycenter1( im );

if ( debug )
        
    show( im, 'figname', 'find bright spot debug' );
    plot(c(2), c(1), 'yo', 'MarkerSize', 10, 'LineWidth', 1); 
    
end

searchR = max( search_radius );

while searchR > min( search_radius )
    
    if ( debug )
        
        cx = c(2) + searchR * cos( 0:0.1:6.3 );
        cy = c(1) + searchR * sin( 0:0.1:6.3 );
        plot( cx, cy, 'w-');
        plot(c(2), c(1), 'yx', 'MarkerSize', 10, 'LineWidth', 1); 

    end

    searchM  = maskcircle1( 2 * searchR + 1 );

    for i=1:300
   
        co = c;

        if ( debug ) plot(c(2), c(1), 'y.'); end
   
        imc   = crop( im, [ c - searchR, c + searchR ], 1 ) .* searchM;
        top   = max(max( imc ));
   
        im2   = imc + ( 1 - searchM ) * top;
        bot   = min(min( im2 ));
   
        im2   = ( imc - bot ) .* searchM;
        c   = c + barycenter1( im2 ) - ( searchR + 1 );
        
        %force to stay inside:
        c = max( [ 1, 1 ], c );
        c = min( size(im), c );
   
        %check for convergence:
        if ( round( c - co ) == [ 0  0 ] )
            break;
        end
   
    end
    
    searchR = searchR / 2;
    
end


return;