function mask=maskgrad1(n, bin)
%mask=maskgrad1(n, bin)
%
% returns a (2n+1)*(2n+1) array mask( x, y ) = (x-n-1)^2 + (y-n-1)^2
% the center is at mask(n+1,n+1) = 0
% 
% f. nedelec, nedelec@embl.de


if rem(n,1) > 0, 
   n = round(n);
   disp(sprintf('n is not an integer and has been rounded to %i',n))
end


xx  = [0:n] .^2;

quarter = ones(n+1,1) * xx + xx' * ones(1,n+1);
quarter = sqrt( quarter );
if ( exist('bin','var') ~= 0 )
    quarter = quarter ./ bin;
end

m = 2*n+1;
mask = zeros(m);
mask( n+1:m,    n+1:m )   = quarter;
mask( n+1:-1:1, n+1:m )   = quarter;
mask( n+1:-1:1, n+1:-1:1) = quarter;
mask( n+1:m,    n+1:-1:1) = quarter;

return;

