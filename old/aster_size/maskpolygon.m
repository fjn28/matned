function [ mask, roi_rect ] = maskpolygon( points, masksize )

% [ mask, rect ] = maskpolygon( points, msize )
%
% make a mask of size <msize> by filling the polygon defined 
% by the given <points> by one-valued pixels
% the points should wrap to close the polygon
%
% the function is similar to roipoly from the image processing library


nbpoints = size( points, 1 );

%----------------------  degenerate case: only one point in the list

if ( nbpoints == 1 )
   mask = 1;
   roi_rect = [ points points ];
   return;
end


%----------------------  close the polygon if that is not the case:

if any( points( 1, : ) ~= points( nbpoints, : ) ) 
    points( nbpoints+1, 1:2 ) = points( 1, 1:2 );
    nbpoints = nbpoints + 1;
end

%----------------------  get a bounding rect:

lx = floor( min( points(:, 1) ) );
ux =  ceil( max( points(:, 1) ) );

ly = floor( min( points(:, 2) ) );
uy =  ceil( max( points(:, 2) ) );

if ( nargin > 1 )
   if ( length(msize) == 1 ) masksize(2) = masksize(1); end
   lx = max( lx, 1 );
   ux = min( ux, masksize(1) );
   ly = max( ly, 1 );
   uy = min( uy, masksize(2) );
end

roi_rect = [ lx ly ux uy ];
%rect = [ lx ly size(mask,1)+lx-1 size(mask,2)+ly-1 ];
%rect                         % debug output

%----------------------  check if this is a rectangle:

lvx = min( points(:, 1) );
uvx = max( points(:, 1) );

lvy = min( points(:, 2) );
uvy = max( points(:, 2) );

if ( nbpoints == 5 ) & all ( points == [ lvx lvy; uvx lvy; uvx uvy; lvx uvy; lvx lvy ] )
    %disp('rectangle');
    mask = ones( ux-lx+1, uy-ly+1 );
    return;
end


%------------------------ offsets the points:

points(:, 1) = points(:, 1) - lx;
points(:, 2) = points(:, 2) - ly;

ux = ux - lx + 1;
uy = uy - ly + 1;

%------------------------ calculate the mask, by scanning in y 

mask = zeros( ux, uy );

for y = 1 : uy
   
   mask( :, y ) = section( points, y, ux );
    
end

return;





%----------------------------------- subfonction ---------------------------

function line = section( points, y, ux );

line = zeros( ux , 1 );

inter  = [];
nbiter = 0;

b = points( 1, : );
nbpoints = size( points, 1 );

for p = 2 : nbpoints
    
    a = b;
    b = points( p, : );
    
    slope = b(2) - a(2);
    
    %---------------------- skip horizontal lines: 
    if  slope == 0 
        continue;
    end
    
    %---------------------- skip lines above and below:
    
    if  ( y <= a(2) ) & ( y <= b(2) ) 
        continue;
    end
    
    if  ( y > a(2) ) & ( y > b(2) ) 
        continue;
    end
        
    %---------------------- find the intersection with the line at y:

    x = fix( a(1) + ( y - a(2) ) * ( b(1) - a(1) ) / slope );
    %disp(sprintf('p = %i : add (%i %i)', p, x, y ));
    %disp(sprintf(' a=(%i %i), b=(%i %i), ab = %f', a(1), a(2), b(1), b(2), ab));
    nbiter = nbiter + 1;
    inter( nbiter ) = 1 + x;
end

%disp( [ sprintf(' y = %i, x = [ 1 %i ] , ', y, ux), int2str( inter ) ] );

inter = sort( inter );
if mod( nbiter, 2 )
    
    disp('ERROR in maskpolygon: the number of intersections should be even :');
    disp( [ sprintf(' y = %i, x = [ 1 %i ] , ', y, ux), int2str( inter ) ] );
    
end


for p = 1:floor(nbiter/2)

    line( inter(2*p-1):inter(2*p) ) = 1;
    
end


return



