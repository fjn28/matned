function calc_dist;
%writes a file 'collect' containing several caracteristics of each simulation

startdir = pwd;

%open output file:
fid   = fopen( 'collect', 'w' );

fprintf( fid, '%%%s\n', date );
fprintf( fid, '%%file  lifetime   dist-min   dist-mean  dist-max  dist-dev   link-min   link-mean  link-max\n');
disp('%%file  lifetime  dist-min  dist-mean  dist-max  dist-dev   link-min  link-mean  link-max\n');


files=dir('data*');

for u=1:size(files, 1)
      
    name = files(u).name;
    nbr  = str2num(name(5:8));
    
    if  0 == exist( [name '/aspos.out'], 'file' ) 
        disp(['file ', name, '/aspos.out not found']);
        continue; 
    end
   
   
    %================== aster to aster distance
    distmin  = 0;
    distmean = 0;
    distmax  = 0;
    distdev  = 0;
    
    try,
        aspos  = load([name '/aspos.out']);
    catch,
        disp(['error loading file ', name, '/aspos.out']);
        continue; 
    end
    
    nbl     = size( aspos, 1 );    
    start   = find( aspos(:,1) > 150 );
    finish  = aspos( nbl, 1 );
   
    if isempty( start )
        disp(['file ', name, '/aspos.out is too short']);
        continue; 
    end
        
    aspos   = aspos( start:nbl, : );
    
    asd = sqrt( sum( ( aspos(:, [2 3 4]) - aspos(:, [5 6 7]) ) .^ 2 , 2) );
    
    distmin  =  min( asd );
    distmean = mean( asd );
    distmax  =  max( asd );
    distdev  =  std( asd );
    
    %================== number of links
   
    nblinks = aspos(:,8) + aspos(:,9) + aspos(:,11) + aspos(:,12);
    
    linkmin   = min( nblinks );
    linkmean  = mean( nblinks );
    linkmax   = max( nblinks );
    
    %=================print and save the data:
    s = sprintf('%04i  %9.2f %9.2f %9.2f %9.2f %9.2f    %9.2f %9.2f %9.2f', nbr, finish,... 
        distmin, distmean, distmax, distdev, linkmin, linkmean, linkmax );
    fprintf( fid, '%s\n', s);      
    
    %=================display:
    disp( s );
    
end

fclose( fid );
         
return;