function scanall;


for iii = 7 : 13;
    
    folder = sprintf('eb%i', iii);
    disp(folder);

    filename = [ folder, '/collect' ];
   
    if exist( folder, 'dir' ) & ( exist( filename, 'file' ) == 0 )
       
       cd( folder );
       mk_collect;
       cd ..;
   
   end
   
end