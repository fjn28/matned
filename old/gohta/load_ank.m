function [ dist, name ] = load_ank(dir)
%F. Nedelec, July 2005

dist = [];

if ( nargin < 1 )
    fid = fopen('ank', 'r');
    dir = pwd;
else
    if isnumeric( dir )
      dir = sprintf('data%04i', dir);
    end
    fid = fopen([dir '/ank'], 'r');
end

if ( fid == -1 )
    fprintf(1, 'file [ank] not found in %s', dir);
    return;
end

fine = 0;
while ~fine
    line = fgets(fid);
    
    if ( line == -1 )
        return;
    else
        fine = ( line(1:8) ~= 'warning:' );
    end
end


for ii=1:12
    
    [parse, count, errmsg, nextIndex ] = sscanf(line, '%s', 1);
    line = line(nextIndex:length(line));
    
    switch ii 
        case 1
            %if ( parse ~= '41' ) error('frame ~= 41'); end
        case 2
        case 3
            %if ( 0 == strcmp(parse,'k_width_worse')) error('k_width_worse'); end
            name(1) = cellstr(parse);
        case 4
            dist(1) = str2double( parse );
        case 5
            %if ( 0 == strcmp(parse,'k_width_avg')) error('k_width_avg'); end
            name(2) = cellstr(parse);
        case 6
            dist(2) = str2double( parse );
        case 7
            %if ( 0 == strcmp(parse,'cen_k_distance')) error('cen_k_distance'); end
            name(3) = cellstr(parse);
        case 8
            dist(3) = str2double( parse );
        case 9
            %if ( 0 == strcmp(parse,'cen_k_Y')) error('cen_k_Y'); end
            name(4) = cellstr(parse);
        case 10
            dist(4) = str2double( parse );
        case 11
            %if ( 0 == strcmp(parse,'cen_Y')) error('cen_Y'); end
            name(5) = cellstr(parse);
        case 12
            dist(5) = str2double( parse );

    end

end

return;
