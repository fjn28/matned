function data = analysekf;
% this if for vale21


ankf = load('an');
params = load('params');

cnt = 0;

good_ones = 0;
bad_ones = 0;

for ii = 1:size(ankf,1)-1
    
    sim  = ankf(ii,1);
    even = ( mod(sim, 2) == 0 ); 
    cov  = ( sim+1 == ankf(ii+1, 1) );
    if ( even & cov )
        
        %check the parameters for both cases
        param_indx1 = find( params(:,1) == sim );
        param_indx2 = find( params(:,1) == sim+1 );
        if ( params(param_indx1,3) ~= params(param_indx2,3) )
            fprintf(1, 'warning: dynein concentration differ for %i and %i\n', sim, sim+1);
        end
        if ( params(param_indx2,2) ~= 0 )
            fprintf(1, 'warning: dynein concentration not zero for %i\n', sim+1);
        end
        if ( params(param_indx1,2) == 0 )
            fprintf(1, 'warning: Eb1 concentration zero for %i\n', sim);
        end

        cnt = cnt + 1;
        data(cnt, 1) = sim;
        data(cnt, 2) = ankf(ii,   2);   %focussing for dynein+ncd
        data(cnt, 3) = ankf(ii+1, 2);   %focussing for dynein alone
        data(cnt, 4) = ankf(ii+1, 3);   %min distance for dynein alone
        
        if ( data(cnt, 2) < data(cnt, 3) )
            good_ones = good_ones + 1;
        else
            bad_ones = bad_ones + 1;
        end
    end
    
end

fprintf(1, 'good_ones = %i\n', good_ones);
fprintf(1, 'bad_ones = %i\n', bad_ones);

%fitcoef = polyfit( data(:,2), data(:,3), 1 );
%fitx = [ 0 9 ];
%fity = polyval( fitcoef, fitx );


figure('Position', [10 10 600 600]);
plot( data(:,3), data(:,2), 'o', 'MarkerSize', 4);
hold on;
%plot( fitx, fity, '-' );
plot([0; 9], [0; 9], 'k:');
title( 'Dynein detaches instantly upon reaching the end of microtubules', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'Focusing by dynein alone (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'Focusing by dynein and Ncd (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );




figure('Position', [610 10 600 600]);
plot( data(:,3), data(:,2)-data(:,3), 'o', 'MarkerSize', 4);
hold on;
plot([0; 9], [0; 0], 'k:');
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'quality of focussing with dynein only (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'improvement brought by ncd (negative=improvement)', 'FontSize', 14, 'FontWeight', 'bold' );


figure('Position', [610 410 600 600]);
plot( data(:,4), data(:,2), 'o', 'MarkerSize', 4);
hold on;
plot([0; 9], [0; 0], 'k:');
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'best focussing with dynein only (lower is better)', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'focussing with ncd (low=better)', 'FontSize', 14, 'FontWeight', 'bold' );
