function [ collect, names, master ] = gohta4( files )


master = [];

if ( nargin < 1 ) | ( length(files) < 1 )
    error('empty file list specified');
end

collect = [];

u = 1;
while  ( u <= length(files) )

    if isfield( files(u), 'name' ) 
        filename = files(u).name;
    else
        x = cell2mat( files(u) );
        if isempty( x )
            break;
        end
        filename = char(x);
    end
    
    if isdir( filename ) & ( filename(1) ~= '.' )

        dataNb        = sscanf(filename, 'data%d');
        [dist, names] = load_ank( filename );
        params        = getParameters( filename );
        
        
        if isempty( master )
            master = params;
        else
            %diff = diffParameters( master, params, 1 );
        end
        
        collect( u, 1:8 ) = [ dataNb, dist, params.cxmax ];

    else
        fprintf(1, '%s is not a directory', filename);
    end

    u = u + 1;
    
end

names = [ cellstr('data'), names, cellstr('ncd'), cellstr('dyn') ];
