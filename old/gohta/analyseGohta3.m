function selection = analyseGohta3(do_check);
% this is for the post-decissive screen
% July 19 in woods hole

if nargin < 1 
    do_check = 1;
end

ignore = [ cellstr('record'), cellstr('file'), cellstr('randseed'), ...
    cellstr('info'), cellstr('file'), cellstr('path') ];


selection_cnt = 0;

files  = gohta_dir;
nfiles = size( files, 1);

cnt    = 0;
data   = [];
nset   = 1;
set    = 0;
master = -1;

colors = 'rgbyk';

figure('Position', [610 10 600 600]);

plot([0.5, 5], [0.5, 5], ':', 'LineWidth', 2);
axis([0.5 5.5 0.5 5.5]);
hold on;

title( 'Model A: Eb1 carries Ncd with its motor domain active', 'FontSize', 14, 'FontWeight', 'bold' );

xlabel( 'K-distance obtained with dynein alone', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'K-distance obtained with dynein and Ncd-Eb1', 'FontSize', 14, 'FontWeight', 'bold' );

for u = 1:nfiles
    
    filename = files(u).name;
    data     = sscanf(filename, 'data%d');
    params   = getParameters( filename );
    ank      = load_ank( filename );
    if isempty( ank )
        continue;
    end
    %type([filename '/ank']); fprintf(1, 'ank(2)=%f\n', ank(2));
    
    if ( 0 == params.cxmax(1) )
        
        if exist( 'master_params', 'var' )
            dif = diffParameters( master_params, params, 1, ignore );
            if isempty( dif )
                nset = 0;
                if ( data > master+2 )
                    fprintf(1, 'same set %04i %04i\n', master, data );
                end
                master_cnt = master_cnt + 1;
                master_ank(master_cnt, 1:5) = ank;
            else
                nset = 1;
                if ( mod(data,10) ~= 0 )
                    fprintf(1, 'new  set %04i %04i :', master, data );
                    disp(dif);
                end
            end
        end

        
        if ( nset )
            set  = set + 1;
            master         = data;
            master_params  = params;
            master_cnt     = 1;
            master_ank(master_cnt, 1:5) = ank;
        end
        
    else
        
        if master < 0
            fprintf(1, 'no master defined for %s\n', filename);
            continue;
        end
        
        if ( do_check )
            dif = diffParameters( master_params, params, 1, ignore );
            if isempty( dif )
                fprintf(1, 'diff %04i %04i : same\n', master_nb, data_nb );
                plot_point = 0;
            else
                %check that the dynein is the same:
                check = ( params.cxmax(2) ~= master_params.cxmax(2) );
                check = check | ( params.haforce(3) ~= master_params.haforce(3) );
                check = check | ( params.haattachrate(3) ~= master_params.haattachrate(3) );
                check = check | ( params.asmtmax ~= master_params.asmtmax );
                if ( check )
                    fprintf(1, 'warning for: %i %i:', master_nb, data_nb);
                    disp(dif);
                    %movefile(filename, 'LOST');
                    plot_point = 0;
                else
                    plot_point = 1;
                end
            end
        else
            plot_point = 1;
        end

            
        if ( plot_point )

            mak   = master_ank(mod(data, master_cnt)+1,2);
            msize = pointSize( params.hadetachrate(1) );
            if ( msize == 0 )
                fprintf(1, '%s : %f\n', filename, rate);
            end
            color = 'k'; %colors(mod(set,5)+1);
            plot(mak, ank(2), [ color, 'o'], 'MarkerSize', msize, 'MarkerFaceColor', color);
        
        end
    end

    drawnow;
end




return;

function msize = pointSize(rate)
msize = 4;
return;
if ((  0 < rate ) & ( rate <  2 )) msize = 3; end
if ((  2 < rate ) & ( rate <  4 )) msize = 4; end
if ((  4 < rate ) & ( rate <  8 )) msize = 5; end
if ((  8 < rate ) & ( rate < 16 )) msize = 6; end
if (( 16 < rate ) & ( rate < 32 )) msize = 7; end
if (( 32 < rate ) & ( rate < 64 )) msize = 8; end
if ( 64 < rate )                   msize = 9; end
return;
