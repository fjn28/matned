function data = analyseGohta2;
% this is for the decissive screen

ignore = [ cellstr('record'), cellstr('file'), cellstr('randseed'), ...
    cellstr('info'), cellstr('file'), cellstr('path') ];


files  = gohta_dir;
nfiles = size( files, 1);

cnt  = 0;
data = [];
nset = 1;


for u = 1:10
    
    filename = files(u).name;
    data_nb  = sscanf(filename, 'data%d');
    params   = getParameters( filename );
    
    if ( 0 == params.cxmax(1) )
        
        if ( nset )
            master    = params;
            master_nb = data_nb;
            nset = 0;
        else
            dif = diffParameters( master, params, 1, ignore );
            if isempty( dif )
                fprintf(1, 'diff %04i %04i : same\n', master_nb, data_nb );
            else
                fprintf(1, 'diff %04i %04i :', master_nb, data_nb );
                disp(dif);
            end
        end
        
    else

        nset = 1;

        dif = diffParameters( master, params, 1, ignore );
        if isempty( dif )
            fprintf(1, 'diff %04i %04i : same\n', master_nb, data_nb );
        else
            fprintf(1, 'diff %04i %04i :', master_nb, data_nb );
            disp(dif);
        end
    end
end

return;


figure('Position', [610 10 600 600]);
plot( data(:,2), data(:,3), 'o', 'MarkerSize', 4);
axis([0 5 0 5]);
hold on;
title( 'Plot 2: average distance K-fibers - centrosome', 'FontSize', 14, 'FontWeight', 'bold' );
xlabel( 'quality of focussing with dynein and Ncd', 'FontSize', 14, 'FontWeight', 'bold' );
ylabel( 'quality of focussing with dynein only', 'FontSize', 14, 'FontWeight', 'bold' );


return;
