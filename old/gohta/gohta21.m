function [ collect, avg ] = gohta21()

%to analyse vale21

parent_dir = pwd;
files      = dir('data*');

collect = zeros( size(files,1), 8 );
avg     = zeros(3,6);

record_now = 0;

u = 1;
while  ( u <= size( files, 1 ) )

    cd( parent_dir );
    filename = files(u).name;
    data=sscanf(filename, 'data%i');

    if ( 1 == files(u).isdir ) & ( filename(1) ~= '.' )

        subdirname = files(u).name;
        cd( subdirname );

        dist = load_ank;
        params = getParameters;
        
        
        
        if ( params.cxmax(1) > 0 )
            if ( params.cxmax(2) > 0 )
                %both motors
                if ( params.haenddetachrate(3) > 1000 )
                    if ( dist(2) < 2 )
                        record_now = 1;
                        cxmax = params.cxmax;
                        avg(1,:) = avg(1,:) + [ dist, 1 ];
                    else
                        record_now = 0;
                    end
                end
            else
                %only ncd
                if ( record_now == 1 )
                    if ( cxmax(1) ~= params.cxmax(1) )
                        fprintf(1, 'mismatch in %s\n', filename);
                    else
                        avg(2,:) = avg(2,:) + [ dist, 1 ];
                    end
                end
            end
        else
            if ( params.cxmax(2) > 0 )
                %only dynein
                if ( record_now == 1 )
                    if ( cxmax(2) ~= params.cxmax(2) )
                        fprintf(1, 'mismatch in %s\n', filename);
                    else
                        avg(3,:) = avg(3,:) + [ dist, 1 ];
                    end
                end
            else
                fprintf(1,'simulation with no motors!');
            end
        end

        collect( u, 1:8 ) = [ data, dist, params.cxmax' ];
        
    else
        fprintf(1, '%s is not a directory', filename);
    end

    u = u + 1;
    
end

for ii=1:3
    avg(ii,1:5) = avg(ii,1:5) / avg(ii,6);
end

cd( parent_dir );
