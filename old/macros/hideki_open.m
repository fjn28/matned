function hideki( filename, im )

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = tiffread( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

if exist( 'imlog', 'var') == 0
    imlog = log( im );
end

%==================find the background level:

base1      = min(min( im ));
base2      = imbase( im, 1 );
disp( sprintf('base = %i,   %i', round(base1), round(base2) ) );

%==================select a rectangular ROI:

showim( imlog );
poly = ROImousepoly;
close;

figure('Name', 'polygon');
plot(poly(:,2), poly(:,1), 'b-');
hold on;
plot(poly(:,2), poly(:,1), 'ko');
%==================   extract the image from the ROI:

[mask, roi] = maskpolygon( poly );
showim(mask, 'figname', 'mask');

subim  = crop( im, roi );
show(subim, 'figname', 'raw picture extracted from square');
show(mask.*subim, 'figname', 'masked picture');

base3  = min(min( subim ));
subim  = mask .* subim + ( 1 - mask ) .* base2;
show(subim, 'figname', 'masked picture with base2 outside');

%==================   find the center of the aster, as the brightest spot:

%showim( subim );
center = findbrightspot( subim, [ 4, 64 ], 1 );

%==================   compute the maximum radius allowed by ROI:

radius = floor( max( [ center, roi(3:4) - roi(1:2) - center ] ) );
%disp( sprintf('radius = %i ', round(radius) ) );

%==================   compute the profile:

bin = 5;

[ val, var, area ]    = distprofile( subim, center, bin,  radius, mask );

base4 = min(val);
disp( sprintf('all should agree: min %i, base %i, roi_min %i, profile_min %i',...
    round(base1), round(base2), round(base3), round(base4) ) );

dist       = bin *( [ 1:length( val ) ] - 0.5 );
profile    = ( val - base4 ) .* area;

%==================   total mass of the aster is the sum of all pixel values within

totalmass  = 10^-5 * sum( profile );

%==================   extract the mean radius of aster:
 
profile    = profile ./ max(profile);
meanlength = bin * sum( profile );

mess1 = sprintf('sum intensity %6.1f 10^5 au\n', totalmass );
mess2 = sprintf('mean radius   %6.1f pixels\n', meanlength );
disp([ mess1, mess2 ]);

%==================   make a summary figure:

figure('Name', filename, 'Position', [ 100, 100, 800, 400 ] );
set(gca, 'Position', [0.05 0.05 0.45 0.95]);
showim( log(subim), 'nofigure', 'axisticks', 'trueratio' );

%--------------plot roi-polygon:
%plot( poly(:,2)-roi(2), poly(:,1)-roi(1), 'm-');

%--------------plot center:
plot(center(2), center(1), 'bx', 'MarkerSize', 15, 'LineWidth', 1);

%--------------plot circle of radius 'meanlength':
cx = center(2) + meanlength * cos( 0:0.1:6.3 );
cy = center(1) + meanlength * sin( 0:0.1:6.3 );
plot( cx, cy, 'b-');

%--------------plot circle of radius 'radius':
cx = center(2) + radius * cos( 0:0.1:6.3 );
cy = center(1) + radius * sin( 0:0.1:6.3 );
plot( cx, cy, 'w-');

axes( 'Position', [ 0.58 0.12 0.4 0.7 ] );
plot( dist, profile );
axis( [ 0, radius, 0, max( profile ) ] );
hold on;

text( 0, 1.2, filename, 'FontSize', 14, 'Interpreter', 'none', 'FontWeight', 'bold' );
ylabel( 'intensity  ( arbitrary units )', 'FontSize', 12 );
xlabel( 'distance   ( pixels )', 'FontSize', 12 );
plot( [ meanlength, meanlength], [ 0, max( profile ) ], 'k-' );
text( 0, 1.05, [mess1, mess2], 'FontSize', 14, 'FontName', 'fixedWidth');

%=============prepare for printing:

setprint;