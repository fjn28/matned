function scan_register_spindle;

fichier = dir('acq*.tif');

for i = 1 : size( fichier, 1 )
    
    inname = fichier(i).name;

    if ( isempty( findstr(inname, 'REG' )))
    
        disp( inname );
        register = register_spindle_click( inname );
        showim( register );
    
        outname = sprintf('RRG%s', inname );
        imwrite( register, outname, 'tif', 'Compression', 'none' );
    
    end
end