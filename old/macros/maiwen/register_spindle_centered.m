function reg = register_spindle_centered( filename, im )

% function spindle( filename, im )
% Last updated August 18, 2003

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = tiffread_buf( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

if ( isfield(im, 'data') ) 
    pixels = double( im.data ); 
end

%==================find the background level:

%rect       = ROImouserect;
%blackarea  = crop( pixels, rect );
%base1      = mean( mean( blackarea ));
base1      = min(min( pixels ));
base2      = imbase( pixels );
%disp( sprintf('base = %i,   %i', round(base1), round(base2) ) );

%==================   select a rectangular ROI:

showim( pixels );
drawnow;

%get the mouse click:
savedpointer = get(gcf, 'pointer');
set(gcf, 'pointer', 'fullcrosshair');
set(gcf, 'units', 'pixels')

click = round( ginput(1) )';
aster1 = [click(2) click(1)];

click = round( ginput(1) )';
aster2 = [click(2) click(1)];

set( gcf, 'pointer', savedpointer );

close;

%==================   extract the image from the ROI:
width = 100;

middle = ( aster1 + aster2 ) / 2 ;


left = floor( middle - width );
right = left + 2*width;

roi = [ left , right ];
subim  = ( crop( pixels, roi, 1 ) );
subim  = subim .* ( subim > 0 );
%showim(subim);
%disp(sprintf('image size is %i %i', size(subim,1), size(subim,2)));
reg = uint16( subim );

%==================   fit an ellipse to the sub-image

%axis = aster2 - aster1;

%degres = - atan2( axis(2), axis(1) ) * 180.0 / pi;
%imrot = imrotate( subim, degres, 'bilinear' );




%[cen, mom, rot, sv] = barycenter2( imrot );

%bbox = [ cen-width, cen+width ];
%reg = crop( imrot, bbox, 1 );
%reg = crop(imsiz, bbox, 1);
%reg = uint16( reg );
%showim(reg);

return;

