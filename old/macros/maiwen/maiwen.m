function fit_coef = maiwen( filename, im )

% Function to fit the phase-lifetime pictures (FLIM)
% last modified 14th April 2003

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.ipl', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = iplread( filename );
    else
        im        = filename;
        filename  = 'unknown';
    end
end

if ( isfield(im, 'data') ) 
    im = double( im.data ); 
end

mask = maskipl( size(im,1), size(im,2) );
im = im .* mask;
fig = show( im );
input('Say something smart to begin (or just type return) : ');

%============= propose a possible center:

%center = findcenterwithcircles( im, [64, 128], 1);
%plot( center(2), center(1), 'y+');
figure(fig);

%============= user click on the center:

square_radius = 20;
center = dotclick;

rect = [ center - square_radius, center + square_radius ];
x = [rect(1), rect(1), rect(3), rect(3), rect(1) ];
y = [rect(2), rect(4), rect(4), rect(2), rect(2) ];
plot(y, x);

subim = crop( im, rect );
pixel_area = size(subim, 1 ) * size(subim, 2);
value_near_center = sum(sum( subim )) / pixel_area;

disp(sprintf('value near center = %.3f', value_near_center));


%============= user click on the edge:

edge = dotclick;

rect = [ edge - square_radius, edge + square_radius ];
x = [rect(1), rect(1), rect(3), rect(3), rect(1) ];
y = [rect(2), rect(4), rect(4), rect(2), rect(2) ];
plot(y, x, 'y');

subim = crop( im, rect );
pixel_area = size(subim, 1 ) * size(subim, 2);
value_near_edge = sum(sum( subim )) / pixel_area;
disp(sprintf('value near edge   = %.3f', value_near_edge));

%============= calculate distance

center_to_edge = sqrt(sum( (center - edge) .^2 ));
max_radius = floor( center_to_edge );

drawnow;

%============= compute profile:
bin = 4;

[ raw_profile, var, area ] = distprofile( im, center, bin, max_radius, mask );

dist = bin *( [ 1:length( raw_profile ) ] - 0.5 );

fig1 = figure('Name', 'raw profile');
plot(dist, raw_profile, '.');

profile_min = min( raw_profile );
profile_max = max( raw_profile );
disp(sprintf('profile_max = %.0f', profile_max));

fit_coef = polyfit( dist, raw_profile, 2 );
fit = ( dist .^ 2 ) * fit_coef(1) + dist * fit_coef(2) + fit_coef(3);

hold on;
plot( dist, fit, 'k');
%plot( dist, area, 'k');


%=============prepare for printing:

setprint;

%=============record to file 'maiwen_excel':
if ( strcmp( filename, 'unknown' ) ) return; end
    
exel_name = strrep(filename, '.ipl', '.xls');

file = fopen(exel_name , 'w');

if ( file == -1 )
    beep;
    disp('error openning file for exel : check that it is not open in excel');
else

    disp(['writing to file ', exel_name]);
    fprintf( file, 'distance\tlifetime\tfit\n');
    
    for i = 1:size(dist, 2)
        fprintf( file, '%6.3f\t%6.3f\t%6.3f\n', dist(i), raw_profile(i), fit(i));
    end
    
    fclose( file );
end
