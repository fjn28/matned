function reg = register_spindle_click( filename, im )

% function spindle( filename, im )
% Last updated August 17, 2003

if ( nargin == 0 )
    [filename, pathname] = uigetfile('*.tif;*.stk', 'select image file');
    cd(pathname);
end

if ( nargin <= 1 )
    if ischar( filename )
        im        = tiffread_buf( filename );
    else
        im        = filename;
        filename  = 'file not specified';
    end
end

if ( isfield(im, 'data') ) 
    pixels = double( im.data ); 
end

%==================find the background level:

%rect       = ROImouserect;
%blackarea  = crop( pixels, rect );
%base1      = mean( mean( blackarea ));
base1      = min(min( pixels ));
base2      = imbase( pixels );
%disp( sprintf('base = %i,   %i', round(base1), round(base2) ) );

%==================   select a rectangular ROI:

showim( pixels );
drawnow;

%roi   = ROImouserect;
poly = ROImousepoly;
[mask, roi] = maskpolygon( poly );
%showim( mask );
close;

subim1  = crop( pixels, roi );
base3  = imbase( subim1 );
subim  = ( subim1 - base3 ) .* mask;
subim  = subim .* ( subim > 0 );

% pour les 10uM GAP/BP1 binning=1
%subim = imresize(subim, 0.5,'bilinear');

pixels = double( subim ); 

showim( pixels );
drawnow;



%get the mouse click:
savedpointer = get(gcf, 'pointer');
set(gcf, 'pointer', 'fullcrosshair');
set(gcf, 'units', 'pixels')

click = round( ginput(1) )';
aster1 = [click(2) click(1)];

click = round( ginput(1) )';
aster2 = [click(2) click(1)];

set( gcf, 'pointer', savedpointer );

close;

%==================   extract the image from the ROI:
width = 100;

middle = ( aster1 + aster2 ) / 2 ;

size_spindle = sqrt((aster1(1)-aster2(1))^2+(aster1(2)-aster2(2))^2);

left = floor( middle - width*1.5 );
right = left + 3*width;

roi = [ left , right ];

subim  = ( crop( pixels, roi ));
subim  = subim .* ( subim > 0 );

%==================   fit an ellipse to the sub-image

axis = aster2 - aster1;

degres = - atan2( axis(2), axis(1) ) * 180.0 / pi;

m = 100 / size_spindle;
imsiz = imresize(subim, m,'bilinear');

%imrot = imrotate( subim, degres, 'bilinear' );
imrot = imrotate( imsiz, degres, 'bilinear' );

[cen, mom, rot, sv] = barycenter2( imrot );

bbox = [ cen-width, cen+width ];
reg = crop( imrot, bbox, 1 );
reg = uint16( reg );
%showim(reg);
return;

