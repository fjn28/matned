% CVS: $Id: insertMat.m,v 1.2 2004/09/20 15:18:29 foethke Exp $

function mat=insertMat(matSize, insertSize, shape, data, colors)
  mat=zeros(matSize(1), matSize(2));

  % check color array
  if ~ exist('colors', 'var')
    colors = zeros(size(data,1),1);
    for k=1:size(data,1)
      colors(k) = k;
    end
  end
  
  % we always want an odd insertSize in order to avoid having to
  % think about how to place objects in the even case
  if insertSize < 1
    error('insertSize must be at least 1');
  elseif rem(insertSize,2) == 0
    insertSize = insertSize+1;
  end

  switch shape
    case 1
      insertMask=maskCircle(insertSize);
    case 2
      insertMask=maskDiamond(insertSize);
    otherwise
      error(sprintf('invalid shape: %d in insertMat!', shape));
  end      

  d = (insertSize - 1)/2;
  for k=1:size(data,1)
    % check if the insertion actually has a part inside of mat
    if    (data(k,1)+d < 1) || (data(k,1)-d > matSize(2)) ...
       || (data(k,2)+d < 1) || (data(k,2)-d > matSize(1))
      error(sprintf('insertion at (%d, %d) lies outside of matrix!', data(k,2), ...
                    data(k,1)));
    end
    
    % the insertion indices for mat
    m = data(k,2)-d;
    n = data(k,2)+d;
    a = data(k,1)-d;
    b = data(k,1)+d;
    
    % the insertion indices for the insertMask
    mIn = 1;
    nIn = insertSize;
    aIn = 1;
    bIn = insertSize;
    
    % if the insertion matrix lies partly outside of mat we have to
    % adjust all the insertion indices
    if m < 1
      mIn = 1 - m + 1;
      m=1;
    end
    if n > matSize(1)
      nIn = insertSize - (n - matSize(1));
      n=matSize(1);
    end
    if a < 1
      aIn = 1 - a + 1;
      a=1;
    end
    if b > matSize(2)
      bIn = insertSize - (b - matSize(2));
      b=matSize(2);
    end
    
    % do it!
    mat([m:n], [a:b]) =  ...
        mat([m:n], [a:b]).*(insertMask([mIn:nIn], [aIn:bIn]) == 0) ...
        + colors(k)*insertMask([mIn:nIn], [aIn:bIn]);
    %  mat([m:n], [a:b]) =  mat([m:n], [a:b]).*(insertMask == 0) + k*insertMask;
  end
  
  mat = int16(mat);
  return;
