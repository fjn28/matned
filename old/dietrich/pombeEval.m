% this function has to provide two structures:
%
% UD: this is passed on to the callback functions as userdata
%     belonging to the figure;
%     it must contain the following fields:
%     UD.markerHighColor - the highlight color for the markers
%     UD.anything else you want to pass to the callback functions
%
% PD: the actual plot data
%     PD.xData           - x coordinates of data points
%     PD.yData           - y coordinates of data points
%     PD.markerColor     - the color for each marker
%     PD.xLabel          - label for the x axis
%     PD.yLabel          - label for the y axis

function pombeEval()

  UD.dir     = '/disks/nedelec1/foethke/SCREEN_4/SAVE';
  CD.stfile  = '/stabletouch.dat';
  CD.snfile  = '/stablenotouch.dat';
  CD.unsfile = '/unstable.dat';

  PD.xLabel  = 'growth speed / {\mu}m s^{-1}';
  PD.yLabel  = 'catastrophe frequency / s^{-1}';
  
  UD.markerHighColor = 'm';


  % read data from disk
  [UD.gr,UD.shr,UD.cat,UD.res,UD.name] = textread([UD.dir CD.snfile],'%f %f %f %f %s');
  snSize         = size(UD.gr, 1);
  PD.markerColor = zeros(snSize, 3);
  for k=1:snSize
    PD.markerColor(k,:) = [1 0 0];
  end

  [gr,shr,cat,res,name] = textread([UD.dir CD.stfile],'%f %f %f %f %s');
  stSize  = size(gr, 1);
  UD.gr   = [UD.gr;   gr];
  UD.shr  = [UD.shr;  shr];
  UD.cat  = [UD.cat;  cat];
  UD.res  = [UD.res;  res];
  UD.name = [UD.name; name];
  PD.markerColor = [PD.markerColor; zeros(stSize, 3)];
  for k=snSize+1:snSize+stSize
    PD.markerColor(k,:) = [0 1 0];
  end
  
  [gr,shr,cat,res,name] = textread([UD.dir CD.unsfile],'%f %f %f %f %s');
  unsSize = size(gr, 1);
  UD.gr   = [UD.gr;   gr];
  UD.shr  = [UD.shr;  shr];
  UD.cat  = [UD.cat;  cat];
  UD.res  = [UD.res;  res];
  UD.name = [UD.name; name];
  PD.markerColor = [PD.markerColor; zeros(unsSize, 3)];
  for k=snSize+stSize+1:snSize+stSize+unsSize
    PD.markerColor(k,:) = [0 0 1];
  end
  
  PD.xData = UD.gr;
  PD.yData = UD.cat;

  plotData( PD, UD );
  return;
