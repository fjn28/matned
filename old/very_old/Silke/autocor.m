
function c = autocor(f);
L = length(f);
f = f - mean(f);
favg = mean(f);
S = sum(f.^2);
for k = 0:L-1,
   f2 = []; f1 = [];
   f2(1:L-k) = f(1+k:L);
   f1 = f(1:L-k);
   c(k+1) = sum(f1 .* f2) / S;
end;

   