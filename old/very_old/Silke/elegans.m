
% elegans

function himg = elegans(img);

ploton=0;

if (isempty(img))
   disp('must loadmovie first');
   return;
end;

% cimg = coarse-grained img box x box
[xs,ys,numimgs] = size(img);

xlim = [1 508];
ylim = [1 755];

box = 10;
step = 4;
xi = 1;
yi = 1;
%c = zeros(size([xlim(1):box:xlim(2)]'*[ylim(1):box:ylim(2)]));
figure(1); clf;
ct=exp(-1);
for x = xlim(1):step:xlim(2),
   yi = 1;
   for y = ylim(1):step:ylim(2),
      xm = min([x+box, xs]);
      ym = min([y+box, ys]);
      for i = 1:numimgs,
         f(i) = mean(mean(img(x:xm,y:ym,i)));
      end;
      cc = autocor(f);
      if ploton
         [ax,h1,h2] = plotyy(1:length(f),f,1:length(cc),cc); 
         set(ax(2),'YLim',[-1 1]);
         set(ax(1),'YLim',[50 250]);
         pause(0.001);
      end;
      %c(xi,yi,1:numimgs) = reshape(cc,[1,1,numimgs]);
      %plot(squeeze(c(xi,yi,:)));
      %pause(0.001);
      h = chartime(cc,ct);
      
      yi = yi + 1;
      himg (xi,yi)=h;
   end;
   xi = xi+ 1;
   xi
end;

figure(2);
imagesc(h);


function h = chartime(cc,frac);
h = 0 ;
i = 1;

while ~h,
   if (cc(i) <= frac)
      h = i;
   end;
   i = i + 1;
   if i > length(cc)
      h = -1;
   end;
   
end;

if h == -1
   h = length(cc) + 1;
end;
