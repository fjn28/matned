function [m, s, cnt] = computeprofile ( im, bin, center )

[sizex, sizey] = size( im );
big(1) = 1 + max( [ sizex - center(1), center(1)] );
big(2) = 1 + max( [ sizey - center(2), center(2)] );

M = sqrt ( (big(1)-1)^2 + (big(2)-1)^2 );
maxindx = 1 + floor(M/bin);

cnt = zeros(maxindx, 1);
m   = zeros(maxindx, 1);
s   = zeros(maxindx, 1);

for x = 1:sizex ;
   for y = 1:sizey ;
      d      = sqrt( ( x - center(1) ).^2 + ( y - center(2) ).^2 );
      r      = 1 + floor( d / bin );
      v      = double(im( x, y ));
      m(r)   = m(r) + v;
      s(r)   = s(r) + v^2;
      cnt(r) = cnt(r) + 1;      
   end;
end;

%removing the empty end, if any
x = size(cnt,1);
while ( cnt(x) == 0 )  x = x - 1; end   
m = m(1:x, 1);
s = s(1:x, 1);

for x = 1:size(cnt,1)
   if (cnt(x,1) ~= 0) 
      m(x) = m(x) / cnt(x);
      s(x) = sqrt( s(x) / cnt(x) - m(x)^2 );
   end
end

