function mask = maskellipse( tilt, padding )

% mask = maskellipse1(tilt, a, padding);
%
% Draw a solid ellipse of ones centered in a square of zero-valued pixels.
% the major axis of size a(1) is tilted by angle <tilt>, a(2) is minor axis size.
% optional parameter <padding> define a layer of zero pixels on the borders
%
% Example: y = imcircle(pi/4, [10,5] );
%
% F. Nedelec, may 2000; Email: nedelec@embl-heidelberg.de

if ( nargin < 2 ) padding = 2; end

if ( length( tilt ) ~= 3 )
    error('first argument should have a length of 3');
end

b    = tilt(3);
a    = tilt(2);
tilt = tilt(1);

if ( a == 0 ) | ( b == 0 )
   return;
end

cxx = ( cos(tilt)/a )^2 + ( sin(tilt)/b )^2;
cyy = ( sin(tilt)/a )^2 + ( cos(tilt)/b )^2;
cxy = cos(tilt)*sin(tilt)*( a^(-2) - b^(-2) );

xmax = ceil( sqrt( cyy / ( cxx * cyy - cxy^2 ) ) );
xmin = - xmax;

ymax = ceil( sqrt( cxx / ( cxx * cyy - cxy^2 ) ) );
ymin = - ymax;

%[xmin ymin xmax ymax ]

mask = zeros( 2*(xmax+padding), 2*(ymax+padding) );

if ( xmax == 0 ) | ( ymax == 0 )
   mask(1,1)=1;
   return;
end

for y = ymin:ymax
   del = ( y*cxy )^2 - cxx * ( y^2 * cyy - 1 );
   if ( del >= 0 )
      del = sqrt( del );
      x1  =  ceil( ( - y*cxy - del ) / cxx );
      x2  = floor( ( - y*cxy + del ) / cxx );
      mask( x1-xmin+padding:x2-xmin+padding, y-ymin+padding) = ones( x2-x1+1, 1);
   end
end

return;