function points = ROImousepoly( im )

% define a polygon by clicks on a picture
% the polygon is closed;


if ( nargin > 0 )
    showim(im);
end

drawnow;

savedpointer = get(gcf, 'pointer');
set(gcf, 'pointer', 'fullcrosshair');
set(gcf, 'units', 'pixels')
set(0, 'units', 'pixels')

n = 1;
while ( 1 )
    
    drawnow;
    k = waitforbuttonpress;
   
    %stop if key pressed or right mouse button:
    if k | strcmp( get( gcf, 'SelectionType' ) ,'alt' )
        % close the polygon:
        if ( n > 1 ) points(n, 1:2) = points(1, 1:2); end
        break;
    end 
    
    p = get(gca,'CurrentPoint');        % button down detected
    p = p(1,2:-1:1);                    % extract x and y
    
    if ( n == 1 )   %if first points is a drag --> we switch to rectangle
        finalrect = rbbox;               % return figure units
        if ( (finalrect(3) > 5) & (finalrect(4) > 5) ) % rectangle is not too small
            q = get(gca,'CurrentPoint');        % button up detected
            q = q(1,2:-1:1);                    % extract x and y
            points = corners( [ p q ] );
            for n=1:4
                plot( points(n:n+1,2), points(n:n+1,1) ); 
            end
            break;
        end
    end
    
    %loc = get(gcf, 'Position' );
    %scrn_pt = get(0, 'PointerLocation');
    %pt = [scrn_pt(1) - loc(1), loc(2) + loc(4) - scrn_pt(2)];
    %round( [pt  p ] )
    
   points(n,1:2) = p;
   if ( n > 1 )
       plot( points(n-1:n,2), points(n-1:n,1) ); 
   else
       plot( points(n,2), points(n,1), 'o' ); 
   end
   %jl=sprintf('m=get(0,''PointerLocation''); plot([%f m(2)],[%f m(1)]);',p(2), p(1));
   %set(fig, 'WindowButtonMotionFcn', jl);
   n = n + 1;
   
end

% bound the points to the size of the rect:
xmax = floor( max( get( gca, 'XLim') ) );
ymax = floor( max( get( gca, 'YLim') ) );

if ( n > 1 )  %at least one point has been clicked:
   plot( points([n-1 n],2), points([n-1 n],1) ); 
else
   points = corners( [1 1 ymax xmax ] );
end


for i=1:size(points,1)
   points(i,:) = max( points(i,:), [ 1 1 ] );
   points(i,:) = min( points(i,:), [ ymax xmax ] );
end


set( gcf, 'pointer', savedpointer );
set( gcf, 'WindowButtonMotionFcn', '');

return;

function pts = corners( rect )
  pl  = [ min( rect(1), rect(3) ), min( rect(2), rect(4) ) ];
  pr  = [ max( rect(1), rect(3) ), max( rect(2), rect(4) ) ];
  pst = zeros(5,2);
  pts(1,1:2) = pl;
  pts(2,1:2) = [pr(1), pl(2)];
  pts(3,1:2) = pr;
  pts(4,1:2) = [pl(1), pr(2)];
  pts(5,1:2) = pl;
return