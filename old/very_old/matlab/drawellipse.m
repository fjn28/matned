function mask = drawellipse( cen, tilt )

% mask = drawellipse(cen, t, a);
%
% Draw a ellipse onto the current graph.
% the major axis of size a(1) is tilted by angle <t>, a(2) is minor axis size.
%
% Example: y = imcircle(pi/4, 10);
%
% F. Nedelec, may 2000; Email: nedelec@embl-heidelberg.de

if ( length( cen ) ~= 2 )
    error('first argument should have a length of 2');
end
if ( length( tilt ) ~= 3 )
    error('second argument should have a length of 3');
end

b    = tilt(3);
a    = tilt(2);
tilt = tilt(1);

if ( a == 0 ) | ( b == 0 )
   return;
end

rotation = [ cos(tilt), -sin(tilt); sin(tilt), cos(tilt) ];

for ang = 0:0.1:2*pi
    
    p = rotation * [ a * cos( ang ); b * sin( ang ) ];
    plot( cen(2)+p(2), cen(1)+p(1), 'y.', 'MarkerSize', 1 );
    
end


return;

% code from maskellipse:

cxx = ( cos(tilt)/a )^2 + ( sin(tilt)/b )^2;
cyy = ( sin(tilt)/a )^2 + ( cos(tilt)/b )^2;
cxy = cos(tilt)*sin(tilt)*( a^(-2) - b^(-2) );

xmax = ceil( sqrt( cyy / ( cxx * cyy - cxy^2 ) ) );
xmin = - xmax;

ymax = ceil( sqrt( cxx / ( cxx * cyy - cxy^2 ) ) );
ymin = - ymax;

%[xmin ymin xmax ymax ]

for y = ymin:ymax
   del = ( y*cxy )^2 - cxx * ( y^2 * cyy - 1 );
   if ( del >= 0 )
      del = sqrt( del );
      x1  =  ceil( ( - y*cxy - del ) / cxx );
      x2  = floor( ( - y*cxy + del ) / cxx );
      plot( cen(2) + y, cen(1) + x1,  'y.');
      plot( cen(2) + y, cen(1) + x2,  'g.');
   end
end

return;