function base = imbase( im, debug )
% base = imbase( im, debug )
%
% find the pixel-value of the background,
% defined by the first minimum from the left in the pixel value histogram.
% plot histogram is debug is given
% F. Nedelec

if ( isfield( im, 'data') ) im = im.data; end

h = imhist( im );


%================= calculate a gaussian filter:

%coef = [ 1 1 1 2 3 4 3 2 1 1 1 ];
%coef = [ 1 1 1 1 1 2 2 3 4 5 6 5 4 3 2 2 1 1 1 1 1 ];
s = 10;
x= [ -2*s:2*s ];
coef = exp( -( x / s ) .^2 );
coef = coef ./ sum( coef );

%================= apply it:

hs = convs1( h, coef );


%======================= find the first maximum

ch = cumsum( hs );
hl = min( find( ch >= 20 ));


[ mv, u ] = max( hs );

base = hl;
while ( hs( base + 1 ) > hs( base ) ) & ( base < u )
    base = base + 1 ; 
end

if ( isempty( base ) ) base = min(min(im)); end

if ( nargin > 1 )
    %graphical output for debugging purpose:

    figure(333);
    clf;
    plot(h, 'b')
    hold on;
    plot(hs, 'k');
    plot(base, hs(base),'ko');
    plot(l, hs(l), 'bx');
    plot(u, hs(u), 'bx');

end

