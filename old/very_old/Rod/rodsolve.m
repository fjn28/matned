function rodsolve

%definition of the springs:
spk = [ 1  1]';               % stiffness
spa = [ -1 0 ;  1 0 ];        % attached position, on the solid
spg = [  1 0 ; -1 0 ];        % grafted position, on the ground

%initial position of the solid:
initc = [ 0 0 0 1 ];             %[ x y dx dy ] : position, direction

%mobility of the solid, here a rod of length L
visc  = 0.1;
L     = 2;
hydro = log( 2 / 0.025 );

mu = hydro / ( 4*pi*visc*L);
et = 3*hydro / (pi*visc*L^3);

%computing the coefficients:

K = 0;
alp = 0;
bet = 0;
A = zeros(1,2);
G = zeros(1,2);

for i=1:length(spk)
   
   K   = K + spk(i);
   alp = alp + spk(i) * vect( spa(i,:), spg(i,:) );
   bet = bet + spk(i) * scal( spa(i,:), spg(i,:) );
   A   = A + spk(i) * spa(i,:);
   G   = G + spk(i) * spg(i,:);
   
end


dt = 1e-3;

tmax = 1;

%solving the motion:
%options=odeset('OutPutFcn','OdePlot','MaxStep',1,'RelTol',1e-4);
%figure(1)
%tspan = [ 0 tmax ];
%p = [ K alp bet A G et mu ];
%[t,rod] = ode23tb('rodode',tspan,initc,options,p);
nbsteps = tmax / dt;
x = initc(1:2);
d = initc(3:4);
%nbsteps = 10;

for i=1:nbsteps
   
   [ f, mom ] = RodForce(K, alp, bet, A, G, x , d);
   
   f = f .* mu .* dt;
   mom = mom * et .* dt;
   
   x = x + f;
   s = mom;
   c = 1 - mom^2/2;
   
   dxn = c * d(1) - s * d(2);
   dyn = c * d(2) + s * d(1);
   d = [ dxn dyn ];
   
   rod(i,1:4) = [ x d ];
   t(i) = i * dt;
end



%display the motion
figure('Position',[ 200, 200, 500, 500]);
clf;
set(gca,'Position',[0 0 1 1]);
for i=1:10:size(rod,1)
   drawrod( rod(i,:), spg, spa );
   hold on;
end


return

function v = normf(a)
v = sqrt( a(:,1).^2 + a(:,2).^2 );
return

function v = vect(a,b)
v = a(1)*b(2) - a(2)*b(1);
return

function v = scal(a,b)
v = a(1)*b(1) + a(2)*b(2);
return


function [ f, mom ] = RodForce(K, alp, bet, A, G, x , d)
dx = d(1);
dy = d(2);
y = x(2);
x = x(1);

mom  = dx * ( alp - A(1)*y + A(2)*x ) - dy * ( bet - A(1)*x - A(2)*y );
f(1) = G(1) - dx * A(1) + dy * A(2) - K * x;
f(2) = G(2) - dy * A(1) - dx * A(2) - K * y;

return