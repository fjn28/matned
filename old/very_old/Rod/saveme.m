
function sol = saveme();
%try to solve the equation for movement of a solid under all the springs

%maple('restart');

%eq1 = 'diff(z(t),t) = cos(z(t)) * ( a - Ax*y(t) + Ay*x(t) ) - sin(z(t)) * ( b - Ax*x(t) - Ay*y(t) )';
mom = 'dx(t) * ( a - Ax*y(t) + Ay*x(t) ) - dy(t) * ( b - Ax*x(t) - Ay*y(t) )';
eq1 = 'diff(dx(t),t) = -( mom ) * dy(t)';
eq2 = 'diff(dy(t),t) =  ( mom ) * dx(t)';

eq1 = strrep( eq1, 'mom', mom );
eq2 = strrep( eq2, 'mom', mom );

%eq3 = 'diff(x(t),t) = Gx - cos( z(t) ) * Ax + sin( z(t) ) * Ay - K * x(t) ';
%eq4 = 'diff(y(t),t) = Gy - sin( z(t) ) * Ax - cos( z(t) ) * Ay - K * y(t) ';
eq3 = 'diff(x(t),t) = Gx - dx(t) * Ax + dy(t) * Ay - K * x(t) ';
eq4 = 'diff(y(t),t) = Gy - dy(t) * Ax - dx(t) * Ay - K * y(t) ';

cmd = sprintf('sol := dsolve({%s,%s,%s,%s},{x(t),y(t),dx(t),dy(t)})',...
    eq1, eq2, eq3, eq4);
%cmd = sprintf('sol := dsolve({%s,%s},{x(t),y(t)})', eq2, eq3);
cmd

maple(cmd);
maple('solx := sol[1];')
maple('soly := sol[2];')
maple('soldx := sol[3];')
maple('soldy := sol[4];')

maple('simplify(solx);')
%maple('simplify(soly);')
%maple('simplify(solz);')
