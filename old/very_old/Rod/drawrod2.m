function drawrod2( rod, sp, color );

%plot( [ rod(1), rod(1)+rod(3) ], [ rod(2), rod(2)+rod(4) ], 'b-', 'LineWidth', 3 );

nbrod = size(rod,1) / 4;

for r=1:nbrod
   x(1) = rod(r);
   x(2) = rod(r+nbrod);
   d(1) = rod(r+2*nbrod);
   d(2) = rod(r+3*nbrod);
   
   e1 = x-d;
   e2 = x+d;
   
   plot( [e1(1), e2(1)] , [ e1(2), e2(2) ], color, 'LineWidth', 1 );
   hold on;
   plot( e1(1), e1(2) , [color 'o'], 'MarkerSize', 11 );
   plot( e2(1), e2(2) , [color 'o'], 'MarkerSize', 11 );
   %plot( x, 'kx', 'LineWidth', 2, 'MarkerSize',11 );
end

for i=1:size(sp,1)
   
   r1 = sp(i,2);
   e1(1) = rod(r1)       + sp(i,3) * rod(r1+2*nbrod);
   e1(2) = rod(r1+nbrod) + sp(i,3) * rod(r1+3*nbrod);
   
   r2 = sp(i,4);
   e2(1) = rod(r2)       + sp(i,5) * rod(r2+2*nbrod);
   e2(2) = rod(r2+nbrod) + sp(i,5) * rod(r2+3*nbrod);
   
   plot( [e1(1), e2(1)] , [ e1(2), e2(2) ], 'k-', 'LineWidth', 2 );
   
end   

