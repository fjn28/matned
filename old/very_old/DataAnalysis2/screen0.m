function screen( data, indx )

if ( size( indx, 2 ) ~= 3 ) return; end


global plotcnt;
plotcnt = 0;

axisX = indx(1);
axisY = indx(2);
color = indx(3);

name  = 1;
dstart = 1;

for u=1:size(data, 1)
   
   if ( data(u, color) ~= name )
      dend = u-1;
      if ( dend > dstart ) 
         dx = data( dstart:dend, axisX );
         dy = data( dstart:dend, axisY );
      else
         disp(['problem with  ' num2str( name )]);
      end
      dstart = u;
      
      screenplot( dx, dy, name );
      
      name = data( u, color );
      
   end
      

end

return;