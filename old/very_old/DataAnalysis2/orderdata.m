function ordered = orderdata( data )

%order the data so that the first motor is the plus-ended

ordered = data;

for i = 1:size( data, 1 )
   
   if ( ordered(i,8) > ordered(i,7) )   %swap
      
      ordered(i, [3, 5, 7, 9, 11, 13 ] ) = data( i, [4, 6, 8, 10, 12, 14] );
      ordered(i, [4, 6, 8, 10, 12, 14] ) = data( i, [3, 5, 7, 9, 11, 13 ] );
      
   end   
end
