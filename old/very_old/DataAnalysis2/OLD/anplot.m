function val = anplot(param, data, linespec)

color = linespec(1);
global averageplot  pindx  dindx;
val = '';

if ( averageplot )
   
   uniq  = sort( unique( param(:, pindx) ) );
   data2 = zeros( size(uniq) );
   p     = zeros( size(uniq) );
   
   for u=1:length(uniq)
      sel  = find ( param(:, pindx) == uniq(u) );
      data2( u ) = mean( data(sel, dindx) );
      val = [ val ' ' num2str( length(sel) ) ];
   end
   
   plot( uniq, data2, linespec,...
      'MarkerFaceColor','w', 'LineWidth', 2, 'MarkerSize', 11 );
   hold on;
   %plot( param(:,pindx), data(:,dindx), linespec,...
   %   'LineWidth', 1, 'MarkerSize',6 );
else
   
   %linespec=linespec+'-';
   plot( param(:,pindx), data(:,dindx), linespec,...
      'MarkerFaceColor','w', 'LineWidth', 3, 'MarkerSize', 15 );
   
end

hold on;

return;