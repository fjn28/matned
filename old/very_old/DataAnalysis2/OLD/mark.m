function mark(param, x, indx, val, which)

figname=['mark param(:,' num2str(indx) ') == ' num2str(val)];
figure('Name',figname);
hold on;


sel = find( param(:,indx) == val );

if ( length(sel) == 0 ) 
   disp('empty, choose below'); 
   disp([num2str(unique(param(:,indx)))]);
   return
end
anplot( param, x , 'k.', which );
anplot( param(sel,:), x(sel,:) , 'bx', which );


return