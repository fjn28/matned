function  andata( p, d )

%p = cat(1, p1, p2);
%d = cat(1, d1, d2);
%p(:,4)=p(:,4)/2500;
%dx(1:359)=d(:,9)+d(:,10)+d(:,11);

Colors='kkkkkk';
Shapes='oso^+';

figure;

indx = [1 2 3];
val  = [1 0.5 2.5 ; 1 5 2.5; 0.2 0.5 2.5 ; 0.2 0.5 0.5; 0.2 0.5 0.05];
%val  = [1 0.5 1; 1 5 1; 0.2 0.5 1; 1 0.5 0.25]; 


for i=1:size(val,1)
   
   sel = [];
   for j=1:size(p,1)
      
      %size( p(j,indx) == val(i,:) )
      match = ( p(j,indx) == val(i,:) );
      if ( all( match ) )
         %[ p(j,:) size( allsel )]
         sel = union( [ j ], sel);
      end
      
   end
   
   ci = 1 + mod(i-1, length(Colors));
   cs = 1 + mod(i-1, length(Shapes));
   if ( val(i,1) == 1 )
      linespec = ['-' Colors(ci) Shapes(cs)];   
   else
      linespec = [':' Colors(ci) Shapes(cs)];   
   end
   
   if ( length( sel ) )
      
      vals='';
      for u=1:size(val,2)
         vals = [vals sprintf('%8.2f  ', val(i,u))];
      end
      
      avg = anplot( p( sel, : ), d( sel, : ), linespec );
      
      disp([linespec ' :' vals  ' (' num2str(length(sel)) ' :' avg,')']);
   end
  
end

textsize=18;
set(gca,'XScale','log');
axis([0.05 2.5 0 120]);
xlabel('motor density','FontSize',textsize);
set(gca,'XTick',[0.062 0.125 0.25 0.5 1 2]);

ylabel('microtubules in asters','FontSize',textsize);
return;

