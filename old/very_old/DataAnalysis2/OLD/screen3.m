function sel = screen3( data, indx, data2, nb )

if ( size( indx, 2 ) ~= 3 ) return; end


global plotcnt;
plotcnt = 0;

sel    = zeros( 0, 1 );
selcnt = 0;

axisX = indx(1);
axisY = indx(2);
color = indx(3);

name  = 1;
dstart = 1;

sname = sprintf('s%03i', nb );
fid   = fopen( sname, 'w' );


for u=1:size(data, 1)
   
   if ( data(u, color) ~= name )
      dend = u-1;
      if ( dend > dstart ) 
         dx = data( dstart:dend, axisX );
         dy = data( dstart:dend, axisY );
         dstart = u;
                    
         ldx = size( dx, 1 );
         hldx = floor( ldx / 2 );
         dym = dy( hldx:ldx );
         
         dymmax = max( dym );
         dymmin = min( dym );
         
         if ( dymmax < 0 )
            tmp = dymmax;
            dymmax = -dymmin;
            dymmin = -tmp;
         end
         
         test1 = ( dymmax < 2 );
         test2 = ( dymmin > 1.1 );
         test3 = ( dymmax - dymmin ) < 0.4;
         
         pline = find( data2(:,1) == name );
         if ( size( pline ) ~= [ 1, 1 ] )
            disp(['screen3:: problem on data2 ' num2str( name )] );
         end
         
         test4 = ( data2(pline, 2) > 50 );
         test5 = ( data2(pline, 4) > 2*data2(pline, 3) );
         
         %save the selection:
         fprintf( fid, '%04i %i %i %i %i %i\n',...
            name, test1, test2, test3, test4, test5);
         
         if ( test1 & test2 & test3 & test4 & test5 )
            %screenplot( dx, dy, name );
            selcnt = selcnt + 1;
            sel( selcnt, 1 ) = name;
            sel( selcnt, 2 ) = 2 - ( dymmax + dymmin )/2;   %the overlap
         end
         
         name = data( u, color );
      else
         disp(['problem with  ' num2str( name )]);
      end
     
   end
      

end

fclose( fid );
         
return;