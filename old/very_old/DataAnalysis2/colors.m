function colors( data, indx )

if ( size( indx, 2 ) ~= 1 ) return; end

axisX = indx(1);
axisY = size(data, 2);

figure('Name',['axis: ' num2str(axisX) ',' num2str(axisY) ]);

colorcodes='rgbkm';
shapecodes='xo*+s^v<>';

others = setdiff( [1:size(data,2)], [axisX axisY] );
nbothers = size(others, 2);

uniq = unique( data(:, others), 'rows' );

for u=1:size(uniq, 1)
   j=0;
   for i=1:size(data,1)
      if  all( data(i, others) == uniq(u,:) )
         j = j + 1;
         ds(j,:) = data(i, :);
      end
   end

   cc = mod(u, length(colorcodes));   
   cs = ( u - cc ) / length(colorcodes);
   cs = mod(cs, length(shapecodes));
   
   linespec = [colorcodes(cc+1) shapecodes(cs+1) '-'];
   
   val='';
   for z=1:nbothers
      val = [ val sprintf('%8.2f ', uniq(u, z))];
   end
   leg(u, 1:length(val)) = val;
   disp([val ' : ' num2str(linespec)]);
   
   if ( j )
      plot( ds(1:j, axisX), ds(1:j, axisY) , linespec,'LineWidth', 2, 'MarkerSize', 10 );
      hold on;
   end
   legend( leg );
end
