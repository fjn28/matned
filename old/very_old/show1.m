function figid = show1 ( im, textcode, varargin )
% show picture in gray level

if ( exist('textcode','var') == 0 ) textcode='scale 8bit '; end
if ( isfield(im,'data') ) im = im.data; end

imsize = size( im );

if ( 0 == doit( textcode, 'figdone' ) )

    scrsz = get(0,'ScreenSize');
    scrsz = scrsz(3:4) - [ 10 50 ];
    figscale  = min( scrsz  ./ imsize );
    
    if ( figscale > 1 )  figscale = floor(figscale); end
    if ( figscale > 1 )  figscale = 1; end
    if ( figscale < 1 )  figscale = 1 / ceil( 1/figscale ); end
    figpos = [10 40 figscale*imsize(2) figscale*imsize(1)];
    if ( doit( textcode, 'small') )
        figscale = ceil( max( [ 100 100 ] ./ imsize) );
        figpos = [10, 1000-figscale*imsize(1), figscale*imsize(2), figscale*imsize(1) ];
    end
    
    figname = ['scale ', num2str( figscale ) ];
    
    figid  = figure('Name', figname, 'Position', figpos, 'MenuBar','None');
    set(figid,'Units','pixels');
    set(gca,'Position',[0 0 1 1]);
    
end

if ( 1 == doit( textcode, 'raw') ) 
   image( im );
else
   image( rebin(im, textcode) );
end

colormap( gray(256) );
axis off; 
hold on;

%================================= plot points if given:
Colors='rbgm';
for i=1:length(varargin)
   c = varargin(i);
   c = c{1};
   if size(c, 1) == 2
      cs = 1 + mod(i-1, length(Colors)); 
      linespec =strcat(Colors(cs), '+' );
      plot( c(2,:), c(1,:), linespec ,'MarkerSize',20, 'LineWidth',2);
   end
end

%highlight saturated pixels:
if ( doit( textcode, 'saturated' ) )
   cmap = gray( 256 );
   cmap(1, :) = [ 0 0 1.0 ];
   %cmap(2, :) = [ 0 0 0.8 ];
   %cmap(255, :) = [ 0.8 0 0 ];
   cmap(256, :) = [ 1.0 0 0 ];
   colormap( cmap );
end

%highlight one particular pixel value:
w = doit('highlight', textcode);
if ( w ) 
   v = sscanf(textcode(w:length(textcode)), 'highlight%i'); 
   cmap = gray( 256 );
   cmap(v, :) = [ 0 1 0 ];
   colormap( cmap );
end

if ( doit( textcode, 'minmaxplot' ) )
   figMinMax = figure('name','pixel MinMax','Position',[10 600 500 400],'MenuBar','none');
   plot(max(imb),'b-'); hold on;
   plot(min(imb),'b-');
end

return;

function yes = doit( name, textcode );
yes = 0;
w   = findstr(textcode, name);
if ( length( w ) ) yes=w; end
return;