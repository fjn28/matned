function center = centercompute1 ( im , level, debugflag )

%compute the center of the aster, 
%from the barycenter of the high luminosity

if (exist('level','var') == 0) level=0.5; end

[sizex, sizey] = size( im );
center = zeros(2,1);

ax = sum( im, 2 );
base = level*max( ax ) + (1-level)*mean(ax);
bx = ax .* round( ax .* ( 0.5/base ) );
center(1) = round( ( [1:sizex] * bx ) / sum( bx, 1) );


ay = sum( im, 1 );
base = level * max( ay ) + (1-level) * mean(ay);
by = ay .* round( ay .* ( 0.5/base ) );
center(2) = round( (  by * [1:sizey]' ) / sum( by, 2) );

if ( exist('debugflag','var') == 1 )
   figure('Name','centercompute debug window (1)');
   hold on
   plot(ax);
   plot([0, sizey], [ base, base],'r-');
   plot([center(1) center(1)],[0, base],'k-');
   
   figure('Name','centercompute debug window (2)');
   hold on
   plot(ay);
   plot([0, sizey], [ base, base],'r-');
   plot([center(2) center(2)],[0, base],'k-');

end;


return;