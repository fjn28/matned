
function imgOut=imsubtract(imgIn1, imgIn2, a)
% imsubtract = (imgIn1 - a*imgIn2).  returns histeq'd uint8

imgTemp=double(imgIn1)-a*double(imgIn2);

ImgMax=max(imgTemp(:));
ImgMin=min(imgTemp(:));


imgOut=(imgTemp-ImgMin)/(ImgMax-ImgMin);
