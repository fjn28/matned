function [imsel, imall] = analysecross( seld, alld, p, q )

nbbin = 4;

nbsel = size(seld, 1);
nball = size(alld, 1);
   
mn = [ min( alld(:,p) ), min( alld(:,q) ) ];
mx = [ max( alld(:,p) ), max( alld(:,q) ) ];
de = ( mx - mn ) ./ nbbin;

if ( sum( de == 0 ) ) return; end
   
figure('Name',[ num2str(p), 'x', num2str(q) ] );
plot( seld(:,p), seld(:,q), 'b.' );

return;
   
imall = zeros( nbbin, nbbin );

for i=1:size(alld,1)
   x = ( [ alld(i, p), alld(i, q ) ] - mn ) ./ de;
   x = floor( x ) + 1;
   x = max( x, [ 1, 1] );
   x = min( x, [nbbin, nbbin] );
   imall( x(1), x(2) ) = imall( x(1), x(2) ) + 1;
end   
   

imsel = zeros( nbbin, nbbin );

for i=1:size(seld,1)
   x = ( [ seld(i, p), seld(i, q ) ] - mn ) ./ de;
   x = floor( x ) + 1;
   x = max( x, [ 1, 1] );
   x = min( x, [nbbin, nbbin] );
   imsel( x(1), x(2) ) = imsel( x(1), x(2) ) + 1;
end   
   
%show( imall );
%show( imsel );
show1( imsel ./ imall, 'scale8bit', [num2str(p) 'x' num2str(q)] );
hold on;
