function scan;

sdir = pwd;
folders = [ 'std20' ; 'std21' ; 'std22'; 'std34' ; 'std35'; 'std36' ];

for u=1:size(folders, 1 )
   
   name = folders(u, :);
   
   disp( name );
   
   cd( name );
   
   screenit;
   selectdynamic;
   
   cd( sdir );
   
end