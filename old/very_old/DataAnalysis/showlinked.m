function showall;

global selcnt;
selcnt = 0;

screen = load('screen');
params = load('tel');

for u=1:size(screen, 1)
      
      nbr  = screen(u, 1);
      name = sprintf('data%04i', nbr );
      
      distmin    = screen( u, 2 );
      distmean   = screen( u, 3 );
      distmax    = screen( u, 4 );
      minlinks   = screen( u, 5 );
      meanlinks  = screen( u, 6 );
      intralinks = screen( u, 7 );
      paralinks  = screen( u, 8 );
      antilinks  = screen( u, 9 );
   
      if ( minlinks > 5 )
         selcnt = selcnt + 1;
      
         npwd = strrep( pwd, '\', '/' );
         webaddress = sprintf( 'file:///%s/%s/last.gif', npwd, name);
      
         %=================display the positions of the asters:
         %screenplot( asd(:,1 ) , dist / 7, u );
         plotasterpos( name );
         
         web( webaddress, '-browser' );
         
         %=================find the parameters:
         
         pline = find( params(:,1) == nbr );
         if ( size( pline ) == [ 1, 1 ] )
            speeds = sprintf(' speeds = %.2f %.2f ', params( pline, 9), params( pline, 10 ) );
         else
            disp(['cannot find ' name ' in <tel>' ]);
         end

         disp( [ name, speeds, ', minlinks = ' num2str( minlinks ) ] );
         
         pause(2);
      end
end
         
return;