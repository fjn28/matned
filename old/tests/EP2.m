function  EP2

L = 50;
dt = 0.1;
a = dt * 0.05;
d = dt * 0.1;
h = dt * 1.5;
r  = a / ( a + d );

figure;
plot( [ 1 L ], [ r r ], 'k-' );
hold on;
axis( [1 L 0 1 ] );

for sigma = 0:1
    
de = d + h * ( 1 - sigma );

s = zeros(L+1, 1);

steps = 10000;

for i = 1 : steps 

    attach  = a .* ( 1 - s );
    detach  = d .*  s;
    %detach( L ) = de * s( L );
    s = s + attach - detach;

    move  = h .* s(1:L) .* ( 1 - s( 2:L+1 ) );
    s = s - [ move ; 0 ] + [ 0 ; move ];
    s( L+1 ) = sigma;
    
end

if ( sigma > r )
    plot( s, 'k^' );
else
    plot( s, 'bd' );
end
drawnow;

end