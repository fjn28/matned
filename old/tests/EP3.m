function  EP3

L = 50;
dt = 0.01;
a = dt * 0.05;           % attachment rate
d = dt * 0.1;            % detachment rate
h = dt * 1.5;            % hopping rate
r  = a / ( a + d );      % predicted occupation ignoring hopping

figure;
plot( [ 1 L ], [ r r ], 'k-' );
hold on;
axis( [1 L 0 1 ] );

for sigma = 0:1
    
de = d + h * ( 1 - sigma );

s = zeros(L+1, 1);

steps = 10000;

for i = 1 : steps 

    attach  = a .* ( 1 - s );
    detach  = d .*  s;
    s = s + attach - detach;

    move  = h .* s(1:L) .* ( 1 - s( 2:L+1 ) );
    s = s - [ move ; 0 ] + [ 0 ; move ];
    s( 1 ) = 0;
    s( L+1 ) = sigma;
    
end

if ( sigma > r )
    plot( s, 'k^' );
else
    plot( s, 'bd' );
end
drawnow;

end