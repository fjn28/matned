function xforce(x, quad)
% 3 = right, 2 = top, 1 = bottom, 0 = left

left  = min(x(:,1));
right = max(x(:,1));
step  = 0.25;
scale = 0.02;

y      = sqrt( x(:,4).^2 + x(:,5).^2 );
x(:,4) = y .* ((x(:,4) > 0)*2-1);

%the angle
a = atan2( y, x(:,3) );

hold off;
mov = 0;
for d=left+step:step:right
    
    i  = find( ( x(:,1) > d-step ) & ( x(:,1) <= d ) );
    xs = x(i,:);
    as = a(i,:);
    
    %---------- all
    r = find( xs(:,2) > 0 );
    
    hold off;
    plot(d,0,'o');
    hold on;
    plot(xs(r,3),xs(r,4), 'r.');
    plot(xs(r,3)+xs(r,6)*scale,xs(r,4), 'r.');
    text(-2,-60,sprintf('x=%.2f',d));
    axis([-16 16 -16 16]);
    
    mov = mov + 1;
    F(mov) = getframe;
end

for d=right:-step:left-step
    
    i  = find( ( x(:,1) > d-step ) & ( x(:,1) <= d ) );
    xs = x(i,:);
    as = a(i,:);
    
    %---------- all
    l = find( xs(:,2) < 0 );

    hold off;
    plot(d,0,'o');
    hold on;

    plot(xs(l,3),xs(l,4), 'g.');
    plot(xs(l,3)+xs(l,6)*scale,xs(l,4), 'g.');
    text(-2,-60,sprintf('x=%.2f',d));
    axis([-16 16 -16 16]);
    
    mov = mov + 1;
    F(mov) = getframe;
        
end


movie(F,50);