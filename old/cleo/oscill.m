function oscill(x, four)

step=0.1;
figure;
hold on;

left  = min(x(:,1));
right = max(x(:,1));


if nargin > 1
    
    if ( four == 3 )
       for h = left:step:right;
           in = ( h-step < x(:,1) ) & ( x(:,1) <= h );
           cp = find(in(:,1) & x(:,2)>0);
           if ~isempty(cp)
               vp = mean(x(cp,2));
               plot(h, vp, 'rx');
           end
           cm = find(in(:,1) & x(:,2)<0);
           if ~isempty(cm)
               vm = mean(x(cm,2));
               plot(h, vm, 'bx');
           end
           drawnow;
       end
       axis([left right -1 1]);
       return;
    end
    
    y=zeros(size(x,1),2);
    y(:,1)=sqrt( x(:,4).^2 + x(:,5).^2 );
    y(:,2)=sqrt( x(:,7).^2 + x(:,8).^2 );
    ang=atan2(x(:,3), y(:,1));
    quad=(x(:,3)>y(:,1))+(x(:,3)>-y(:,1));
end


mag = 50000;
axis([left right -mag mag]);

for h = left:step:right;
    
    if nargin > 1
        in = ( h-step < x(:,1) ) & ( x(:,1) <= h ) & ( quad(:,1) == four );
    else
        in = ( h-step < x(:,1) ) & ( x(:,1) <= h );
    end
    
    cp = find(in(:,1) & x(:,2)>0);
    if ~isempty(cp)
        fp = -sum(x(cp,6));
        plot(h, fp, 'r.');
    end
    
    cm = find(in(:,1) & x(:,2)<0);
    if ~isempty(cm)
        fm = -sum(x(cm,6));
        plot(h, fm, 'b.');
    end
    drawnow;
    
end

plot([left right], [0 0], '-');