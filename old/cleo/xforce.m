function xforce(x, quad)
% 3 = right, 2 = top, 1 = bottom, 0 = left

left  = min(x(:,1));
right = max(x(:,1));
step  = 0.25;

figure;
hold on;

q = ( x(:,3)>x(:,4) ) + 2*( x(:,3) > -x(:,4));

for d=left+step:step:right
    
    i  = find( ( x(:,1) > d-step ) & ( x(:,1) <= d ) );
    xs = x(i,:);
    qs = q(i,:);
    
    
    if nargin < 2
        %---------- all
        r = find( xs(:,2) > 0 );
        l = find( xs(:,2) < 0 );
        plot(d,-sum( xs(r,6) ),'r.');
        plot(d,-sum( xs(l,6) ),'g.');
    else
        %------top
        topr = find(( qs(:,1) == quad ) & ( xs(:,2) > 0 ));
        topl = find(( qs(:,1) == quad ) & ( xs(:,2) < 0 ));       
        plot(d, -sum( xs(topr,6) ),'r.');
        plot(d, -sum( xs(topl,6) ),'g.');
    end
    
end
    
if nargin >= 2
   switch(quad)
       case 0 
           title('left');
       case 1
           title('bottom');
       case 2
           title('top');
       case 3
           title('right');
       otherwise
           title('forces');
   end
end