function showfig( sel );


linespec = '';
plotcnt  = 0;
olddir = '';
mes='';

if ( nargin < 1 )
    sel = load('sel');       %   load the selection of files
end

for u=1:size(sel, 1)
         
    if ( size(sel, 2 ) == 2 )
        
        folder = sel{ u, 1 };
        if 0 == findfolder( folder )
            return;
        end
        nbr = sel{ u, 2 };
        getparams = ( pwd ~= olddir );
        
    else
        
        nbr = sel( u, 1 );
        getparams = ( u == 1 );
        
    end
    
    if ( getparams )
        if exist('tel', 'file')
            params = load('tel');
        else
            clear params;
        end
        olddir = pwd;
    end
    
    name=sprintf('data%04i', nbr);
    
    %=================load the params:
    
    if exist('params', 'var') == 1
        
        psize = size( params, 2 );
        pline = find( params(:,1) == nbr );
        
        if ( size( pline ) ~= [ 1, 1 ] )
            mes = ['cannot find ' name ' in <tel>' ];
        else
            %km = params( pline, 2 );
          
            %special1 = params( pline, psize-11 );
            %special2 = params( pline, psize-10 );
            
            speed1 = params( pline, psize-7  );
            speed2 = params( pline, psize-6 );
            
            %pend1 = params( pline, psize-1 );
            %pend2 = params( pline, psize );
            
            %specials = sprintf(' specials = %i %i ', special1, special2 );
            speeds = sprintf(' speeds = %.2f %.2f ', speed1, speed2 );
            %pends  = sprintf(' pends = %.2f %.2f ', pend1, pend2 );
            %rigid  = sprintf(' km = %.2f', km );
            
            mes = speeds;
        end
    end
    
    %=================display the positions of the asters:
    %plotasterpos( nbr );
    %pause(3);
    
    %linespec = plotasterdist( nbr, plotcnt );
    plotasterpos( nbr );
    
    drawnow;
    
    disp( [ pwd '   ' name, '   ', mes , ' : ', linespec ] );
    %wait = input( [ name ' ( press return )'], 's' );

    
    plotcnt  = mod( plotcnt + 1 , 3 );
    if  plotcnt == 0
        disp('press any key or click to continue (s to stop)');
        if 1 == waitforbuttonpress
            if 's' == get(gcf, 'CurrentCharacter')
                return;
            end
        end
    end
    
    
end
         
return;