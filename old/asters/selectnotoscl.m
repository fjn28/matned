function sel = selectnotoscl;

selcnt = 0;

%open output file:
oscl   = load( 'oscl' );
screen = load( 'screen' );

fid   = fopen( 'sel', 'w' );
fprintf( fid, '%%%s\n', date );

for u=1:size( oscl, 1 );
   
    nbr = oscl( u, 1 );
    name = sprintf('data%04i', nbr );
    
    nboscl = oscl( u, 2 );
            
    %============================= screen
    
    pline = find( screen(:,1) == nbr );
    if ( size( pline ) ~= [ 1, 1 ] )
        disp(['cannot find ' name ' in <screen>' ]);
    else
        distmin    = screen( u, 2 );
        distmean   = screen( u, 3 );
        distmax    = screen( u, 4 );
        distdev    = screen( u, 5 );
        
        linkmin    = screen( u, 6 );
        linkmean   = screen( u, 7 );
        linkintra  = screen( u, 8 );
        linkpara   = screen( u, 9 );
        linkanti   = screen( u, 10 );
      
      
        if ( nboscl < 3 ) & ( linkmin > 1 ) 
          
            fprintf( fid, '%04i\n', nbr );
            selcnt = selcnt + 1;
            sel( selcnt, 1 ) = nbr;
            
            disp( [ name , ' oscil. = ', num2str(nboscl) ] );
        end
    end

end
         
fclose( fid );

return;