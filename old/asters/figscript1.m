


s1 = selectpool( 'S1', '1dhetero' );
p1 = getdata( s1 );

s2 = selectpool( 'S2', '1dhetero' );
p2 = getdata( s2 );

%s2h = selectallS5;
%p2h = getdata( s2h );

disp([' S1      found ', num2str( size(s1,1) ) ]);
disp([' S2      found ', num2str( size(s2,1) ) ]);
%disp([' S2-hold found ', num2str( size(s2h,1) ) ]);



%initialize graph for concentration:

fig=figure('Name','speeds','Position',[30 50 600 450]);
setprint;

textsize=14;

%plot experimental data:
axis([0 1 -1 0 ]);
set(gca, 'box', 'on');

hold on;
plot([0 1], [0 -1], 'k--' );

plot(p1(:,9), p1(:,10), 'k.','MarkerSize', 8);
plot(p2(:,9), p2(:,10), 'bo', 'MarkerSize', 6);
%plot(p2h(:,9), p2h(:,10), 'r+', 'MarkerSize', 5);

xlabel('Speed of plus-end directed motor ( um/s )','FontSize',textsize);
ylabel('Speed of minus-end directed motor ( um/s )','FontSize',textsize);
set(gca,'FontSize', 14)