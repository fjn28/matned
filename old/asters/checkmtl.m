function  [l, z] = checkmtl


z = zeros(100,1);

for filenb=1:400
   
   name=sprintf('data%04i/mtl.out', filenb);
   ok=dir(name);
      
   if ( size(ok, 1) > 0 )
      x = load(name);
      z = z + x(:,2);
      if ( 0 == exist( 'l', 'var' ) )
         l = x(:,1);
      end
   end
   
end;


sumz = sum( z );
z = z / sumz;

plot(l,z);

mtl = sum( z .* l ) / sum( z );
disp([ 'mtl = ', num2str( mtl ) ]);

clear list;

return;
