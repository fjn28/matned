function data = getseldata();

sel = load('sel');
params = load('tel');

data = zeros( size(sel,1), size(params, 2 ) );

for u=1:size(sel, 1)
      
      nbr  = sel(u, 1);
       
      pline = find( params(:,1) == nbr );
      
      if ( size( pline ) ~= [ 1, 1 ] )
          disp(['cannot find ' name ' in <tel>' ]);
      else
          
          data( u, : ) = params( pline, : );
              
      end

end
         

return;