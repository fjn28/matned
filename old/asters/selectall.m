function sel = selectall;

selcnt = 0;

screen = load('screen');

%open output file:
fid   = fopen( 'sel', 'w' );
fprintf( fid, '%%%s selectall\n', date );

files=dir('data*');

for u=1:size(files, 1)
      
      name = files(u).name;
      nbr  = str2num(name(5:8));
      
%      pline = find( screen(:,1) == nbr );
%      if ( size( pline ) ~= [ 1, 1 ] )
%          disp([ 'selectall : cannot find <', name, '> in screen'] );
%          continue;
%      end  
                 
      fprintf( fid, '%04i\n', nbr );
      selcnt = selcnt + 1;
      sel( selcnt, 1 ) = nbr;

end
         
fclose( fid );

return;