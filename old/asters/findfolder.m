function ok = findfolder( folder )
% try to set current directory to this folder, searching on the different disks
% but always in folder /nedelec 

ok = 0;

%----------------- check if we are already at the right place:

curdir = pwd;
if strcmp( folder, curdir(12:length(curdir) ) ) > 0
    ok = 1;
    return;
end

%------------------ first try in current folder:

if ( exist( folder, 'dir') == 7 )

    cd( folder );
    ok = 1;
    return;
    
end

%----------------- try on the current disc, on /nedelec

%----------------- move up to /nedelec
if length( curdir ) >= 10
    cd( curdir(1:10) );
end

if ( exist( folder, 'dir') == 7 )

    cd( folder );
    ok = 1;
    return;
    
end

%------------------ try all disks:
       
for disk = 0:4
    trythis = [ char( 'H' + disk ), ':\nedelec\', folder ];
    if ( exist( trythis, 'dir' ) == 7 )
        cd( trythis );
        ok = 1;
        return;
    end
end
    
disp( sprintf( 'WARNING : could not find <%s>', folder ) );

return;