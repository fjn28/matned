function linespec = plotasterdist( nbr, plotcnt )

if nargin < 2
    plotcnt = 0;
end

filename = sprintf('data%04i/aspos.out', nbr);

if exist(filename, 'file') == 0
    return; 
end

aspos = load(filename);

dx   = aspos(:, 1);
if all( 0 == aspos(:,3 ) )
    dim  = 1;
    dy   = ( aspos(:, 2) - aspos(:, 5) ) ./ 15;
else
    dim  = 2;
    dv   = aspos(:, 2:4) - aspos(:, 5:7);
    dy   = sqrt( sum( dv .^ 2, 2 ) ) ./ 7;
end

fig=findobj('Tag', 'plotasterdist' );
if isempty( fig )
    scrsz = get(0, 'ScreenSize');
    figure('Tag', 'plotasterdist', 'Position', [1 scrsz(4)/2 scrsz(3) scrsz(4)/2-20], 'MenuBar', 'none');
    setplot( 1000, dim );
end

if  plotcnt == 0
    setplot( 1000, dim );
end

colorcodes = 'grbk';
linewidths = [ 3, 3, 3, 3, 4 ];

ci = 1 + mod( plotcnt-1, length(colorcodes));   
lw = linewidths( ci );

switch mod( fix( (plotcnt-1)/length(colorcodes) ), 3)
case 0
   linespec = [colorcodes(ci) '-']; 
   lw = 7;
case 1
   linespec = [colorcodes(ci) '--'];
   lw = 3;
case 2
   linespec = [colorcodes(ci) ':']; 
end
      
%==============================plot:

plot( dx, dy , linespec, 'LineWidth', lw, 'MarkerSize', lw );
            
return;

%==============================tag:

sx  = size(dx, 1);
sxl = sx / 20;
sxr = sx - sxl;
tag( 1, 1:2 ) = [ dx(1)-25,    dy(1)  ];
tag( 2, 1:2 ) = [ dx(sx)+10,   dy(sx) ];
nbtag = 2;
[val indx] = max( dy );
if ( indx > sxl ) & ( indx < sxr )
    nbtag = nbtag + 1;
    tag( nbtag, 1:2 ) = [ dx( indx ),  val+0.05 ];
end
[val indx] = min( dy );
if ( indx > sxl ) & ( indx < sxr )
    nbtag = nbtag + 1;
    tag( nbtag, 1:2 ) = [ dx( indx ),  val-0.05 ];
end

for i=1:nbtag
    text( tag(i,1), tag(i,2) , num2str( nbr ), 'FontSize', 8);
end

return;


function setplot( hsize, dim )
clf;
set(gca, 'Position', [0.05 0.12 0.9 0.8]);
%set(gca, 'YTick',[]);
if ( dim == 1 )
    axis([ 0 hsize -2 2]);
    set(gca,'YTick',[-2;-1;0;1;2]);
    set(gca,'YTickLabel', [-2;-1;0;1;2], 'FontSize', 30);
    hold on;
    plot( [ 0 hsize ], [ -1 -1 ] ,'k-');
    plot( [ 0 hsize ], [ 0 0 ] ,'k-');
    plot( [ 0 hsize ], [ 1 1 ] ,'k-');
else
    axis([ 0 hsize 0 3]);
    set(gca,'YTick',[0;1;2]);
    set(gca,'YTickLabel', [0;1;2], 'FontSize', 30);
    hold on;
    plot( [ 0 hsize ], [ 2 2 ] ,'k-');
    plot( [ 0 hsize ], [ 1 1 ] ,'k-');
    plot( [ 0 hsize ], [ 0 0 ] ,'k-');

end

return;