function netsolve
%simulate a string of points, of fixed length and given rigidity

%dynamic of a string of points with fixed length, and rigidity
N        = 10;        %nb of points
L        = 1;         %length of the bonds:
km       = 5;

rigid    = 0;        %rigidity
rkm      = rigid / ( L ^ 3 );
fkm      = 1;         %feed back coefficient

dt       = 1e-2;
nbsteps  = 2000;
rec      = 10;

%mobility of the rods, of length L
visc  = 0.1;
mu    = 1;

%additional springs:
sp(1,1:5) = [ 5 1  1 2 -1 ];  

figure('Position',[ 200, 200, 800, 800], 'MenuBar','None');
set(gca,'Position',[0 0 1 1]);


%vector x contains [x,y] of all successive points

x  = zeros(N*2, 1);          %positions
F  = zeros(N*2, 1);          %external applied forces
FI = zeros(N*2, 1);          %internal forces due to rigidity
FV = zeros(N*2, 1);          %internal forces due to inextensibility

%initial position is straight:
LN = L * N;
for i=1:N
   t = ( i-(N+1)/2 ) * L ;
   x(2*i-1:2*i) = [ t, 0];
end

substep = round( nbsteps / rec );
sav=1;

%start the timer:
tic

%integrating the motion:
for t=0:nbsteps
   
   %display the position
   if ( rem( t, substep ) == 0 )
      plot( x(1:2:2*N-1), x(2:2:2*N), '-ko');
      axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
      hold on;
      %plot the force vectors:
      j=1;
      for i=1:N
         plot( [ x(2*i-1), x(2*i-1) + j*F(2*i-1) ],...
            [ x(2*i), x(2*i) + j*F(2*i)], '-k', 'Linewidth', 3);
         
         plot( [ x(2*i-1), x(2*i-1) + j*( FI(2*i-1) ) ],... 
            [x(2*i), x(2*i) + j*( FI(2*i) )], '-b', 'Linewidth', 3);
         
         plot( [ x(2*i-1), x(2*i-1)+j*FV(2*i-1) ],...
            [x(2*i), x(2*i)+j*FV(2*i)], '-r', 'Linewidth', 3);
      end
      pause(1);
      hold off;
      %saving position:
      sol(sav, 1:2*N) = x';
      sav = sav + 1;
   end
   

   %add a external force translating the mt:
   F = zeros(N*2,1);
   F(2) = -1;
   
   %reset internal forces:
   FI = zeros(N*2,1);
   
   %add the rotation rigidity term:
   for i=0:N-3
      dx = 2*x(2*i+[3:4]) - x(2*i+[1:2]) - x(2*i+[5:6]);
      FI(2*i+[1:2]) = FI(2*i+[1:2]) + dx;
      FI(2*i+[3:4]) = FI(2*i+[3:4]) - 2*dx;
      FI(2*i+[5:6]) = FI(2*i+[5:6]) + dx;
   end
   
   FI = FI * rkm;
      
   %building the jacobian of length constraints:
   J = zeros(N-1, 2*N);
   for r=1:N-1
      J(r, 2*r-1) = x(2*r-1) - x(2*r+1);
      J(r, 2*r)   = x(2*r)   - x(2*r+2);
      J(r, 2*r+1) = x(2*r+1) - x(2*r-1);
      J(r, 2*r+2) = x(2*r+2) - x(2*r);
   end   
   
   %J*J' is symetric definite positie
   JJJJ = J' * inv(J * J') * J;
   
   %finding the lagrange multipliers:
   %l = pcg( J * J', J * ( F + FI ) );
   %FI = FI - J' * l;
   
   FI = FI - JJJJ * ( F + FI );
   
   %the sum of all interior forces is zero, because J is :
   %SFI = [ sum( FI(1:2:2*N) ), sum( FI(2:2:2*N) ) ];
   %FI(2*N-1)
   
   %adding the feed-back term on the length constraints:
   FV = zeros(2*N,1);
   for i=0:N-2
      dx = x(2*i+[3:4]) - x(2*i+[1:2]);
      d  = sqrt( dx(1)^2 + dx(2)^2 );
      FB = dx * ( d - L ) / d;
      FV(2*i+[1:2]) = FV(2*i+[1:2]) + FB;
      FV(2*i+[3:4]) = FV(2*i+[3:4]) - FB;
   end
   FV = FV * fkm;
   
   %Forward Euler:
   x = x + ( F + FI + FV ) .* ( mu * dt );
   
end

toc

figure('Position',[ 200, 200, 800, 800], 'MenuBar','None');
set(gca,'Position',[0 0 1 1]);
for i=1:sav-2
   for j=1:N
      plot( [ sol(i,j*2-1), sol(i+1,j*2-1)], [sol(i,j*2), sol(i+1,j*2)], '-ko');
      hold on;
   end
end
%axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
%plot the force vectors:


dis1 = sqrt( ( sol(:,4) - sol(:,2) ) .^ 2 + ( sol(:,3) - sol(:,1) ) .^ 2 );
dis2 = sqrt( ( sol(:,6) - sol(:,4) ) .^ 2 + ( sol(:,5) - sol(:,3) ) .^ 2 );
dis3 = sqrt( ( sol(:,8) - sol(:,6) ) .^ 2 + ( sol(:,7) - sol(:,5) ) .^ 2 );

figure(111);
plot(dis1);
hold on;
plot(dis2);
plot(dis3);
hold off;