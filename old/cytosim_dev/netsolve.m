function netsolve
%simulate a string of points, of fixed length and given rigidity

%dynamic of a string of points with fixed length, and rigidity
N        = 10;        %nb of points
L        = 1;         %length of the bonds:
km       = 10;        %stiffness
rigid    = 10;        %rigidity
rkm      = rigid / ( km * L ^ 3 );

dt       = 1.5e-3;
nbsteps  = 1000;
rec      = 100;

%mobility of the rods, of length L
visc  = 0.1;
mu    = 1;

%additional springs:
sp(1,1:5) = [ 5 1  1 2 -1 ];  

figure('Position',[ 200, 200, 800, 800], 'MenuBar','None');
clf;
set(gca,'Position',[0 0 1 1]);



x = zeros(1,N*2);
f = zeros(1,N*2);

for i=0:N-1
   %initial position of the solid:
   x(2*i+1:2*i+2) = [ i*L, 5];
   %initial force:
   f(2*i+1:2*i+2) = [ 0, 0 ];
end

%solving the motion:
substep = round( nbsteps / rec );

for t=1:nbsteps
   
   %compute the forces due to extension:
   for i=0:N-2
      dx = x(2*i+[3:4]) - x(2*i+[1:2]);
      d  = sqrt( dx(1)^2 + dx(2)^2 );
      if ( d > 2*L ) 
         disp('numerical instability');
         return;
      end
      d  = ( d - L ) / d;
      F  = dx * d;
      f(2*i+[1:2]) = f(2*i+[1:2]) + F;
      f(2*i+[3:4]) = f(2*i+[3:4]) - F;
   end
   
 
   %add the rigidity term:
   for i=0:N-3
      dx = 2*x(2*i+[3:4]) - x(2*i+[1:2]) - x(2*i+[5:6]);
      F  = dx * rkm;
      f(2*i+[1:2]) = f(2*i+[1:2]) + F;
      f(2*i+[3:4]) = f(2*i+[3:4]) - 2*F;
      f(2*i+[5:6]) = f(2*i+[5:6]) + F;
   end
   
   f = f .* km;
   
   
   %add a closing force:
   i  = N-1;
   dx = x(2*i+[1:2]) - x(1:2);
   f(1:2) = f(1:2) + dx;
   f(2*i+[1:2]) = f(2*i+[1:2]) - dx;
   
   %and a bit in the y axis, to force buckling:
   f(2)   = f(2)   + 1;
   f(2*N) = f(2*N) + 1;
   
   
   %pulling in the middle:
   %i = N + rem(N,2);
   %f(i) = f(i) + 5;
      
   %Forward Euler:
   x = x + f .* ( mu * dt );
   
   %reset forces:
   f = zeros(1,N*2);

   %display the motion
   if ( rem( t, substep ) == 0 )
      plot( x(1:2:2*N-1), x(2:2:2*N), '-ko');
      axis( [ -1 N*L -1 N*L]);
      pause(0.1);
      %hold on;
   end
   
   %saving position:
   sol(t, 1:2*N) = x;
end


dis = sqrt( ( sol(:,4) - sol(:,2) ) .^ 2 + ( sol(:,3) - sol(:,1) ) .^ 2 );

figure(111);
plot(dis);