function con9
%simulate two string of points, of fixed length and given rigidity
%implicit integration

global L M P;

M        = 20;          %for the first mt;
P        = 20;          %second one
L        = 1;           %length of the bonds
rigid    = 10;           %rigidity of the polymer


km       = 100;        %rigidity of spring-links
v        = 1;          %speed of the links
ab1      = [ M-1, P-1] * L/2;   %positions of the links at beginning
ab2      = [ M-1, P-1] * L/2;   %positions of the links at beginning


rkm      = rigid / ( L ^ 3 );
fb       = 0.4;      %feed back coefficient for length constraint

dt       = 1e-1;
nbsteps  = 50;
rec      = 25;

%mobility of the rods, of length L
mu    = 10/L;
LN    = max(M,P) * L;
mudt  = mu * dt;

figure('Position',[ 200, 200, 800, 800], 'MenuBar','None', 'Name','Implicit');
set(gca,'Position',[0 0 1 1]);

%vector x contains [x,y] of all successive points

x  = zeros((M+P)*2, 1);          %positions of points

%initial position is an <X>:
d  = L * [ sqrt(2)/2, sqrt(2)/2 ];
for i=1:M
   x(2*i-1:2*i) = ( i - (M+1)/2 ) * d;
end

d  = L * [ sqrt(2)/2, -sqrt(2)/2 ];
for i=M+1:M+P
   x(2*i-1:2*i) = ( i - (M+(P+1)/2) ) * d;
end


%grafted links:
G(1,1)       =  x(1);
G(1,2)       =  x(2);
G(1,3)       =  0;           %abscice on the mt


%building the matrix for rigidity forces:
R1 = RigidityMatrix(M);
R2 = RigidityMatrix(P);

R = zeros( 2*(M+P), 2*(M+P) );
R(1:2:2*M, 1:2:2*M) = R1;
R(2:2:2*M, 2:2:2*M) = R1;
R(2*M+1:2:2*(M+P), 2*M+1:2:2*(M+P)) = R2;
R(2*M+2:2:2*(M+P), 2*M+2:2:2*(M+P)) = R2;
R = R * rkm;

substep = round( nbsteps / rec );
sav=0;

%start the timer:
tic

%==================================================================================

%integrating the motion:
for t=0:nbsteps
   
   %display the position
   if ( rem( t, substep ) == 0 )
      sav = sav + 1;
      plot( x(1:2:2*M-1), x(2:2:2*M), '-ko', 'LineWidth', 2, 'MarkerEdgeColor','b');
      hold on;
      plot( x(2*M+1:2:2*(M+P)-1), x(2*M+2:2:2*(M+P)), '-ks', 'LineWidth', 2, 'MarkerEdgeColor','r');
      axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);
      hold off;
      pause(0.1);
      %hold off;
      %saving position:
      sol(sav, 1:2*(M+P)) = x';
   end
   
   %moving the motors:
   ab1  = min( ab1 + v * dt, [L*(M-1)-0.01, L*(P-1)-0.01] );
   ab2  = max( ab2 - v * dt, [0.01, 0.01] );
   G(1,3) = min( G(1,3) + v * dt, L*(M-1)-0.01 );
   
   %matrix A, B defining spring (linear) forces:
   [ Ag, Bg ] = GraftedMatrix( G );
   A   = km * ( Ag );
   B   = km * ( Bg + LinkMatrix( ab1 ) + LinkMatrix( ab2 ) );
   
   %building the jacobian of length constraints:
   J = zeros(M+P-2, 2*(M+P));
   c = 1;
   for r = [ 1:M-1, M+1:M+P-1]
      J(c, 2*r + [-1, 0, 1, 2] ) = x( 2*r + [-1, 0, 1, 2] )' - x( 2*r + [ 1, 2, -1, 0] )';
      c = c + 1;
   end   
   
   %J is rectangular, and J*J' is symetric definite positive
   
   JJJJ = ( eye( 2*(M+P) ) - J' * inv( J * J' ) * J );
   %can we calculate that one in a more simple way ?
   PL = ( mudt / 2 ) .* JJJJ * ( R + B );
   
   %implicit, mid-point method:
   PL1 = inv( eye(2*(M+P)) - PL );
   PL2 =    ( eye(2*(M+P)) + PL );
      
   x = PL1 * ( PL2 * x + mudt * ( JJJJ * A ) ) ;

   if ( fb )
      %Feed-back on the length constraints:
      dx = zeros(2*(M+P), 1);
      for i = [ 1:M-1, M+1:M+P-1]
         d  = sqrt( ( x(2*i+1) - x(2*i-1) )^2 + ( x(2*i+2) - x(2*i) )^2 );
         dl = ( d - L ) / d;
         dx(2*i-1) = dx(2*i-1) + dl * ( x(2*i+1) - x(2*i-1) );
         dx(2*i)   = dx(2*i)   + dl * ( x(2*i+2) - x(2*i) );
         dx(2*i+1) = dl * ( x(2*i-1) - x(2*i+1) );
         dx(2*i+2) = dl * ( x(2*i) - x(2*i+2) );
      end      
      x = x + fb * dx;
   end
   
end
toc



%==================================================================================

%plot trajectory:
hold on;
for i=1:sav-1
   for j=1:M+P
      plot( [ sol(i,j*2-1), sol(i+1,j*2-1)], [sol(i,j*2), sol(i+1,j*2)], '-b');
   end
end
axis( [ -LN/2 LN/2 -LN/2 LN/2 ]);

%plot some distances:
dis1 = sqrt( ( sol(:,4) - sol(:,2) ) .^ 2 + ( sol(:,3) - sol(:,1) ) .^ 2 );
dis2 = sqrt( ( sol(:,6) - sol(:,4) ) .^ 2 + ( sol(:,5) - sol(:,3) ) .^ 2 );
dis3 = sqrt( ( sol(:,8) - sol(:,6) ) .^ 2 + ( sol(:,7) - sol(:,5) ) .^ 2 );

figure('Name','Implicit-distances');
plot(dis1);
hold on;
plot(dis2);
plot(dis3);
hold off;


return;

%==================================================================================


function R = RigidityMatrix( N )

R = zeros(N);

if ( N < 3 ) return; end

if ( N == 3 )
   R = [ -1 2 -1; 2 -4 2; -1 2 -1 ]; 
   return;
end

if ( N == 4 )
   R = [ -1 2 -1 0; 2 -5 4 -1; -1 4 -5 2; 0 -1 2 -1 ]; 
   return;
end

for i=1:N;     R(i, i)   = -6;  end
for i=1:N-1;   R(i, i+1) =  4; R(i+1, i) =  4;  end
for i=1:N-2;   R(i+2, i) = -1; R(i, i+2) = -1;  end
R(1:2, 1:2)     = [-1 2; 2 -5];
R(N-1:N, N-1:N) = [-5 2; 2 -1];
return;



function B = LinkMatrix( ab )

global L M P;

B   = zeros(2*(M+P));

abN = floor(ab/L);
abf = ab - L*abN;
abN = abN + [ 1, 1+M ];

indx = [ abN(1), abN(1)+1, abN(2), abN(2)+1 ];
F = [ -1+abf(1)/L, -abf(1)/L,  1-abf(2)/L,  abf(2)/L ];
w = [  1-abf(1)/L;  abf(1)/L; -1+abf(2)/L; -abf(2)/L ];

B(2*indx-1, 2*indx-1) = w * F;
B(2*indx,   2*indx  ) = w * F;

return;


function [ A, B ] = GraftedMatrix( G )

global L M P;


ab  = G(1,3);
abN = floor( ab / L );
abf = ab - L * abN;
indx = [ abN+1, abN+2 ];

F = [ -1+abf/L, -abf/L ];
w = [  1-abf/L;  abf/L ];

B   = zeros(2*(M+P));
B(2*indx-1, 2*indx-1) = w * F;
B(2*indx,   2*indx  ) = w * F;

A   = zeros(2*(M+P), 1);
A( 2*indx-1 ) = w * G(1,1) ;
A( 2*indx   ) = w * G(1,2) ;

return;