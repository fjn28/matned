function plotCatAngleSimFig(data)

% To generate a figure with cell-length and cell-width as X-axes, 
% and the angle of MT at catastrophe time, in Y-axes
% (Fig. 3D)
%
% F. Nedelec. November 2008

labels = 1;

if  nargin < 1
    data = load('res.txt');
end

% make a figure:
figure('Position', [85   505   800   420]);
set(gcf, 'Name', ['Simulation data ', pwd]);


%% Cell width

rangeW = [ 2.5 4.5   0 pi ];

sel = logical( (data(:,7) < 4.5) .* (data(:,8) > 10) );
px = data(sel,7);
py = func(data(sel,4));

axes('Position', [0.08 0.18 0.4 0.7]);

draw_fit(px, py, rangeW, 'Diameter');

%plot(rangeW(1:2), [pi/2, pi/2], 'b:');
axis(rangeW);
ylabel('Angle (radians)', 'FontSize', 16, 'FontWeight', 'Bold');
if labels
    xlabel('cell width (micro-meters)', 'FontSize', 16, 'FontWeight', 'Bold');
    set(gca, 'FontSize', 16);
else
    set(gca, 'xticklabel',[]);
end

%% Cell length

rangeL = [  10  15   0 pi ];

px = data(sel,8);
py = func(data(sel,4));


axes('Position', [0.52 0.18 0.4 0.7]);
draw_fit(px, py, rangeL, 'Length');

%plot(rangeL(1:2), [pi/2, pi/2], 'b:');
axis(rangeL);
if labels
    xlabel('Cell length (micro-meters)', 'FontSize', 16, 'FontWeight', 'Bold');
    set(gca, 'FontSize', 16);
else
    set(gca,'xticklabel',[]);
end
set(gca,'ytick',[]);

end

%% functions

function [v, l, u] = envelope_of_fits(px, py, range, msg)
v = range(1):0.1:range(2);
u = range(3)*ones(size(v));
l = range(4)*ones(size(v));

cnt = 1000;
slopes = zeros(1,cnt);

for c=1:cnt

    prob = 100 / size(px,1);
    s = logical( rand(size(px,1), 1) < prob );
    f = polyfit(px(s), py(s), 1);
    slopes(1,c) = f(1);
    y = polyval(f, v);
    u = max(u, y);
    l = min(l, y);

end

txt = sprintf('Slope %.3f +/- %.3f ', mean(slopes), std(slopes));
fprintf('%s: %s\n', msg, txt);

text(range(2), 3, txt,...
    'FontSize', 16, 'HorizontalAlignment', 'right');
    
end



function draw_fit(px, py, range, msg)


    s = logical( rand(size(px,1), 1) < 0.02 );
    plot(px(s), py(s), 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 5);
    hold on;
    
    %draw the envelope:
    [v,l,u] = envelope_of_fits(px, py, range, msg);
    plot(v, l, ':k', 'LineWidth', 1);
    plot(v, u, ':k', 'LineWidth', 1);
    
    %draw the central fit:
    fit = polyfit(px, py, 1);
    fprintf( '%10s: fit(x) = %+f x %+f\n', msg, fit(1), fit(2));
    plot(range(1:2), [polyval(fit,range(1)), polyval(fit,range(2))], '-k', 'LineWidth', 1);
    
    %text(range(2), 3, sprintf('Slope %.3f ', fit(1)),...
    %    'FontSize', 16, 'HorizontalAlignment', 'right');
end

    
%calculate the angle
function y=func(x)
    y = acos(-x);
end



