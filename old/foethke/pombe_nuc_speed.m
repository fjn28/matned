function pombe_nuc_speed(indx)
%nedelec, March 2007.plot the nucleus position, to estimate speed


figure;

color=['r', 'g', 'b', 'y', 'm', 'c', 'k'];

for ii = 1 : length(indx)
    
    i = indx(ii);
    e=load(sprintf('run%i/pombe.out',i));

    plot(e(:,1), e(:,2), color(ii), 'LineWidth', 3);
    hold on;
        
end

%plot the pseed reported by Daga et al. NCB
plot([60 600], [-3 0], 'k:');

axis([60 600 -5 1]);