function collectTraits

% function collectTraits
%
% scan all directories, calling getTraits()

fid  = fopen('traits.sta', 'w');

data  = dir('run*');

ndata = size(data, 1);

wb = waitbar(0, 'Collecting traits...');

for u = 1 : ndata
    
    if ~isempty(wb)
        waitbar(u/ndata);
    end

    if data(u).isdir
        getTraits(fid, data(u).name)
    end

end

fclose(fid);

if ~isempty(wb)
    close(wb);
end