function plotCatPosExp(s, alt)
%to plot experimental data

if nargin > 1
    scale = 0.08;
    s(:,1:3) = scale * s(:,1:3);
else
    %transform simulation coordinates to cell-ends
    s(:,1) = s(:,1) + s(:,8)/2;
end

if ( 0 )
    figure;
    plot(s(:,1), s(:,2), 'k.');
    hold on
    for n = 1:size(s,1)
        plot([s(n,1), e(n,1)], [s(n,2), e(n,2)], 'b-')
    end
    title('centered coordinates');
end


if ( 1 )
    figure;
    e = s(:,1:3) + 0.5 * s(:,4:6);

    plot(s(:,1), s(:,2), 'k.');
    hold on
    for n = 1:size(s,1)
        plot([s(n,1), e(n,1)], [s(n,2), e(n,2)], 'b-')
    end
    axis([-3 3 -3 3]);
    axis equal;
    title('cell-end coordinates');
end

if ( 1 )
    figure;
    plot(s(:,7), s(:,4), 'k.');
    axis([3 4.5 -1 1]);
    title('direction-X vs. cell width');
end
