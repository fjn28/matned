function [ C, s ] = checkTraits(T);

if  size(T,2) ~= 11
    error('size(T,2) should be 11');
end

C = -ones(size(T));

%name of parameter file
C(:,1) = T(:,1);

%trait 1: pole-catastrophes
C(:,2) = T(:,2) > 0.9;

%trait 2: cortex-catastrophes
C(:,3) = T(:,3) > 0.9;

%trait 3: nb of bundles at the poles
C(:,4) = 2 < T(:,4) & T(:,4) < 6;

%trait 4: contact times
C(:,5) = 60 < T(:,5) & T(:,5) < 100;

%trait 5: bundle-length
C(:,6) = 0.6 < T(:,6);

%trait 6: curling
C(:,7) = T(:,7) < 0.01;

%trait 7: nucleus variance
C(:,8) = T(:,8) < 0.25;

%trait 8: centering speed
C(:,9) = 0.2/60 < T(:,9)  &  T(:,9) < 0.9/60;

%trait 9: asymmetric contact times 45 +/- 25
C(:,10) = 20 < T(:,10) & T(:,10) < 70;

%trait 10: bundle centering
C(:,11) = 1.4 < T(:,11) & T(:,11) < 3.4;

end
