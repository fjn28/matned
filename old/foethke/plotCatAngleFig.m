function plotCatAngleFig(s)

% To generate a figure with cell-length and cell-width as X-axes, 
% and the angle of MT at catastrophe time, in Y-axes
% (Fig. 3D)
%
% F. Nedelec.

rangeW = [ 2.5 4.5   0 pi ];
rangeL = [  10  15   0 pi ];

figure('Position', [85   505   800   420]);


if  nargin > 0 
   
    if 1
        %select 20% of the data:
        sel = (s(:,7) < 4.5) .* (s(:,8) > 10) .* ( rand(size(s,1), 1) < 0.2 );
    else
        %select only 2% of the data:
        sel = (s(:,7) < 4.5) .* (s(:,8) > 10) .* ( rand(size(s,1), 1) < 0.02 );
    end
    
    s = s(logical(sel),:);
    
    set(gcf, 'Name', ['Simulation data ', inputname(1)]);
    plot(s(:,7), func(s(:,4)), 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 7);
    hold on;
    %plot(rangeW(1:2), [pi/2, pi/2], 'b:');
    axis(rangeW);
    ylabel('Angle (radians)', 'FontSize', 16, 'FontWeight', 'Bold');
    %xlabel('cell width (micro-meters)', 'FontSize', 16, 'FontWeight', 'Bold');
    
    set(gca, 'FontSize', 16);
    set(gca, 'xticklabel',[]);
    set(gca, 'Position', [0.08 0.18 0.4 0.7]);

    add_slope(s(:,7), func(s(:,4)), rangeW, 'Diameter');

    
    axes('Position', [0.52 0.18 0.4 0.7]);
    plot(s(:,8), func(s(:,4)), 'ko', 'MarkerFaceColor', 'w', 'MarkerSize', 7);
    hold on;
    %plot(rangeL(1:2), [pi/2, pi/2], 'b:');
    axis(rangeL);
    %xlabel('Cell length (micro-meters)', 'FontSize', 16, 'FontWeight', 'Bold');
    set(gca,'ytick',[]);
    %xlabel('Cell length (micro-meters)', 'FontSize', 16, 'FontWeight', 'Bold');
    %set(gca, 'FontSize', 16);
    set(gca,'xticklabel',[]);
    
    add_slope(s(:,8), func(s(:,4)), rangeL, 'Length')
    return;
    
end



%load converted experimental data:
wt  = load('wt.txt');
tea = load('tea1.txt');
s = cat(1, wt, tea);

plot(wt(:,7), func(wt(:,4)), 'ko', 'MarkerSize', 7);
hold on;
plot(tea(:,7), func(tea(:,4)), 'ko', 'MarkerSize', 7, 'MarkerFaceColor', 'k');
%plot(rangeW(1:2), [pi/2, pi/2], 'b:');
axis(rangeW);
ylabel('Angle (radians)', 'FontSize', 16, 'FontWeight', 'Bold');
xlabel('cell width / \mum', 'FontSize', 16, 'FontWeight', 'Bold');
set(gca, 'FontSize', 16);
set(gca, 'Position', [0.08 0.18 0.4 0.7]);

add_slope(s(:,7), func(s(:,4)), rangeW, 'Diameter');

    
    
axes('Position', [0.52 0.18 0.4 0.7]);
plot(wt(:,8), func(wt(:,4)), 'ko', 'MarkerSize', 7);
hold on;
plot(tea(:,8), func(tea(:,4)), 'ko', 'MarkerSize', 7, 'MarkerFaceColor', 'k');
%plot(rangeL(1:2), [pi/2, pi/2], 'b:');
axis(rangeL);
set(gca,'ytick',[])
xlabel('Cell length / \mum', 'FontSize', 16, 'FontWeight', 'Bold');
set(gca, 'FontSize', 16);

add_slope(s(:,8), func(s(:,4)), rangeL, 'Length');

end



function add_slope(x, y, range, msg)
    fit=polyfit(x, y, 1);
    fprintf( '%10s: fit(x) = %+f x %+f\n', msg, fit(1), fit(2));
    plot(range(1:2), [polyval(fit,range(1)), polyval(fit,range(2))], '-k');
    text(range(2), 3, sprintf('Slope %.3f ', fit(1)),...
        'FontSize', 16, 'HorizontalAlignment', 'right');
end

    
%calculate the angle
function y=func(x)
    y = acos(-x);
end