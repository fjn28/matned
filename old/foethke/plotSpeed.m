function plotSpeed(debug)

%plot nucleus centering speed

if nargin < 1
    debug = 0;
end

if debug == 1 
    fig = figure;
end


data  = dir('run*');

ndata = size(data, 1);
res   = zeros(ndata,2);

for u = 1 : ndata
    
    if data(u).isdir
        cd(data(u).name);
        
        pam = getParameters;
        res(u,1) = pam.mtdynamic;
        res(u,2) = pam.nubundlemax;
        
        bun = load('pombe.sta');
        sel = find( 600 < bun(:,1) & bun(:,1) < 800 );
        p   = polyfit( bun(sel,1), bun(sel,2), 1 );
        
        res(u,3) = -p(1);

        % trait 8: nucleus centering speed

        if debug
            if ( res(u,1) == 4 ) 
                color='b.';
            else
                color='k.';
            end
            plot(bun(sel,1), bun(sel,2), color);
            hold on;
                    
            fprintf('%s: model %i bun %i speed %f\n', data(u).name, res(u,1), res(u,2), res(u,3));
        end

        cd('..');
    end
end

if debug == 1
    xlabel('time (s)');
    ylabel('nucleus position (um)');
end


