function T = traits89(debug)
%F.nedelec, May 2007

if nargin < 1
    debug = 0;
end

T = [-1, -1];

if ~isdir('run0001')
    error('run0001 missing');
end
cd('run0001')


bun = load('pombe.sta');

% trait 8: nucleus centering speed

sel = find( 50 < bun(:,1) & bun(:,1) < 200 );
if debug
    figure;
    plot( bun(sel,1), bun(sel,2) );
end

p = polyfit( bun(sel,1), bun(sel,2), 1 );
T(1) = -p(1);
cd('..');

% trait 9: proximal and distal contact times
if ~isdir('run0002')
    return
end
cd('run0002');

system('cut -c2- mtcontacts.sta | tail -n +2 > mtcontacts.cut');
conA   = load('mtcontacts.cut');

start  = 0;
sel    = find( conA(:,2)==1 & conA(:,3)>start); % & conA(:,4)<600 );
con    = conA(sel,:);

dis    = find( con(:,5)<0 );
pro    = find( con(:,5)>0 );

disT   = mean( con(dis, 4) - con(dis, 3) );
proT   = mean( con(pro, 4) - con(pro, 3) );

if debug
    figure;
    plot( con(dis,5), con(dis,7), 'bx');
    hold on;
    plot( con(pro,5), con(pro,7), 'rx');
    axis([-7 7 -3 3]);
    hold off;
    fprintf( '%4i  bundle-catastrophes in poles:\n', size(sel,1));
    fprintf( '%4i  proximal: %.2fs\n', size(pro,1), proT);
    fprintf( '%4i  distal:   %.2fs\n', size(dis,1), disT);
end

T(2)   = proT - disT;

cd('..');

