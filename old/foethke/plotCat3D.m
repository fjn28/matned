function plotCat3D(s)

%load converted data:
wt  = load('wt.txt');
tea = load('tea1.txt');



figure;
wt_e = wt(:,1:3) + 0.5 * wt(:,4:6);
tea_e = tea(:,1:3) + 0.5 * tea(:,4:6);


plot3(wt(:,1), wt(:,2), wt(:,3), 'bv');
hold on
for n = 1:size(wt,1)
    plot3([wt(n,1), wt_e(n,1)], [wt(n,2), wt_e(n,2)], [wt(n,3), wt_e(n,3)], 'b-')
end

plot3(tea(:,1), tea(:,2), tea(:,3),'ko');
for n = 1:size(tea,1)
    plot3([tea(n,1), tea_e(n,1)], [tea(n,2), tea_e(n,2)], [tea(n,3), tea_e(n,3)], 'k-')
end

%axis([-3 3 -3 3]);
axis equal;
title('cell-end coordinates');
