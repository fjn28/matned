function T = traits127(debug)
%F.nedelec, May 2007

if nargin < 1
    debug = 0;
end

T = -ones(1, 7);

idir = pwd;
if isdir('run0000')
    cd('run0000')
end

if ~ exist('mtcontacts.sta', 'file')
    fprintf('mtcontacts.sta missing in %s\n', pwd);
    cd(idir);
    return;
else
    system('cut -c2- mtcontacts.sta | tail -n +2 > mtcontacts.cut');
    con = load('mtcontacts.cut');
end

pam    = getParameters('config.cym');
celLen = 2 * ( pam.boxsize(1) + pam.boxsize(2) );


bun    = load('pombe.sta');
mxTime = bun(size(bun,1),1);



%find bundle-catastrophes
selB   = find( con(:,2)==1 & con(:,4)>500 );
nbCat  = size(selB,1);
loc    = con(selB, 5:7);

if debug
    r = sqrt( con(:,6).^2 + con(:,7).^2 );
    figure('Units', 'pixels', 'Position', [200, 110, 900, 400]);
    set(gca, 'Units', 'pixels', 'position', [100,  80,  120*5.8,  120*2.3])

    selB   = find( con(:,2)==1 & con(:,4)>10000 );
    gray = [0.5; 0.5; 0.5];
    %plot( abs(con(selB,5)), r(selB),  'o', 'MarkerSize', 5, 'MarkerFaceColor', gray, 'MarkerEdgeColor', gray);
    plot( abs(con(selB,5)), r(selB),  'k^', 'MarkerSize', 5);
    hold on;
    selM   = find( con(:,2)==0 & con(:,4)>10000 );
    plot( abs(con(selM,5)), r(selM), 'ko', 'MarkerSize', 5, 'MarkerFaceColor', 'k');
    
    
    axis([0 +5.8 0 +2.3]);
    xlabel('|x| (\mu m)', 'FontSize', 22);
    ylabel('r = (y^2+z^2)^{1/2} (\mu m)', 'FontSize', 22);
    set(gca, 'FontSize', 20);
    set(gca, 'TickDir', 'out');
    hold off;
    
    
    figure('Position', [10, 110, 700, 300]);
    xedges = 0:0.25:6;
    yedges = 0:0.25:2.5;
    hc = hist2(abs(con(selB,5)), r(selB), xedges, yedges);
    %hc = hist2(abs(con(selB,5)), abs(con(selB,6)), xedges, yedges);
    image(xedges, yedges, hc)
    axis equal
    set(gca, 'XTick', 0:6);
    set(gca, 'YTick', 0:0.5:2.5);
end



% trait 1: percent bundle-catastrophes in cell-pole
poleC  = sum( abs(loc(:,1)) > pam.boxsize(1) );
T(1)   = poleC / nbCat;



% trait 2: percent bundle-catastrophes within 0.5 um of cortex
d      = distance(loc, pam.boxsize);
T(2)   = sum( d < 0.5 ) / nbCat;



% trait 3: number of bundles contacting the poles on average
%we sum the contact times, and divide by the total simulation time
selP   = find( con(:,2)==1 & con(:,3)>0 );

if  debug
    fprintf( '%i bundle-catastrophes\n', nbCat);
    fprintf( '%i bundle-catastrophes in poles\n', size(selP,1));
end

times  = con(selP, 4) - con(selP, 3);
T(3)   = sum(times) / mxTime;



% trait 4: contact time for bundles, in seconds
T(4)   = mean(times);

if  debug
    figure;
    %plot(times, 'x');
    x = 3:12:300;
    
    if exist('../../contactsExp.txt', 'file')
        ce = load('../../contactsExp.txt');
        fprintf('%i experimental contact times\n', size(ce,1))
        scale = size(x,2) / size(ce,1);
        he = scale * hist( 3 * ce(:,3), x);
        hold on;
        bar(x, he);
    end

    %plot the simulation data
    scale = size(x,2) / size(times,1);
    h = scale * hist(times, x);
    plot(x, h, 'k-', 'LineWidth', 4);
    
    
    xlabel('contact time (s)', 'FontSize', 20);
    ylabel('normalized density', 'FontSize', 20);
    set(gca, 'FontSize', 20);
    axis([0 250 0 4.5]);
    set(gca, 'TickDir', 'out');

end




% trait 5: length of bundles (above 1000s), in % of cell size
nB    = (size(bun,2)-2) / 3;
selL  = find( bun(:,1) > 800 );
lenB  = zeros(nB,1);
for bi=1:nB
    len(bi) = mean( abs(bun(selL, 2+3*bi) - bun(selL,1+3*bi)) );
end

T(5)  = mean(len) / celLen;



% trait 6: number of mt curling per frame
if ~ exist('curl.sta', 'file')
    if exist('result.out', 'file')
        system('~/bin/analyse2 bent > curl.sta');
    else
        fprintf( 'file result.out missing')
    end
end

if exist('curl.sta', 'file')
    curl = load('curl.sta');
    if  isempty(curl)
        fprintf( 'file curl.sta is empty')
    end
    T(6) = curl;
else
    T(6) = -1;
end



% trait 7: variance of nucleus motions
T(7) = var(bun(:,2));

cd(idir);
return;

%--------------------------------------------------------------------------


function d = distance(pos, cell)

xp=pos(:,1);
for ii=1:size(xp,1)
    if ( xp(ii) > +cell(1) ) xp(ii) = +cell(1); end
    if ( xp(ii) < -cell(1) ) xp(ii) = -cell(1); end
end

ad = sqrt( (pos(:,1) - xp).^2 + pos(:,2).^2 + pos(:,3).^2 );
d  = cell(2) - ad; 
    
    