function s = plotCatastrophes(s)

% plot the locations of catastrophes in 'mtcontacts.sta' for S.pombe simulations
%F. nedelec, Feb 2006


if ( nargin < 1 )
    system('cut -c2- mtcontacts.sta | tail -n +2 > mtcontacts.cut');
    s = load('mtcontacts.cut');
end

%select only bundle catastrophes
sb = find(s(:,2)==1);
sm = find(s(:,2)==0);

%dimensionality of simulation
dim = ( size(s,2) - 4 ) / 2;


%plot x-y:
figure('name', pwd, 'Position', [100 200 800 300]);
plot( s(sm,5), s(sm,6), 'bx');
hold on;
plot( s(sb,5), s(sb,6), 'ko');
axis([-6 +6 -2.5 +2.5]);
axis equal
title('Position of catastrophe (X-Y)');
xlabel('x'); ylabel('y');

if ( dim == 3 )
    %plot x-z:
    figure('name', pwd, 'Position', [200 300 800 300]);
    plot( s(sm,5), s(sm,7), 'bx');
    hold on;
    plot( s(sb,5), s(sb,7), 'ko');
    axis([-6 +6 -2.5 +2.5]);
    axis equal
    title('Position of catastrophe (X-Z)');
    xlabel('x'); ylabel('z');
end

if ( dim == 3 )
    %plot y-z:
    figure('name', pwd, 'Position', [200 300 400 400]);
    plot( s(sm,6), s(sm,7), 'bx');
    hold on;
    plot( s(sb,6), s(sb,7), 'ko');
    axis([-2.5 +2.5 -2.5 +2.5]);
    title('Transverse position of catastrophe (Y-Z)');
    xlabel('y'); ylabel('z');
end


%position of direction information:
indx = size(s,2) - dim + 1;

%plot directions:
figure('name', pwd, 'Position', [300 300 400 400]);
plot( s(sm,indx+1), s(sm,indx+2), 'bx');
hold on;
plot( s(sb,indx+1), s(sb,indx+2), 'ko');
axis([-1 +1 -1 +1]);
title('Transverse direction of tip at catastrophe');
xlabel('dy'); ylabel('dz');



cellBody = 3.5;
nbCatEnd = sum( abs( s(sb,5) ) > cellBody );
nbCatAll = sum( abs( s(sb,5) ) > 0 );

fprintf( 'bundle-catastrophes %i, at cell-pole: %.2f %%\n', nbCatAll, 100* nbCatEnd / nbCatAll );

%---------------- transverse information

%plot cortical distance of catastrophes:
N = size(sb,1);
%distance catastrophe location / axis center
d = sqrt( s(:,6).*s(:,6) + s(:,7).*s(:,7) );

si = find( abs(s(:,5)) < 3.5 );

figure('name', 'distance from cortex');

%all catastrophes are red
hold on;
hist(d(si));
h1 = findobj(gca,'Type','patch');

%bundle-catastrophes are red:
se = intersect( si, sb ); 
hist(d(se));
h2 = findobj(gca,'Type','patch');

%all catastrophes are blue, bundle-catastrophes are red:
set(h2,'FaceColor','r','EdgeColor','w')
set(h1,'FaceColor','b','EdgeColor','w')
