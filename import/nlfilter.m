function b = nlfilter(a,nhood,FUN,P1,P2,P3,P4,P5,P6,P7,P8,P9,P10)
%NLFILTER Local non-linear filtering.
%	B = NLFILTER(A,[M N],'fun') applies the m-file 'fun' to
%	each M-by-N sliding neighborhood of A.  The m-file 'fun'
%	should return a scalar result, c = fun(x), which is
%	the filtered value for the center pixel in the M-by-N
%	neighborhood, x.  'fun' will be called for each value in A.
%
%	Up to 10 additional parameters can be passed to the 
%	function 'fun' using
%	   B=NLFILTER(A,[M N],'fun',P1,P2,P3,...)
%	in which case 'fun' is called using 
%	   c = fun(x,P1,P2,P3,...).
%
%	At the edges, the M-by-N neighborhood is formed by padding
%	with ones if A is an indexed image or with zeros otherwise.
%
%	See also BLKPROC, COLFILT.

%	Clay M. Thompson 1-25-93
%	Copyright (c) 1993 by The MathWorks, Inc.
%	$Revision: 1.7 $  $Date: 1994/05/25 20:00:31 $

error(nargchk(3,13,nargin));

% Form call string.
params = [];
for n=4:nargin
  params = [params,',P',int2str(n-3)];
end
if ~any(FUN<48), fcall = [FUN,'(x',params,')']; else fcall = FUN; end

% Expand A
[ma,na] = size(a);
if isind(a),
  aa = ones(size(a)+nhood-1);
else
  aa = zeros(size(a)+nhood-1);
end
aa(floor((nhood(1)-1)/2)+[1:ma],floor((nhood(2)-1)/2)+[1:na]) = a;

% Apply m-file to each neighborhood of a
b = zeros(size(a));
rows = [0:nhood(1)-1]; cols = [0:nhood(2)-1];
f = waitbar(0,'Applying non-linear filter...');
for i=1:ma,
  for j=1:na,
    x = aa(i+rows,j+cols);
    b(i,j) = eval(fcall);
  end
  waitbar(i/na)
end
close(f)
