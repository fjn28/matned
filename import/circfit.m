function   [ cen, R, eq ] = circfit(x, y, make_plot)
%
%   [cen, R, eq] = circfit(x,y)
%
%   fits a circle  in x,y plane in a more accurate
%   (less prone to ill condition )
%  procedure than circfit2 but using more memory
%  x,y are column vector where (x(i),y(i)) is a measured point
%
%  result is center point (yc, xc) and radius R
%  an optional output is the vector of coeficient a
%  describing the circle's equation
%
%   X^2 + Y^2 + a(1)*X + a(2)*Y + a(3) = 0
%

if nargin < 3
    make_plot = 0;
end

%%

x = x(:);
y = y(:);

m = [ sum(x.*x), sum(x.*y), -sum(x);
      sum(x.*y), sum(y.*y), -sum(y);
      sum(x),    sum(y),    -length(x) ];

n2 = x.^2 + y.^2;
eq = m \ [ sum(n2.*x); sum(n2.*y); sum(n2) ];

cen = [ eq(1) / 2, eq(2) / 2 ];
R  = sqrt( cen(1)^2 + cen(2)^2 - eq(3) );

if make_plot > 0
    plot(x,y,'+');
    hold on;
    plot(cen(1), cen(2),'x');
    theta=0:0.1:2*pi+0.1;
    plot(cen(1)+R*cos(theta), cen(2)+R*sin(theta));
end

return;

%  Code below by:  Izhak bucher 25/oct/1991, 

a   = [x y ones(size(x))]\[-(x.^2+y.^2)];
cen = [ a(1) / 2, a(2) / 2 ];
R   =  sqrt((a(1)^2+a(2)^2)/4-a(3));

