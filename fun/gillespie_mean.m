function [m, v] = gillespie_mean(t, n)

% calculate mean and variance of the quantity
% specified by `n` at the times `t`

nt = length(t);
tot = t(end) - t(1);
dt = [ reshape(diff(t), nt-1, 1) ; 0 ];

for c = 1:size(n,2)
    m(c) = sum( dt .* n(:,c) ) / tot;
    v(c) = sum( dt .* n(:,c) .^ 2 ) / tot;
end

end