function [t, n] = gillespie_decay(par, tmax)

% A stochastic simulation of reaction kinetics.
%
% This implements a single reaction:
% A -> 0, with rate par.dA
%
% The argument `par` should be a structure providing:
% - the reaction rate: par.dA
% - the initial number of molecules: par.A
%
% The output is a vector of time points `t`, 
% and a vector `n` containing the quantity of A at every time point.
%
% Francois Nedelec, 04.2015

%% offer default parameters

if isempty(par)
    par.A = 100;
    par.dA = 1;
end

%% initialization

A = par.A;
time = 0;

t(1) = 0;
n(1) = A;

%% simulation

while 1

    % calculate rate of reaction:
    prop = A * par.dA;
    % calculate time until next reaction:
    dt = -log(rand(1)) / prop;
    
    % update time
    time = time + dt;
    
    if ( time > tmax )
        break
    end
    
    % update population:
    A = A - 1;
        
    % record time and current state
    t(end+1) = time;
    n(end+1) = A;

end


end