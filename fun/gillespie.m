function [ t, n ] = gillespie(stoch, rates, expon, initial, time_max)
% Synopsis:
%
% [ t, n ] = gillespie(stoch, rates, expon, initial, time_max)
%
% Simulate for a time `time_max` a system of reactions that is defined by a
% stoichiometry matrix, an associated set of rates and the initial abundance
% of each species. The system of reaction is simulated stochastically with 
% Gillespie's most basic method.
%
% The matrix `stoch` describes the stoichiometry of the reactions:
%    its number of lines should be the number of reaction
%    its number of column should be the number of species
%    its values are integers, with negative values indicating consumption,
%    and positive values corresponding to production
% The vector `rates` and matrix `expon` are used to calculate the speed of 
% each reaction as a function of the abundance of each species:
%       speed = rate .* ( abundance .^ expon )
% The vector `initial` specifies the initial aboundance of each species.
%
% The output is a vector of time points `t`, and a matrix `n` containing in
% each column the number of each species, at every time point.
%
%
% Example 1: a simple decay over an interval of 100 seconds:
%   A -> 0  with rate `k`
% with an initial state (time=0) with `n` molecules of A:
%
% [ t, n ] = gillespie([-1], [k], [1], [n], 100);
% plot(t, n);
%
% Example 2: a single reaction:
%   A -> B  with rate kA
% with an initial state (time=0) with nA molecules of A and 0 molecules B:
%
% [ t, n ] = gillespie([-1 1], [kA], [1], [nA 0], 100);
% plot(t, n);
% legend('A', 'B');
%
% Example 3: to simulate two reactions leading to an equilibrium:
%   A -> B  with rate kA
%   B -> A  with rate kB
%
% [ t, n ] = gillespie([-1, 1; 1, -1], [kA; kB], [1 0; 0 1], [nA, nB], 100);
% plot(t, n);
% legend('A', 'B');
%
% Copyright Francois Nedelec, 09.2014 -- 04.2015


% get number of reaction, and number of species:
[ nreac, nspec ] = size(stoch);

if length(rates) ~= nreac
    error('the rate vector (argument 2) does not match the number of reactions');
end
rates = reshape(rates, nreac, 1);

if isempty(expon)
    expon = -stoch .* ( stoch < 0 );
elseif any( size(stoch) ~= size(expon) )
    error('the exponent matrix (argument 3) does not match the stoichiometry');
end

if length(initial) ~= nspec
    error('the initial state vector (argument 4) does not match the number of species');
end

% initialize system:
time = 0;
pop  = reshape(initial, 1, nspec);
vo   = ones(nreac, 1);

% estimate total number of reactions, and pre-allocate output arrays
% (this is just to speed-up execution)
est = ceil( 2 * sum(initial) * sum(rates) * time_max );
t = zeros(est, 1);
n = zeros(est, nspec);

% set first record
rec = 1;
t(1) = time;
n(1, :) = pop;

while 1;

    % calculate propensity of each reaction:
    prop = prod(power(vo*pop, expon), 2) .* rates;
    
    % find out which reaction occurs occurs first, and when it occurs:
    [dt, ix] = gillespie_choice(prop);

    time = time + dt;
    
    if time > time_max || ix == 0
        break
    end
    
    % update population:
    pop = pop + stoch(ix, :);

    % add record:
    rec = rec + 1;
    t(rec) = time;
    n(rec, :) = pop;
    
end

% trim output to valid records:
t = t(1:rec);
n = n(1:rec, :);

end
