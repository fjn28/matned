function t = gillespie_variable_rate(dt, tmax, repeat)

% function t = gillespie_variable_rate(dt, tmax, repeat)
%
% This implements a reaction with a rate that is dependent on time.
% This rate is given by a subfunction defined inside the m-file.
%
% Arguments:
%   `dt` is the time step 
%   `tmax` is the total time
%   `repeat` is a boolean that is true if the event is recurrent
%
% The output is a vector of time points `t`, where the event fired.
%
% Francois Nedelec, 04.2015

%% offer default parameters

if nargin < 3
    repeat = 1;
end
if nargin < 2
    tmax = 30;
end
if nargin < 1
    dt = 0.1;
end

%% simulation

t = [];
time = 0;
esp = -log(rand(1));

while time < tmax

    % update time
    time = time + dt;

    % get current value of rate:
    R = rate(time);
    % decrease counter:
    esp = esp - R * dt;
    
    while ( esp < 0 )
        e = time + esp / R;
        if repeat
            % record time and current state
            t(end+1) = e;
            esp = esp - log(rand(1));
        else
            t = e;
            return
        end
    end

end

if nargin == 0
    hist(t, 100);
end

    function r = rate(t)
        r = 100 * ( sin(t) + 1 );
    end
end