function [ T, N ] = gillespie_choice(rates)

% function [ T, N ] = gillespie_choice(rates)
%
% Select stochastically the next reaction that fires,
% according to Gillespie's procedure (using `rand`).
%
% The input is a vector of reaction rates (all positive).
% The function returns the time delay T (T is positive), 
% and the index N of the next reaction (N > 0).
% If the sum of the rates is zero, it returns [T=inf, N=0]
%
% Francois Nedelec, 2012

S = sum(rates);

% Two random numbers from `rand` are used. 

if S > 0
    
    T = -log(rand(1)) / S;
    
    N = 1;
    X = rand(1) * S - rates(1);
    
    while X > 0
        N = N + 1;
        X = X - rates(N);
    end
    
else
    
    N = 0;
    T = inf;

end

end
