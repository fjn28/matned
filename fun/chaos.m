function chaos( b )
%a very simple chaotic equation

if ( nargin < 1 )
    b = 0.02;
end

step = 1e-1;
iter = 1e5;
wind = 20;

y = [ 1;0;0 ];
fig=figure( 'Position', [ 200, 400, 600, 600], 'Name', 'chaos', 'MenuBar', 'None' );
%set(fig, 'DoubleBuffer','on')
axes('Position',[0 0 1 1]);

axis( [ -wind, wind, -wind, wind ] );

hold on;

for i = 1:iter
    
    % the integrator is the worse you can get:
    dy = -b * y + sin( y([2,3,1]) );
    y  = y + step * dy;

    plot( y(1), y(2), '.', 'EraseMode', 'none', 'MarkerSize', 1 );
    drawnow;

end

