function [ res, self, info ] = spin_measure_fourier(dna, tub, opt, self)

% function [ res, self, info ] = spin_measure_fourier(dna, tub, self)
% Analyse spindle shape using Fourier coefficients
%
% F. Nedelec, March 2008


%build the info string:
if nargout > 2
    info{1} = 'OrderB';
    info{2} = 'AngleB';

    for n = 1:size(tub,3)
        info{2*n+1} = ['Order', num2str(n)];
        info{2*n+2} = ['Angle', num2str(n)];
    end
end

% Number of fourier-coefficients to consider
nFou = 10;

if isempty(self)  &&  opt.visual > 1
    mag = 2;
    W = max(size(dna));
    D = mag * W;

    figure('Name', 'Fourier', 'MenuBar','None', 'Position', [2*D+60 50 2*D, D]);
    self(1) = axes('Units', 'pixels', 'Position', [1   1 D D] );
    self(2) = axes('Units', 'pixels', 'Position', [1+D+30 30 D-50 D-50] );
end



[nPoles, angle] = analyze( dna - mean(mean(dna)), 0 );
res = [ nPoles, angle ];

for d = 1:size(tub,3)
    [nPoles, angle] = analyze( tub(:,:,d) - mean(mean(tub(:,:,d))), d );
    res = cat(2, res, [ nPoles, angle ]);
end


    function [nPoles, angle] = analyze(im, axi)
        
        cFou = fourier_image_coefficients(im, nFou);
        magn = sqrt( sum( cFou.^2, 2 ) );
        [ mx, nPoles ] = max(magn(2:nFou));

        % Extract the orientation from the phase of the 2d order coefficients
        angle = atan2(cFou(3,2), cFou(3,1)) / 2;

        if ~isempty(opt.hAxes)
            cen = ( [1,1] + size(im) ) / 2;
            %draw minor axis (should be the metaphase plate:
            ma = 30 * [ cos(angle+pi/2), sin(angle+pi/2) ];
            pt = [ cen, cen ] + [ -ma, ma ];
            plot(opt.hAxes(axi+1), pt([2,4]), pt([1,3]), 'y:');
        end
    end

if opt.visual > 1
    %we show the last time-point
    iFou = fourier_image(cFou, size(dna,1));
    show_image(iFou, 'Handle', self(1));

    cla(self(2));
    bar(self(2), 0:nFou, magn);

    text('Unit', 'normalized', 'Position', [0.5 0.9],...
        'FontSize', 16, 'FontWeight', 'bold', 'HorizontalAlignment', 'center', ...
        'String', ['Type', sprintf(' %i', nPoles)] );

    xlim(self(2), [-1 nFou+1]);
end

end


