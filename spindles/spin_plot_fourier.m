function spin_plot_fourier(opt)

% function spin_plot_fourier
%
%
% F. Nedelec, March 2008


try
    data = load('results_fourier.txt');
catch
    error('File "results_fourier.txt" not found');
end



% Produce a figure for each time-point, using colored regions to
% highlight the dectected symmetry of the structure,
rgb = 'rgbymwc';


TUB = spin_load_images('tub');

for n = 1:length(TUB)
    
    show_image(TUB(n));
    
    for p = 1:size(data,1)

        region = data(p, 1:5);
        nPoles = data(p, 6+2*n);
        angle  = data(p, 7+2*n);

        col = rgb(min(nPoles, length(rgb)));
        image_drawrect(region(2:5), [col '-'], num2str(region(1)));

        cen = 0.5 * ( region([2,3]) + region([4,5]) );
        rd = min( abs( region([4,5]) - region([2,3]) )) / 2;
        dd = rd * [ -sin(angle), cos(angle) ];  %metaphase direction
        pt = [ cen - dd, cen + dd ];
        %plot( cen(2), cen(1), 'wo');
        plot( [pt(2), pt(4)] , [pt(1), pt(3)] , [col, ':']);
    end
    
end


end
