function [ res, self, info ] = spin_measure_crown(dna, tub, opt, self)

% function [ res, self, info ] = spin_measure_crown(dna, tub, self)
% Analyse profile around the dna patch
%
% F. Nedelec, April 2008


if nargout > 2
    info{1} = 'profile';
end

if opt.auto_center == 0
    fprintf('Measure_crown called without "Auto-Center" option');
end

if opt.visual > 1
    if isempty(self)
        figure('Name', 'Profile', 'MenuBar','None');
        self = axes;
        xlabel('distance from center (pixels)');
        ylabel('fluorescence (a.u.)');
        hold on;
    else
        cla(self);
    end
end

[ c, m, v, d ] = radial_profile(dna);

if ~ isempty(self)
    plot(self, d, m, 'k--', 'LineWidth', 2);
end

for inx = 1:size(tub,3)
    
    im = tub(:,:,inx);
    
    back = opt.tub_back(inx);
    if back > 0
        im = ( im - back ) .* ( im > back );
    end
    
    [ c, m, v, d ] = radial_profile(im);

    if ~ isempty(self)
        plot(self, d, m);
    end
    
end

%quantification of the profile has to be done:    
res = 1;

    
end