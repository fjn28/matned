function spin_plot_click(opt)

% plot the mouse-clicks and calculate the length of the spindles

try
    data = load('results_click.txt');
catch
    disp('File "results_click.txt" not found');
    return;
end

sel_2clicks = ( data(:, 6) == 2 );


if 1
    %select all structures with 2 clicks:
    sel = sel_2clicks;
else
    %we only process the bipolar structures with 2 clicks
    sel_fourier = spin_select_bipolar;
    sel = sel_2clicks  &  sel_fourier(1:size(sel_2clicks,1));
end


TUB = spin_load_images('tub');

%infer the number of points that stored in the file
width = ( size(data,2) - 5 ) / length(TUB);

polpol = [];
for n = 1:length(TUB)
    
    show_image(TUB(n));    
    
    for p = 1:size(data,1)
        if sel(p)

            region  = data(p, 1:5);
            nClicks = data(p, 6+(n-1)*width);
            clicks  = data(p, (7:12)+(n-1)*width);
            
            image_drawrect(region(2:5), 'b-', num2str(region(1)));
            for x = 1:nClicks
                pt = [ clicks(2*x-1)+region(2)-1, clicks(2*x)+region(3)-1 ];
                plot(pt(2), pt(1), 'gx');
            end
            
            len = sqrt( sum( (clicks(1:2) - clicks(3:4)).^2 ));
            polpol = cat(2, polpol, len);
            
        end
    end
end

%convert from pixels to micro-meters:
polpol = TUB(1).pixel_size * polpol;


figure;
bar(polpol);
xlabel('Bipolar Structures', 'FontSize', 16, 'FontWeight', 'bold');
ylabel('Length / um', 'FontSize', 16, 'FontWeight', 'bold');

figure, hist(polpol, 20);
xlabel('Length / um', 'FontSize', 16, 'FontWeight', 'bold');
ylabel('Count', 'FontSize', 16, 'FontWeight', 'bold');

end

