function im = spin_load_images(kind, what, opt)

% im = spin_load_images(kind)
% im = spin_load_images(kind, what)
%
% Read from 'image.txt', and load images cooresponding to 'kind'
% if 'what' is numeric, load only image at index 'what'
% the returned structure has a field .data containing the pixel values
% 
%
% F. Nedelec, April 2008



if nargin < 1
    error('You must specify which kind of images to load!');
end
if nargin < 2
    what = [];
end

try
    fid = fopen('images.txt', 'rt');
    info = textscan(fid, '%s %d %s %f %f %f %d', 'CommentStyle', '%');
    fclose(fid);
catch
    error('File "images.txt" not found');
end

names = info{1};

%% find the selected images:
sel = find( strcmp(info{3}, kind) );
    
if isempty(sel)
    warning('could not find image of kind "%s" in images.txt', kind);
    im = [];
    return
end

%reduce selection if specified:
if  ~isempty(what)  &&  isnumeric(what)  &&  length(sel) > 1
    try
        sel = sel(what);
    catch
        error('Image index out of range');
    end
end

%% Load images:
im = [];
nImages = length(sel);


for n = 1:nImages

    name = char(names(sel(n)));

    im(n).file_name = name;
    im(n).indx = info{2}(sel(n));
    im(n).kind = info{3}(sel(n));
    im(n).back = info{5}(sel(n));
    im(n).time = info{4}(sel(n));
    im(n).gain = info{6}(sel(n));
    im(n).pos  = info{7}(sel(n));

    
    if isfield(opt, 'load_pixels') && ~opt.load_pixels
        continue;
    end
    
    tim = tiffread(name, 1);
    
    %copy the pixel size which is stored in the LSM meta-data
    if isfield(tim, 'lsm') && isfield(tim.lsm, 'VoxelSizeX') && isfield(tim.lsm, 'VoxelSizeY')
        if ( tim.lsm.VoxelSizeX == tim.lsm.VoxelSizeY )
            im(n).pixel_size = tim.lsm.VoxelSizeX * 1e6; %convert to micro-meters
        end
    end

    
    if iscell(tim.data)
        pix = tim.data{im(n).indx};
    else
        if im(n).indx == 1
            pix = tim.data;
        else
            tim = tiffread(name, im(n).indx);
            pix = tim.data;
        end
    end

    if isempty(pix)
        error('could not load image');
    end
    
    if nargin > 2  &&  isfield(opt, 'flatten_image')  &&  opt.flatten_image > 0
        fprintf('Correcting exposure for 3x3 image "%s"\n', name);
        pix = flatten_background(pix, [3, 3]);
        im(n).back = 0;
    end
    
    %add image to the stack
    im(n).data = double(pix);
    
end


end

