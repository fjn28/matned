function spin_master_plus

% A dialog with buttons to easily start the macros used to analyse
% spindle morphology
%
% Can be closed when the work is done
%
% F. Nedelec, Feb. 2008, March 2008


%default options:
opt = spin_default_options;


%size of the buttons
width  = 140;
height = 38;

%pick a random color
color = rand(1,3);
while sum(color) < 1
    color = rand(1,3);
end

%number of buttons
nButtons = [ 9, 3 ];

sFig = [200 200 nButtons(2)*width+20 nButtons(1)*(height+5)+50];

hFig = figure('Position', sFig,...
    'Name', 'Spin-Master-Plus', 'MenuBar', 'none', 'Resize', 'off', ...
    'NumberTitle', 'off', 'NextPlot', 'new',...
    'HandleVisibility', 'callback', 'Color', color);


%% the 'Close' button
uicontrol(hFig, 'Style', 'pushbutton', 'Tag', 'close-button', ...
    'Position', [ 15 sFig(4)-height-5 width-40 height ],...
    'String', 'Close All', 'FontSize', 17, ...
    'Callback',{@callback_close});

%% the auto_center field
rPos = [ 2*width , sFig(4)-30 ];
hCenter = uicontrol(hFig, 'Style', 'checkbox', 'BackgroundColor', color, ...
    'Position', [ rPos(1) rPos(2) 40 30 ],...
    'Min', 0, 'Max', 1, 'Value', opt.auto_center,...
    'Callback',{@callback_center});

uicontrol(hFig, 'Style', 'text', 'String', 'Center:',...
    'Position', [ rPos(1)-60 rPos(2) 60 20 ],...
    'FontSize', 15, 'BackgroundColor', color);

%% the use_mask checkbox
hMask = uicontrol(hFig, 'Style', 'checkbox', 'BackgroundColor', color, ...
    'Position', [ rPos(1) rPos(2)-20 40 30 ],...
    'Min', 0, 'Max', 1, 'Value', opt.use_mask,...
    'Callback',{@callback_mask});

uicontrol(hFig, 'Style', 'text', 'String', 'Mask:',...
    'Position', [ rPos(1)-60 rPos(2)-20 60 20 ],...
    'FontSize', 15, 'BackgroundColor', color);

%% the flatten field
hMask = uicontrol(hFig, 'Style', 'checkbox', 'BackgroundColor', color, ...
    'Position', [ rPos(1) rPos(2)-40 40 30 ],...
    'Min', 0, 'Max', 1, 'Value', opt.flatten_image,...
    'Callback',{@callback_flatten});

uicontrol(hFig, 'Style', 'text', 'String', 'Flatten:',...
    'Position', [ rPos(1)-60 rPos(2)-40 60 20 ],...
    'FontSize', 15, 'BackgroundColor', color);


%% the 'set Radius' field
rPos = [ sFig(3)-70, sFig(4)-35 ];
hRadius = uicontrol(hFig, 'Style', 'edit', 'BackgroundColor', [1 1 1],...
    'Position', [ rPos(1) rPos(2) 50 25 ],...
    'String', num2str(opt.radius), 'FontSize', 17,...
    'Callback',{@callback_radius});

uicontrol(hFig, 'Style', 'text', 'String', 'Radius:',...
    'Position', [ rPos(1)-60 rPos(2) 50 height-20 ],...
    'FontSize', 15, 'BackgroundColor', color);

%% The 'visual' menu
rPos(2) = rPos(2) - 35;
hVisual = uicontrol(hFig, 'Style', 'popupmenu', 'BackgroundColor', color,...
    'Position', [ rPos(1) rPos(2) 50 25 ],...
    'String', '0|1|2|3|4', 'Value', opt.visual+1, 'FontSize', 14, ...
    'Callback',{@callback_visual});

uicontrol(hFig, 'Style', 'text', 'String', 'Visual:',...
    'Position', [ rPos(1)-60 rPos(2) 50 height-20 ],...
    'FontSize', 15, 'BackgroundColor', color);


%% handles to the other buttons:
hButton = [];

    function button(i, j, name, action)
        hButton(i,j) = uicontrol(hFig, 'Style', 'pushbutton',...
            'Position',[ 15+width*(j-1) 10+height*(i-1)+10*fix((i-1)/2) width-10 height ],...
            'String', name, 'FontSize', 17, ...
            'Callback',{@callback, action i j});
        if strncmp(name, 'Plot', 4)  ||  strncmp(name, 'Show', 4)
            set(hButton(i,j), 'ForegroundColor', [0 0 1]);
        end
    end

%% definition of the buttons, and their actions:
button(1,1, 'Set Files',    'make_image_list;');
button(1,2, 'Edit Files',   'edit images.txt;');
button(1,3, 'Show Images',  'spin_show_images(opt);');

button(2,1, 'Set Regions',  'set_regions_grid(image_base, opt);');
button(2,2, 'Edit Regions', 'edit_regions(image_base);');
button(2,3, 'Show Regions', 'show_regions(image_base, load_regions);');

button(3,1, 'Measure DNA',  'opt=spin_measure(''dna'',opt);');
button(3,2, 'Measure Mass', 'spin_measure_mass(opt);');
button(3,3, 'Plot Mass',    'spin_plot_mass(opt);');

button(4,1, 'Calib. Beads', 'spin_measure(''beads'',opt);');
button(4,2, 'Plot Beads',   'spin_plot_beads(opt);');
button(4,3, 'Plot Calib.',  'spin_calibrated_dna(''DNA fluorescence'', 1);spin_calibrated_dna(''DNA area'', 1);');

button(5,1, 'Crown',        'spin_measure(''crown'', opt);');
button(6,1, 'Plot Crown',   'spin_plot_crown(opt)');

button(5,2, 'Fourier',      'spin_measure(''fourier'', opt);');
button(6,2, 'Plot Fourier', 'spin_plot_fourier(opt);');

button(5,3, 'Ellipse',      'spin_measure(''ellipse'', opt);');
button(6,3, 'Plot Ellipse', 'spin_plot_ellipse(opt);');

button(7,1, 'Click',        'spin_measure(''click'', opt);');
button(8,1, 'Plot Click',   'spin_plot_click(opt)');

button(7,2, 'Show Panels',  'spin_plot_panels(opt);');

drawnow;

%% Callbacks

    function callback_close(hObject, eventdata)
        if strcmp( get(hObject, 'String'), 'Close All')
            figs = setdiff( get(0,'Children'), hFig );
            delete(figs);
        end
        set_state(1);
    end

    function callback_radius(hObject, eventdata)
        opt.radius = str2num( get(hObject, 'String') );
        %fprintf('radius = %f\n', opt.radius);
    end

    function callback_visual(hObject, eventdata)
        opt.visual = get(hObject, 'Value') - 1;
        %fprintf('visual = %i\n', opt.visual);
    end

    function callback_center(hObject, eventdata)
        opt.auto_center = get(hObject, 'Value');
        %fprintf('auto_center = %i\n', opt.auto_center);
    end

    function callback_mask(hObject, eventdata)
        opt.mask_dna = get(hObject, 'Value');
        %fprintf('mask_dna = %i\n', opt.mask_dna);
    end

    function callback_flatten(hObject, eventdata)
        opt.flatten = get(hObject, 'Value');
        %fprintf('flatten = %i\n', opt.flatten);
    end

    function callback(hObject, eventdata, action, i, j)
        set_state(0);
        %commandwindow;
        try
            eval(action);
        catch ME
            if opt.catch_exceptions
                fprintf(2, 'xxxxxxxxxxxxxxxxx Error xxxxxxxxxxxxxxxx\n');
                disp(ME.message)
                fprintf(2, '----------------------------------------\n');
            else
                rethrow(ME);
            end
        end
        set_state(1);
    end


    function set_state(state)
        hlist = findobj(get(hFig, 'Children'), 'Type', 'uicontrol');
        
        for ii = 1:size(hlist,1)
            h = hlist(ii);
            if strcmp(get(h, 'Tag'), 'close-button')
                if state
                    set(h, 'String', 'Close All');
                else
                    set(h, 'String', 'Reset');
                end
            else
                if state
                    set(h, 'Enable', 'on');
                else
                    set(h, 'Enable', 'off');
                end
            end
        end
        
        refresh(hFig);
    end


end