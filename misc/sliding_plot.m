function sliding_plot()
% A driver to plot the results of sliding() for different values
% F. Nedelec 20.03.2018

D = 0.001;
figure

for i = 0:1
for j = 0:1
    if i 
        D = 0.001;
    else
        D = 0.0001;
    end
    subplot(2, 2, 1+i*2+j);
    hold on;

    for c = 20:20:80
    for m = 1:100
        v = sliding(c, m, D, j);
        plot(m, v, '.');
    end
    end
    
    ylim([0 0.025]);
    xlabel('Number of motors');
    ylabel('Sliding speed');
    title(['D = ',num2str(D), ' model = ', num2str(j)]);

end
end