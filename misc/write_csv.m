function write_csv(data, filename, info)
%
%
% F. Nedelec, 2014

if nargin < 3
    info = '';
end

if nargin < 2
    error('argument 2 (filename) should be provided');
end

if ~isnumeric(data)
    error('argument 1 (dataX) should be a vector');
end

fid=fopen(filename, 'wt');
fprintf(fid, '%% %s\n', info);

for i=1:size(data, 1)
    fprintf(fid, '\n %10.5f', data(i, 1));
    fprintf(fid, ', %10.5f', data(i, 2:end));
end

fprintf(fid, '\n');
fclose(fid);

end