function [ cnt, Xedges, Yedges, Z ] = hist2d(P, Xedges, Yedges, color_range, levels)

% function [ cnt, Xedges, Yedges, Z ] = hist2d(P, Xedges, Yedges)
% calculate a 2D histogram of data points P along with given bin edges
%
% P should be specified as P(n,:) = { X, Y, Z1, Z2, Z3 ... }
%
% cnt(i,j) returns the number of point P inside bin (i,j),
% which are the points satisfying:
%       Xedges(j) <= P(:,1) < Xedges(j+1)
%       Yedges(i) <= P(:,2) < Yedges(i+1)
% Z(i,j,n) returns the average of P(:,2+n) in bin (i,j)
% 
% Points outside the outer edges are not counted
% if Xedges and Yedges are ommited, they are calculated automatically,
% from the min/max values of the data, to have 16 points/bin on average
%
% F. Nedelec, September 2007

if size(P,2) < 3
    error('data should be provided as an array P(n,:) = { X, Y, Z1, Z2...}');
end

if nargin == 2
    error('none, or both X and Y edges must be provided');
end

if nargin < 2
    Xedges = [];
    Yedges = [];
end

if ( isempty(Xedges) || isempty(Yedges) )
    Xmin = min(P(:,1));
    Xmax = max(P(:,1));
    
    Ymin = min(P(:,2));
    Ymax = max(P(:,2));
    
    %choose the grid to have 16 data-points in each bin, on average
    n = floor( sqrt( size(P,1) / 16 ) );
    
    Xedges = Xmin:(Xmax-Xmin)/n:Xmax;
    Yedges = Ymin:(Ymax-Ymin)/n:Ymax;

    %expand the last bin, to include the max-value
    Xedges(1,size(Xedges,2)) = Xmax + 0.001*(Xmax-Xmin)/n;
    Yedges(1,size(Yedges,2)) = Ymax + 0.001*(Ymax-Ymin)/n;
    
end

[Xn, Xbin] = histc(P(:,1), Xedges);
[Yn, Ybin] = histc(P(:,2), Yedges);

Xmax  = size(Xn,1);
Ymax  = size(Yn,1);

XYmin = Ymax + 1;
XYmax = Ymax * Xmax + Ymax;

XYbin = Ymax * Xbin + Ybin;
%XYbin = Xmax * Ybin + Xbin;

%remove out-of-range data:
out = find( Xbin == 0 | Ybin == 0 );
XYbin(out) = zeros(size(out));

ps  = size(P,2);

%calculate the 2D-histogram:
cnt = zeros(Xmax*Ymax,  1);
Zm  = zeros(Xmax*Ymax,  ps-2);
Z   = zeros(Ymax, Xmax, ps-2);

if ps > 2
    for b = XYmin:XYmax
        
        s  = find(XYbin == b);
        ix = b - XYmin + 1;
        
        le = length(s);
        if ( le > 0 )
            cnt(ix,1) = le;
            Zm(ix,1:ps-2) = mean( P(s,3:ps), 1 );
            %Zm(ix,1) =  min( P(s,3) );
            %Zm(ix,3) =  max( P(s,5) );
        end
    end
    for c = 1:ps-2
        Z(1:Ymax, 1:Xmax, c) = reshape(Zm(:,c), Ymax, Xmax);
    end
else
    cnt = histc(XYbin, XYmin:XYmax);
    size(cnt)
end


cnt = reshape(cnt, Ymax, Xmax);


%display the results, with a 'jet' colormap:
for c = 1:ps-2
    
    bits=65536;
    
    dz = Z(:,:,c);
    figure('Position', [200 400 600 420], 'Name', sprintf('%s(:,%i)', inputname(1), c+2));

    if  nargin > 3
        %reduce rangle as specified
        zMin = color_range(1);
        zMax = color_range(2);
    else
        zMin = min(min(dz));
        zMax = max(max(dz));
        title(sprintf('color range [%.2f %.2f]',zMin, zMax));
    end
    
    if ( bits == 256 )
        im = uint8( (dz-zMin) * ( bits / (zMax-zMin)) );
    else
        im = uint16( (dz-zMin) * ( bits / (zMax-zMin)) );
    end
    image(Xedges, Yedges, im);
    caxis([0 bits]);
    colormap( jet(bits) );

    %add a colorbar:
    cbh=colorbar();
    
    if  nargin > 4
        set(cbh, 'YTickMode', 'manual');
        ticks = sort( bits * ( levels - zMin ) / (zMax-zMin) );
        tvals = ticks/bits * (zMax-zMin) + zMin;
        labels=num2str(tvals(:), ' %.0f');
        set(cbh, 'YTick', ticks);
        set(cbh, 'YTickLabel', labels);
    end
    
end



