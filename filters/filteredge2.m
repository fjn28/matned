function im2 = filteredge2( im )
% compute the norm of the gradient of the image
% the "Sobel edge filter"


if ( isfield(im,'data') ) im = im.data; end

lx = 2;
ux = size(im,1) - 1;
ly = 2;
uy = size(im,2) - 1;

mm1 = [ -1 0 1; -sqrt(2) 0 sqrt(2); -1 0 1];
mm2 = mm1';

gx = convs2( im, mm1, 1 );
gy = convs2( im, mm2, 1 );

im2 = sqrt( gx .^2 + gy .^2 );

return;