function im2 = filteredge1( im )
% compute the norm of the gradient of the image
% so called " " filter,

if ( isfield(im,'data') ) im = im.data; end

lx = 1;
ux = size(im,1) - 1;
ly = 2;
uy = size(im,2);

gx = double( im(lx:ux, ly:uy) ) - double( im(lx+1:ux+1, ly-1:uy-1) );
gy = double( im(lx:ux, ly-1:uy-1) ) - double( im(lx+1:ux+1, ly:uy) );

im2 = zeros( size(im) );
im2(lx:ux, ly:uy) = sqrt( gx .^2 + gy .^2 );

return;