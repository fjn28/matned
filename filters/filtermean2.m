function im2 = filtermean2( im )
% supress extremum values of the image
% very very slow...

if ( isfield(im,'data') ) im = im.data; end

dv  = [ -1 -1; 0 -1; 1 -1; -1 0; 1 0; -1 1; 0 1; 1 1];
ldv = length(dv);
%dv =[ 0 -1; -1 0; 1 0; 0 1 ];

for i = 2:size(im,1)-1
for j = 2:size(im,2)-1
   v = zeros(ldv,1);
   for k = 1:ldv
      v(k) = im( i+dv(k,1), j+dv(k,2) );
   end
   im2(i,j) = meanvalue( v );
end
end

im2 = im;

return;

function m = meanvalue( val );
vals = sort( val );
m = vals( round(length(val)/2) );