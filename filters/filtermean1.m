function im2 = filtermean1( im, n )
% convolution by a circle


if ( isfield(im,'data') ) im = im.data; end
if ( nargin < 2 ) n = 1; end

mask = maskcircle1( 2*n+1 );
   
%im2 = convs2( im, mask, 1 );
im2 = filter2(mask, im);

im2 = im2 ./ sum(sum(mask));

return;
