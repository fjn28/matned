function writeframes(movie, pixelRange)
%write all frames in the movie, as jpg images
%F. nedelec, Jan 2006

off = pixelRange(1);
sca = 255 / (pixelRange(2) - pixelRange(1));

for indx = 1:size(movie,2);
    
    sfrm = (movie(indx).data - off) * sca;
    name = sprintf('frame%04i.jpg', indx);
    imwrite(uint8(sfrm), name);
    fprintf( 'saved %s', name);
    
end
