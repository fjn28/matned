function [ mov, cen1, cen2, founds ] = movietrack1( im )

global debug;
global minDistance searchR minPixels;

%define ROI by mouse clicks:
poly = ROImousepoly( im(1) );
[mask, roi] = maskpolygon( poly );
fig1 = gcf;

rond    = maskcircle1( 2 * searchR + 1 );

%track the movie:
for indx=1:length(im)  
   
   imc  = image_crop( im(indx).data, roi );
   imc  = double( imc ) .* mask;
   
   %finding new points:
   [ bn, rn, found ] = dotfind2( imc );
   samen = dist( bn, rn ) < minDistance;
      
   if ( indx == 1 )
      bt = bn;
      rt = rn;
   end
   
   %following the old ones:
   [ bt, rt ] = dottrack2( imc, bt, rt );   
   
   samet = dist( bt, rt ) < minDistance;
      
   %measure the brightness:
   [ bbt, vbt ] = valuetop( image_crop( imc, [bt - searchR, bt + searchR] ), minPixels);
   [ brt, vrt ] = valuetop( image_crop( imc, [rt - searchR, rt + searchR] ), minPixels);
   [ bbn, vbn ] = valuetop( image_crop( imc, [bn - searchR, bn + searchR] ), minPixels);
   [ brn, vrn ] = valuetop( image_crop( imc, [rn - searchR, rn + searchR] ), minPixels);
   
   fprintf('b tk  %4.1f   %4.1f  : nw  %4.1f  %4.1f', bbt, vbt, bbn, vbn);
   fprintf('r tk  %4.1f   %4.1f  : nw  %4.1f  %4.1f', brt, vrt, brn, vrn);
   
   if ( vbn > vbt )  bt = bn;  end
   if ( vrn > vrt )  rt = rn;  end
   
   if ( samet )
      bt = bn;
      rt = rn;
   end
   if ( samen )
   end
   
   
   %round it and translate:
   c1(indx, 1:2) = round(100*bt(1:2))/100 + roi(1:2) - [ 1 1 ];
   c2(indx, 1:2) = round(100*rt(1:2))/100 + roi(1:2) - [ 1 1 ];
   founds(indx)  = found;
   
   fprintf('%i b:  %5.2f %5.2f ', indx, bt(1), bt(2));
   fprintf('%i r:  %5.2f %5.2f ', indx, rt(1), rt(2));
   
   %make a little movie, for checking afterward:     
   imb = rebin( imc, 'bin2 scale 8bit');
   if ( indx == 1 )
      smallfig = show1( imb, 'raw small', indx);
   else
      figure( smallfig );
      set( smallfig, 'Name', num2str( indx ) );
      cla;
      image( imb );
   end
   plot(bt(2)/2, bt(1)/2, 'b-x','MarkerSize',10);
   plot(bn(2)/2, bn(1)/2, 'b-o','MarkerSize',10);
   plot(rt(2)/2, rt(1)/2, 'r-x','MarkerSize',10);
   plot(rn(2)/2, rn(1)/2, 'r-o','MarkerSize',10);
   pause(0.1);
   mov(indx)=getframe;
   
end

%make a graph of the tracking:
figure( fig1 );
for indx=1:length(im)
   plot(c1(indx,2), c1(indx,1),'bo');
   plot(c2(indx,2), c2(indx,1),'ro');
end

if ( debug == 20 )
   tile(im, cen1);
end




return;


%support functions:

function x = dist(c,d)
x = sqrt( (c(1)-d(1))^2 + (c(2)-d(2))^2 );
return
