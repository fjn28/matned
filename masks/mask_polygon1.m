function [ mask, rect ] = maskpolygon( points, masksize )

% [ mask, rect ] = maskpolygon( points, msize )
%
% make a mask of size <msize> by filling the polygon defined 
% by the given <points> by one-valued pixels
% the points should wrap to close the polygon

nbpoints = size( points, 1 );

%----------------------  degenerate case: only one point in the list

if ( nbpoints == 1 )
   mask = 1;
   rect = [ points points ];
   return;
end

%----------------------  close the polygon if that is not the case:

if any( points(1,:) ~= points( nbpoints, : ) ) 
    points( nbpoints+1, 1:2 ) = points( nbpoints, 1:2 );
    nbpoints = nbpoints + 1;
end

%----------------------  get a bounding rect:

lx = floor( min( points(:, 1) ) );
ux =  ceil( max( points(:, 1) ) );

ly = floor( min( points(:, 2) ) );
uy =  ceil( max( points(:, 2) ) );

if ( nargin > 1 )
   if ( length(msize) == 1 ) masksize(2) = masksize(1); end
   lx = max( lx, 1 );
   ux = min( ux, masksize(1) );
   ly = max( ly, 1 );
   uy = min( uy, masksize(2) );
end

rect = [ lx ly ux uy ];
%rect = [ lx ly size(mask,1)+lx-1 size(mask,2)+ly-1 ];
%rect                         % debug output

%------------------------ calculate the polygon, by scanning in y 
mask = zeros( ux-lx+1, uy-ly+1 );

for y = ly : uy
    
   mask( :, y - ly + 1 ) = section( points, y, lx, ux );
    
end

return;

%------------------------ subfonction 

function line = section( points, y, lx, ux );

nbpoints = size( points, 1 );

%------------------------ find the intersection with the line at y:
inter = [];
b = points( 1, : );

for p = 2 : nbpoints
    
    a = b;
    b = points( p, : );
    
    %---------------------- skip horizontal lines:
    if  a(2) == b(2) 
        continue;
    end
    
    l = ( y - a(2) ) / ( b(2) - a(2) );
    if ( 0.0 <= l ) & ( l < 1 )
        
        x = a(1) + l * ( b(1) - a(1) );
        inter( size(inter, 1 ) + 1 ) = floor( x - lx + 1 );
        
    end
    
end

inter = sort( inter );
if mod( length( inter ), 2 )
    disp('error there');
end

%disp( [ sprintf(' y = %i, [ %i %i ] , ', y, lx, ux) int2str( inter ) ] );


line = zeros( ux - lx + 1 , 1 );

inter

for p = 1:2:size( inter, 1 )

    line( inter(p):inter(p+1) ) = 1;
    
end

return



