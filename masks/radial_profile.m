function [ c, m, v, d ] = radial_profile( im, center, bin, weights )

% function [ c, m, v, d ] = radial_profile( im, center )
% function [ c, m, v, d ] = radial_profile( im, center, bin )
% function [ c, m, v, d ] = radial_profile( im, center, bin, weights )
%
% Computes:
%    c = the count of pixels,
%    m = the mean of pixel values
%    s = the standard deviation of pixel values
%    d = the distance from the center in pixels.
% {c, m, v} are all functions of the distance returned in `d`
%
% If provied, the profiles are calculated with binning 'bin'.
%
% if `weights` is specified ( should have the same size as `im` ),
% then it is used a a weigth matrix for each corresponding pixel
%
% Example:
%   im = tiffread;
%   show(im)
%   cen = mouse_point;
%   [c, m, v, d] = radial_profile(im, cen)
%   figure, plot(d, m);
%
% F. Nedelec, April 2008

if nargin < 2  || isempty(center)
    center = ( [1,1] + size(im) ) / 2;
end

if nargin < 3
    bin = 1;
end

if nargin > 3  &&  any( size(mask) ~= size(im) )
    error('size missmatch between mask and image');
end

if isfield(im, 'data')
    im = double( im.data );
end

%% determine area that can be used
xi  = center(1)  - 1;
xs  = size(im,1) - center(1);
yi  = center(2)  - 1;
ys  = size(im,2) - center(2);

dim = ceil(2*max([xi, xs, yi, ys])+1);
xi = round( (1+dim)/2 - xi );
yi = round( (1+dim)/2 - yi );
xs = xi + size(im,1) - 1;
ys = yi + size(im,2) - 1;

inx = image_crop( mask_radial(dim, bin), [xi, yi, xs, ys] );

%% calculate profiles

mx = max(max( inx ));
c  = zeros( 1, mx );
m  = zeros( 1, mx );
v  = zeros( 1, mx );

for i = 1:mx
    
    ic = logical( inx == i );
    
    if any( size(ic) ~= size(im) )
        error('size missmatch');
    end

    if nargin > 4
        c(i) = sum( sum(weights .* (inx==i) ) );
        m(i) = sum( weights(ic) .*  im(ic)    );
        v(i) = sum( weights(ic) .*  im(ic).^2 );
    else
        c(i) = sum( sum(ic)    );
        m(i) = sum(  im(ic)    );
        v(i) = sum(  im(ic).^2 );
    end
    
    %fprintf('%i:  %8.3f  %8.3f  mean = %8.3f\n', i, c(i), m(i), m(i)/c(i));
        
end

% distance
d = bin * (1:mx);

% divide to get mean, and calculate standard-deviation
m = m ./ c;
v = sqrt( v ./ c - m.^2 );


% debug:
%figure, plot(d, m);


end

