function mask = mask_radial(n, bin)

% mask = mask_radial(n)
% mask = mask_radial(n, bin)
%
% Return a n*n matrix filled with the distance from each
% pixel to the center of the array.
%
% The second form provides numbers > 1, which can be used as indices 
% to build an histogram: value(i,j) = 1 + floor( distance(i,j) / bin );
%
% F. Nedelec, Feb. 2008


if rem(n,1) > 0, 
   n = round(n);
   fprintf('n is not an integer and has been rounded to %i',n)
end

if rem(n,2) == 0
    m = n / 2;
    xx  = ( 0.5 + (0:m-1) ) .^2;
else
    m = (n+1) / 2;
    xx  = (0:m-1) .^2;
end


quart = sqrt(ones(m,1) * xx + xx' * ones(1,m));


if nargin > 1
    %remove the corners of the square, leaving only the central discs
    %maximum where the circles are still complete:
    quart = floor(quart ./ bin) + 1;
    sup = quart( m, 1 ) + 1;
    quart = min( quart, sup * ones(size(quart)) );
end


mask = zeros(n,n);

if rem(n,2) == 0
    % n = 2*m
    mask( m+1:n,  m+1:n ) = quart;
    mask( m:-1:1, m+1:n ) = quart;
    mask( m:-1:1, m:-1:1) = quart;
    mask( m+1:n,  m:-1:1) = quart;
else
    % n = 2*m-1
    mask( m:n,    m:n )   = quart;
    mask( m:-1:1, m:n )   = quart;
    mask( m:-1:1, m:-1:1) = quart;
    mask( m:n,    m:-1:1) = quart;
end

end