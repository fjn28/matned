function mask = mask_angles(n, nb_bins)

% mask = mask_angles(n)
% mask = mask_angles(n, nb_bins)
%
% Return a n*n array filled with the angle in [ 0, 2*pi ), measured
% from the center of the array.
% The second form provides numbers >= 1, which can be used as indices 
% to build a histogram based on the angle.
% The values returned are then: floor( bin * angle / 2*pi ) + 1;
%
% F.Nedelec, Feb. 2008


if rem(n,1) > 0, 
   n = round(n);
   fprintf('n is not an integer and has been rounded to %i',n)
end


if rem(n,2) == 0
    m = n / 2;
    x = ( 0.5 + (0:m-1) )' * ones(1,m);
else
    m = (n+1) / 2;
    x = ( 0:m-1 )' * ones(1,m);
end

quart = atan2(x',x);


mask = zeros(n,n);

if rem(n,2) == 0
    % n = 2*m
    mask( m+1:n,  m+1:n ) = quart;
    mask( m:-1:1, m+1:n ) = quart' + pi*0.5;
    mask( m:-1:1, m:-1:1) = quart  + pi;
    mask( m+1:n,  m:-1:1) = quart' + pi*1.5;
else
    % n = 2*m-1
    mask( m:n,    m:n )   = quart;
    mask( m:-1:1, m:n )   = quart' + pi*0.5;
    mask( m:-1:1, m:-1:1) = quart  + pi;
    mask( m:n,    m:-1:1) = quart' + pi*1.5;
end


if nargin > 1
    mask = ceil( mask * (nb_bins / max(mask(:))) );
    %remove the corners of the square:
    sup  = mask( m, n ) + 1;
    cir  = mask_circle(n);
    mask = mask .* cir + sup * (1-cir);
end

return