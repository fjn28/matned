function [ mask, rect ] = maskpolygon( points, msize )

% [ mask, rect ] = maskpolygon( points, msize )
%
% make a mask of size <msize> by filling the polygon defined 
% by the given <points> by one-valued pixels
% the points should wrap to close the polygon

if ( size( points, 1 ) == 1 )
   mask = 1;
   rect = [ points points ];
   return;
end

if ( size( points, 1 ) > 2 )
   points = setconvclockwise( points );
end

lx = floor( min( points(:, 1) ) );
ux =  ceil( max( points(:, 1) ) );

ly = floor( min( points(:, 2) ) );
uy =  ceil( max( points(:, 2) ) );

if ( nargin > 1 )
   if ( length(msize) == 1 ) msize(2) = msize(1); end
   lx = max( lx, 1 );
   ux = min( ux, msize(1) );
   ly = max( ly, 1 );
   uy = min( uy, msize(2) );
end

% [ lx ly ux uy ]           %for debug purpose

mask = zeros( ux-lx+1, uy-ly+1 );
for y=ly:uy   
   [ lsx, usx ] = polygonsection( points, y );
   lxx = floor( lsx );
   uxx = ceil( usx );
   if ( uxx >= lxx )
      mask(lxx-lx+1:uxx-lx+1, y-ly+1 ) = ones( uxx - lxx + 1, 1); 
   end
end

rect = [ lx ly ux uy ];
%rect = [ lx ly size(mask,1)+lx-1 size(mask,2)+ly-1 ];

return


%support function:


function [ l, u ] = polygonsection( points, y )

u = [];
l = [];

for i=1:size(points,1)-1
   
   a = points(i,:);
   b = points(i+1,:);
   
   if any( a ~= b ) 
      
      %horizontal line:
      if ( floor( a(2) ) == y ) & ( floor( b(2) ) == y )
         l  = max( [ l , min( a(1), b(1) ) ] );
         u  = min( [ u , max( a(1), b(1) ) ] );
      end   
      
      if ( ceil( a(2) ) == y ) & ( ceil( b(2) ) == y )
         l  = max( [ l , min( a(1), b(1) ) ] );
         u  = min( [ u , max( a(1), b(1) ) ] );
      end   
      
      if ( a(2) > y ) & ( b(2) <= y )
         if ( b(2) ~= a(2) )
            p = ( y - a(2) ) / ( b(2) - a(2) );
         else
            p = 0.5;
         end
         nl = a(1) + p * ( b(1) - a(1) );
         l  = max( [ l , nl  ] );
      end
      
      if ( a(2) < y ) & ( b(2) >= y )
         if ( b(2) ~= a(2) )
            p = ( y - a(2) ) / ( b(2) - a(2) );
         else
            p = 0.5;
         end
         nu = a(1) + p * ( b(1) - a(1) );
         u  = min( [ u , nu ] );
      end
      
   end
end

return
