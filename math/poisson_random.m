function res = poisson_random(E, n)

% generate an nx1 vector of poisson-distributed random values
% with parameter E
%
% mean(res) = E
% variance(res) = E
%
% F. Nedelec, 28.09.2017

if ~isscalar(E) 
    error ('the first argument must be a scalar value');
end

%% Check overflow:

if ( E > 256 )
    
    if nargin < 2
        res = randn * sqrt(E) + E;
    else        
        res = randn(n, 1) * sqrt(E) + E;
    end
    return
    
end

%% Generate a single value:

if nargin < 2
    L = exp(-E);
    p = rand;
    k = 0;
    while p > L
        k = k + 1;
        p = p .* rand;
    end
    res = k;
    return
end

%% Generate a vector of values:

L = exp(-E);
p = rand(n,1);
res = zeros(n,1);
k = 0;

sel = logical( p > L );
while any(sel)
    k = k + 1;
    res(sel) = k;
    p(sel) = p(sel) .* rand(sum(sel), 1);
    sel = logical( p > L );
end

end

