function [ pos, dir ] = interpolate_fiber(pts, abs)

% F. Nedelec 01.04.2018

n_pts = size(pts, 1);
q = pts(1, :);
for i = 2 : n_pts
    
    p = q;
    q = pts(i, :);
    n = norm( q - p );
    if abs < n
        dir = ( q - p ) / n;
        pos = p + dir .* abs;
        %fprintf('\n  interpolate %i : %.2f %.2f %.2f', i, pos(1), pos(2), pos(3));
        return
    end
    abs = abs - n;
    
end

pos = [ 0 0 0 ];
dir = [ 1 0 0 ];