function pts = setconvclockwise( points );

% find the convex hull of the points, and
% reorder the points in a clockwise maner

ind = convhull( points(:,1), points(:,2) );
pts = points( ind, : );

return;
