function k = poissrnd(lambda)
    EL = exp(-lambda);
    p = rand();
    k = 0;
    while ( p > EL )
        k = k + 1;
        p = p * rand();
    end
end
