function [ex, ey, ez] = orthonormal_basis(vec)
% Return three vectors forming a orthonormal basis
% where 'ez' is parallel to 'vec'
%
% FJ Nedelec, 26.04.2019
% original code by Marc B. Reynolds

if numel(vec) ~= 3
    error('vector should have 3 components');
end
dir = size(vec);

ez = reshape( vec/norm(vec), dir);

if ez(3) < 0
    s = -1;
else
    s = 1;
end

a = ez(2) / ( ez(3) + s );
b = ez(2) * a;
c = ez(1) * a;
        
ex = reshape([ -ez(3) - b, c, ez(1) ], dir);
ey = reshape([ s * c, s * b - 1.0, s * ez(2) ], dir);

end