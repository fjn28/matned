function [alpha, beta, h] = scale_to_powerlaw(data, exponent, scale, verbose)

% 16.11.2017
% calculate best scaling factors to match powerlaw:
%
% data : (x , y) on each line
% powerlaw: scale * x ^ exponent
%
% output: (x, alpha * y + beta )
%

fts = 18;

xpts = data(:,1);
ypts = data(:,2);

sumY  = sum(ypts);
sumYY = sum(ypts.^2);
sumX  = sum(xpts.^exponent);
sumXY = sum(xpts.^exponent.*ypts);

if 0
    n_points = length(data);

    M = [sumYY, sumY; sumY, n_points];
    V = scale * [ sumXY; sumX ];
    
    sol = linsolve(M, V);
    %M*sol
    
    alpha = sol(1)
    beta = sol(2)
    
else
    
    alpha = scale * sumXY / sumYY;
    beta = 0;
    
end


if verbose

    if verbose > 1
        figure
        hold on;
        xval = min(xpts):0.1:max(xpts);
        curve = scale .* xval .^ exponent;
        plot(xval, curve, '-');
    end
    
    h = scatter(xpts, alpha * ypts + beta, 32);
    h.MarkerFaceColor = [0 0 1];
    h.MarkerFaceAlpha = 0.25;
    h.MarkerEdgeAlpha = 0;
    xlabel('X', 'FontSize', fts);
    ylabel('Y', 'FontSize', fts);

end

end


