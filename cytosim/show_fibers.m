function hfig = show_fibers(data)

% Plot data in structure returned by load_fibers()
%
% F. Nedelec, Oct 2017

hfig = figure;

for i = 1 : length(data)
   
    P = data(i).pts;
    
    plot3(P(:,1),P(:,2),P(:,3));
    hold on;
    plot3(P(:,1),P(:,2),P(:,3), '.', 'MarkerSize', 4);
   
    if isfield(data(i), 'intersect')
        P = data(i).intersect;
        if ~isempty(P)
            plot3(P(1),P(2),P(3), '*');
        end
    end
    
end

axis equal;

end