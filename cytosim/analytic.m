syms t a b c u v w

tpa = t + a^2;
tpb = t + b^2;
tpc = t + c^2;

F = ( a*u / tpa ) ^2 + ( b*v / tpb ) ^ 2 + ( c * w / tpc ) ^2 - 1;
G = -F * ( tpa * tpb * tpc ) ^ 2
GS = simplify(G)
R=collect(GS, t)


a0=1;
a1=2*c^2+2*a^2+2*b^2;
a2=4*b^2*c^2+4*a^2*b^2-a^2*u^2-b^2*v^2+4*a^2*c^2+b^4+c^4-c^2*w^2+a^4;
a3=-2*c^2*w^2*a^2+2*a^4*b^2-2*c^2*w^2*b^2+2*a^2*b^4-2*b^2*v^2*c^2+2*b^2*c^4+2*b^4*c^2+8*a^2*b^2*c^2-2*a^2*u^2*c^2-2*a^2*u^2*b^2-2*b^2*v^2*a^2+2*a^2*c^4+2*a^4*c^2;
a4=-c^2*w^2*b^4-4*a^2*u^2*b^2*c^2-a^2*u^2*c^4+4*a^4*b^2*c^2+a^4*c^4+4*a^2*b^4*c^2+a^4*b^4-4*c^2*w^2*a^2*b^2-c^2*w^2*a^4-b^2*v^2*a^4+b^4*c^4-4*b^2*v^2*a^2*c^2-a^2*u^2*b^4+4*a^2*b^2*c^4-b^2*v^2*c^4;
a5=-2*a^2*u^2*b^2*c^4+2*a^4*b^4*c^2+2*a^2*b^4*c^4-2*b^2*v^2*a^4*c^2+2*a^4*b^2*c^4-2*b^2*v^2*a^2*c^4-2*a^2*u^2*b^4*c^2-2*c^2*w^2*a^4*b^2-2*c^2*w^2*a^2*b^4;
a6=-b^2*v^2*a^4*c^4-c^2*w^2*a^4*b^4+a^4*b^4*c^4-a^2*u^2*b^4*c^4;

Rc = a0*t^6 + a1*t^5 + a2*t^4 + a3*t^3 + a4*t^2 + a5*t^1 + a6;

%simplify(Rc-R)

simplify(a2)
factor(a2)
collect(a2)