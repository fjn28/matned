function P = getParameters( file_or_dir_name )
% P = getParameters;
% P = getParameters( file_name );
% P = getParameters( directory_name );
%
% returns an object containing the parameter values read from a specified
% cytosim parameter file, or from the files 'data.in' or 'data.out' found
% in the current or specified directory. The actual file read is returned
% in P.file and P.path
%
% F. Nedelec, Nov. 2 2005
% CVS: $Id:$

P = [];

if nargin < 1
    file_list = { 'config.cym' 'data.out' 'data.in' };
else
    if isdir( file_or_dir_name )
        file_list = { [file_or_dir_name, '/config.cym']  [file_or_dir_name, '/data.in'] };
    else
        file_list = { file_or_dir_name };
    end
end

for ii=1:size(file_list, 2)
    P.file = char(file_list(ii));
    fid = fopen( P.file  );
    if ( fid ~= -1 )
        P.path = fullfile( pwd, P.file );
        break;
    end
end

if ( fid == -1 )
    error('could not open input file %s', char(file_list(1)) ); 
end

%------------------------------------------------------------------

while 1
    
    line = fgets(fid);
    if ( line == -1 )
        break;
    end
    
    [name, count, errmsg, nextIndex ] = sscanf(line, '%s', 1);
    
    if ( count > 0 ) & isName(name)
      
      lline = length( line );
      while  (nextIndex < lline) & (isValue(line(nextIndex)) == 1)
        nextIndex = nextIndex + 1;
      end
      
      last = nextIndex;
      while  (last < lline) & isValue(line(last))
        last = last + 1;
      end
      
      values = line( nextIndex:last-1 );
      %fprintf('%13s [%s]\n',name, values);
      
      % split the 'values' string in pieces
      [start,final]=regexp(values, '[-\w\.]*');
      for n=1:length(start)
        
        singleVal = values([start(n):final(n)]);
        
        % check for hex strings first and convert them to decimals
        if (length( singleVal ) >= 3) && (isequal(values([1:2]), '0x'))
        
          % if it is a hex string, convert it
          hexstr = singleVal([3:end]);
          hexval = hex2dec(hexstr);
          
          if exist('P') && isfield(P, name)
            eval(['P.' name '= [P.' name '; hexval];']);
          else
            eval(['P.' name '= hexval;']);
          end
          
        else
          % if singleVal is a normal number use str2num
          V  = str2num( singleVal );
          
          if isempty( V )
              V = [ char(singleVal), ' ' ];
          end
          
          if exist('P') && isfield(P, name)
              %concatenate horizontally:
              eval(['P.' name '= [P.' name ', V];']);
          else
              %set a field:
              eval(['P.' name '= V;']);
          end
        end      
      end
    else
        %fprintf('ignored: %s', line);
    end
  end
    
  fclose( fid );
  return;

%-------------------------------------------------------------------

function r = isName(name)
   r = true;
   for ii = 1:length(name)
       c = name(ii);
       if ~ ( isstrprop(c, 'alphanum') | ( c == '_' ))
           r = false;
           return;
       end
       if ( ii == 1 ) & isstrprop(c, 'digit')
           r = false;
           return;
       end
   end
return


function r = isValue(c)
  if isspace(c)
    r = 1;
    return;
  end
  if isstrprop(c, 'digit') | ( c == '.' ) | ( c == '-' ) | ( c == '+' ) | ( c == 'e' )
    r = 2;
    return;
  end
  if isstrprop(c, 'alpha') | ( c == '_' )
    r = 3;
    return;
  end
  if ( c == '\0' )
    r = 4;
    return;
  end
  r = 0;
  return
