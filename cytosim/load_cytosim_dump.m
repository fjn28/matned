function check_dump()

% This is used to explore Cytosim's linear system in matlab
% - load the matrices and vector from Cytosim's dump
% - plot convergence pattern of BICGstab, with and without preconditionning
%
% F. Nedelec, 16 Oct. 2014, March 2018, June 2018

%% Loading

ord = load('dump/ord.txt');
time_step = load('dump/stp.txt');
fprintf(1, 'system of size %i with time_step %f\n', ord, time_step);

obj = fread(fopen('dump/obj.bin'), ord, 'double');
drg = fread(fopen('dump/drg.bin'), ord, 'double');
sys = fread(fopen('dump/sys.bin'), [ord, ord], 'double');
ela = fread(fopen('dump/ela.bin'), [ord, ord], 'double');  %elasticity matrix
mob = fread(fopen('dump/mob.bin'), [ord, ord], 'double');  %projection matrix
con = fread(fopen('dump/con.bin'), [ord, ord], 'double');  %preconditionner
pts = fread(fopen('dump/pts.bin'), ord, 'double');
rhs = fread(fopen('dump/rhs.bin'), ord, 'double');
sol = fread(fopen('dump/sol.bin'), ord, 'double');

%% Check matrix

show(abs(sys)); set(gcf, 'name', 'System matrix');
%show(abs(mob)); set(gcf, 'name','Projection matrix');
%show(abs(ela)); set(gcf, 'name','Elasticity matrix');

if ( 1 )
    mat = eye(ord) - time_step * mob * ela;
    show(abs(mat)); set(gcf, 'name','Reconstituted matrix');

    figure('Position', [200 200 600 600]);
    plot(reshape(mat,1,ord*ord), reshape(sys,1,ord*ord), '.')
    xl = xlim;
    ylim(xl);
    xlabel('Reconstituted matrix');
    ylabel('cytosim matrix');
end
if ( 0 )
    figure; hold on;
    plot(abs(sys), '^b');
    plot(abs(mat), 'vr');
end


%% check iterative solver
tol = 0.0001;
maxit = ord;

sss = sparse(sys);

% without preconditionning:
[x0,fl0,rr0,itr,rv0] = bicgstab(sss, rhs, tol, maxit);
fprintf(1, 'BCGS        converved after %6.1f operations %f\n', 2*itr, rr0);

figure('Position', [100 300 1400 600]);
subplot(1,2,1);
plot(x0, sol, 'k.');
xlabel('matlab solution');
ylabel('cytosim solution');
xl = xlim;
ylim(xl);

subplot(1,2,2);
semilogy(rv0/rv0(1),'b:', 'Linewidth', 2);

xlabel('Number of M*V operations');
ylabel('Relative residual');
title('Solver convergence');
hold on;

%% Try different preconditionners

iCON = inv(con);

function y = mfun1(x)
   y = con * x;
end

function y = mfun2(x, mode)
    if ( strcmp(mode, 'notransp') )
        y = con * x;
    else
        y = con' * x;
    end
end

% with cytosim's preconditionner:
[x0,fl0,rr0,itr,rv0] = bicgstab(sss, rhs, tol, maxit, @mfun1);
semilogy(rv0/rv0(1),'b-', 'Linewidth', 2);
fprintf(1, 'BCGS-P      converved after %6.1f operations %f\n', 2*itr, rr0);

[L, U] = ilu(sss);
[x0,fl0,rr0,itr,rv0] = bicgstab(sss, rhs, tol, maxit, L, U);
semilogy(rv0/rv0(1),'b--', 'Linewidth', 2);
fprintf(1, 'BCGS-LU     converved after %6.1f operations %f\n', 2*itr, rr0);

for i = 3:8
    RS = 2^i;
    [x0,fl0,rr0,itr,rv0] = gmres(sss, rhs, RS, tol, maxit);
    semilogy(rv0/rv0(1),'k:', 'Linewidth', 2);
    fprintf(1, 'GMRES   %03i converved after %6.1f operations %f\n', RS, (itr(1)-1)*RS+itr(2), rr0);
end
for i = 2:7
    RS = 2^i;
    [x0,fl0,rr0,itr,rv0] = gmres(sss, rhs, RS, tol, maxit, @mfun1);
    semilogy(rv0/rv0(1),'k-', 'Linewidth', 2);
    fprintf(1, 'GMRES-P %03i converved after %6.1f operations %f\n', RS, (itr(1)-1)*RS+itr(2), rr0);
end
for i = 2:7
    RS = 2^i;
    [x0,fl0,rr0,itr,rv0] = gmres(sss, rhs, RS, tol, maxit, L, U);
    semilogy(rv0/rv0(1),'k--', 'Linewidth', 2);
    fprintf(1, 'GMRES-LU %03i converved after %6.1f operations %f\n', RS, (itr(1)-1)*RS+itr(2), rr0);
end

[x0,fl0,rr0,itr,rv0] = qmr(sss, rhs, tol, maxit);
semilogy(rv0/rv0(1),'m:', 'Linewidth', 2);
fprintf(1, 'QMR         converved after %6.1f operations %f\n', 2*itr, rr0);

[x0,fl0,rr0,itr,rv0] = qmr(sss, rhs, tol, maxit, @mfun2);
semilogy(rv0/rv0(1),'m-', 'Linewidth', 2);
fprintf(1, 'QMR-P       converved after %6.1f operations %f\n', 2*itr, rr0);


OPT.smoothing = 1;
for i = 3:8
    RS = 2^i;
    [x0,fl0,rr0,itr,rv0] = idrs(sss, rhs, RS, tol, maxit, [], [], [], OPT);
    semilogy(rv0/rv0(1),'r:', 'Linewidth', 2);
    fprintf(1, 'IDRS   %03i  converved after %6.1f operations %f\n', RS, itr, rr0);
end
for i = 2:7
    RS = 2^i;
    [x0,fl0,rr0,itr,rv0] = idrs(sss, rhs, RS, tol, maxit, iCON, [], [], OPT);
    semilogy(rv0/rv0(1),'r--', 'Linewidth', 2);
    fprintf(1, 'IDRS-P %03i  converved after %6.1f operations %f\n', RS, itr, rr0);
end

%%

X = eye(ord) - diag(time_step./drg) * ela;
P = X + con .* ( X == 0 );
[L, U] = ilu(sparse(P));

[x0,fl0,rr0,itr,rv0] = bicgstab(sss, rhs, tol, maxit, L, U);
semilogy(rv0/norm(rhs),'k--');

figure;
spy(P)
set(gcf, 'name', 'System matrix');
 
end