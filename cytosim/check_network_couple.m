function check_network_couple(arg)

% F. Nedelec, 16.03.2016
%
% Plot number of bridge couples, predicted and simulated

if nargin < 1
    arg = '.';
end

list = {};

if isdir(arg)
    list{1} = fullfile(pwd, arg);
else
    [s, out] = system(['echo ', arg]);
    if s == 0
        list = strsplit(out);
    end
end


hdir = pwd;

for i = 1:length(list)
    
    pth = list{i};
    
    if isdir(pth)
        cd(pth)
        process(pth);
        cd(hdir);
    end    
end

end

%%

function process(pth)
    figure('MenuBar', 'none', 'Position', [256, 512+256, 512+256, 256+64+32]);
    c = load('couple.txt');

    subplot(1,2,1);
    plot_one(c(:, 1:6));
    title(['motor - ', pth]);

    subplot(1,2,2);
    plot_one(c(:, 7:12));
    title(['crosslink - ', pth]);
    savepng('couples.png')
end

function plot_one(c)
    scale = max(c(:,1)+c(:,2)+c(:,3));
    plot([0, scale], [0, scale], 'y-');
    hold on;
    plot(c(:,1), c(:,4), 'g.');
    plot(c(:,2), c(:,5), 'b.');
    plot(c(:,3), c(:,6), 'r.');
    xlabel('predicted', 'FontSize', 18);
    ylabel('simulated', 'FontSize', 18);
    xlim([0, scale]);
    ylim([0, scale]);
end


%%

function process_old(pth)
    figure('MenuBar', 'none', 'Position', [256, 512+256, 512+256, 256+64+32]);

    subplot(1,2,1);
    m = load('motor.txt');
    plot_two(m);
    title(['motor - ', pth]);

    subplot(1,2,2);
    x = load('crosslinker.txt');
    plot_two(x);
    title(['crosslink - ', pth]);
end


function plot_two(c)
    scale = max(c(:,1));
    plot([0, scale], [0, scale], 'y-');
    hold on;
    plot(c(:,2), c(:,3), 'b.');
    plot(c(:,4), c(:,5), 'r.');
    xlabel('predicted', 'FontSize', 18);
    ylabel('simulated', 'FontSize', 18);
    xlim([0, scale]);
    ylim([0, scale]);
end
