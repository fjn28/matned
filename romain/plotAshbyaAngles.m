function plotAshbyaAngles

% plot histograms for angles

a = load('angle_measured.txt');
as = load('angle_analyze.txt');

bins = 0:30:180;

hs = histcounts(as*180/pi, bins);
h = histcounts(a, bins);

mid = (bins(2:end) + bins(1:end-1))/2;
bar(mid',[hs'/sum(hs), h'/sum(h)]);

xlabel('Angle (degree)');
ylabel('Distribution (a.u.)');
legend('simulated', 'measured');

end