function analyzeNucPositions(varargin)
%
% Analyze motion of nuclei in Ashbya gossypii
% F. Nedelec 1/10/2016
% Last modified 30/12/2016
%
       
color = [ 0 0 0; 0.9 0 0; 0 0.9 0; 0.7 0.7 0; 0 0 1;...
    1 0 1; 0.5 0.5 0.5; 1 0 0.5; 0.1 0.7 0.5; 0.5 0 0.7;...
    0.5 0.7 0; 0 0.5 0.7; 0.7 0.5 0.7; 0.9 0.5 0 ];

%% get a list of files to process:

files = {};
paths = dir('Movie*');
for ip = 1:length(paths)
    list = dir([paths(ip).name, '/hypha*.txt']);
    for ii = 1:length(list);
        files{end+1} = [paths(ip).name, '/', list(ii).name];
    end
end

%% collect data for all files:

time_interval = 30;

% max interval to calculate delta-position:
max_interval = 20;
% pixel size
pixel_size = 0.2;

%files={files{12}};

nucpos = cell(max_interval, 1);
speeds = cell(length(files), 1);
distances = cell(length(files), 1);
separation = cell(length(files), 1);

for f = 1:length(files);
    file = files{f};
    path = file(1:strfind(file, '/'));
    cd(path);
    get_parameters;
    cd ..
    fprintf(1, 'processing file %i: %s\n', f, file);
    fprintf(1, '    pixel_size = %f\n', pixel_size);
    [ data, speeds{f}, distances{f}, separation{f} ] = get_data(file, 0);
    for id = 1:length(data)
        nucpos{id} = vertcat(nucpos{id}, data{id});
    end
end

nHyphae = length(speeds);

%% plot growth speed

figure('Name', 'Speed');
hold on;
for i = 1:nHyphae;
    plot(i, speeds{i}(2:end), 'o', 'color', color(i,:));
    plot([i-0.4, i+0.4], [speeds{i}(1), speeds{i}(1)], 'k-');
end
xlim([0.4, nHyphae+0.6]);
xlabel('Hyphae');
ylabel('Mean Speed (\mum/s)')
title('Nuclei Speed & Growth speed');
savepng('speed');

%% hyphae tip and nuclei positions

figure('Name', 'Position');
hold on;
for i = 1:nHyphae;
    plot(i, distances{i}, 'o', 'color', color(i,:));
end
xlim([0.4, nHyphae+0.6]);
xlabel('Hyphae');
ylabel('Distance from hyphae tip (\mum)')
title('Distance from hyphae tip');
savepng('position');

%% Separation between nuclei

fid=fopen('separation.txt', 'wt');
fprintf(fid, '%% Nuclei separation data %s\n', date);

dnn = [];
for i = 1:nHyphae;
    dnn = vertcat(dnn, separation{i});
    fprintf(fid, '%% hypha %i\n', i);
    fprintf(fid, '%10.5f\n', separation{i});
end

fclose(fid);

figure('Name', 'Separation');
hist(dnn, 0.5:17);
xlim([0, 15]);
xlabel('Separation between nuclei / micrometer');
ylabel('Histogram counts');
title('Nuclei Separation in Live Ashbya');

savepng('separation.png');
savepdf('separation.pdf');

fprintf(1, 'separation number of records: %i\n', length(dnn));
fprintf(1, 'separation between nuclei: %f +/- %f um\n', mean(dnn), std(dnn));

%% correlation between speed and position

% we take away the correlation with growth speed by dividing:
figure('Name', 'Speed vs. Position');
hold on;
x = [];
y = [];
for i = 1:nHyphae;
    plot(distances{i}, speeds{i}/speeds{i}(1), 'o', 'color', color(i,:));
    x = horzcat(x, distances{i});
    y = horzcat(y, speeds{i}/speeds{i}(1));
end
m = sum(x.*(y-1))/sum(x.^2);
plot(x, m*x+1, '-');
xlabel('Distance from hyphae tip (\mum)')
ylabel('Nucleus mean speed / hyphae growth speed')
title('Speed vs. Position');
savepng('position_speed');

%% Calculate basic characteristics of nuclei movements

x = zeros(length(nucpos), 1);
ym = zeros(length(nucpos), 1);
yv = zeros(length(nucpos), 1);

for i = 1:length(nucpos);
    x(i) = i * time_interval;
    ym(i) = mean(nucpos{i});
    yv(i) = var(nucpos{i});
end

%% Convection

m = sum(x.*ym)/sum(x.^2);

figure('Name', 'convection');
plot(x, ym, 'o');
hold on;
plot(x, m*x, '-');
xlabel('Time (sec)');
ylabel('Distance (\mum)')
title(sprintf('mean convection speed %.5f um/s\n', m));
savepng('convection');


%% Diffusion

m = sum(x.*yv)/sum(x.^2)/2;

figure('Name', 'diffusion');
plot(x, yv, 'o');
hold on;
plot(x, 2*m*x, '-');
xlabel('Time (sec)');
ylabel('Distance Squared (\mum^2)')
title(sprintf('Diffusion coefficient %.5f um^2/s\n', m));
savepng('diffusion');

%% sub functions

function [res, speed, distances, separation] = get_data(filename, make_plots)
    
    data = load(filename);
    
    % Check first line:
    if data(1,1) ~= 0
        fprintf(2, '    unexpected format\n');
    end
    
    % Number of nuclei:
    nNuclei = max(data(:,1));
    
    % number of time frame
    nIntervals = max(data(:,2)) - min(data(:,2));
    
    % repackage data
    nucleus = cell(nNuclei, 1);
    for n = 1:nNuclei
        % extract data for this nuclei:
        tmp = data(logical(data(:,1)==n), 2:4);
        % convert positions to micro-meters
        tmp(:,2) = tmp(:,2) * pixel_size;
        tmp(:,3) = tmp(:,3) * pixel_size;
        nucleus{n} = tmp;
        
        if ( size(tmp, 1) ~= nIntervals+1 )
            fprintf(2, '    found %i records for nucleus %i\n', size(tmp,1), n);
        end
    end
 
    % calculate direction of hyphae based on tip growth:
    
    dpos = nucleus{1}(end,2:3) - nucleus{1}(1,2:3);
    dist = sqrt( dpos(1).^2 + dpos(2).^2 );
    direc_growth = dpos / dist;
    delta_growth = dist / ( nucleus{1}(end,1) - nucleus{1}(1,1) );

    % better calculation a direction of hyphae based on PCA:
    pos = [];
    for n = 1:nNuclei
        pos = vertcat(pos, nucleus{n}([1,end],2:3));
    end
    cen = mean(pos, 1);
    pos = pos - ones(size(pos,1),1) * cen;
    [~,~,vec] = svd(pos, 0);
    direc = vec(:,1)';
    % reorient the vector to align with growth:
    if ( direc_growth * direc' < 0 )
        direc = -direc;
    end
    
    %fprintf(1, 'growth_speed = %f\n', speed(1));

    % plot nuclei XY-positions and axis of hyphae
    if ( make_plots )
        figure('Name', filename);
        hold on;
        pos = [];
        for n = 1:nNuclei
            plot(nucleus{n}(:,2), nucleus{n}(:,3), 'o', 'color', color(n,:));
            pos = vertcat(pos, nucleus{n}(:,2:3));
        end
        cen = mean(pos, 1);
        rad = sqrt( std(pos(:,1))^2 + std(pos(:,2))^2 );
        plot(cen(1), cen(2), 'x');
        dif = rad * direc_growth;
        plot([cen(1)-dif(1), cen(1)+dif(1)], [cen(2)-dif(2), cen(2)+dif(2)], 'k--');
        dif = rad * direc;
        plot([cen(1)-dif(1), cen(1)+dif(1)], [cen(2)-dif(2), cen(2)+dif(2)], 'k-');
        title(filename);
        xlabel('X-coordinate (\mum)');
        ylabel('Y-coordinate (\mum)');
        axis equal;
    end
    
    % calculate separation between nuclei:
    separation = [];
    for n = 1:nIntervals
        nuc = data(logical((data(:,2)==n) .* (data(:,1)>1)), [1,3,4]);
        pos = pixel_size * sort( direc(1)*nuc(:,2) + direc(2)*nuc(:,3) );
        separation = vertcat(separation, diff(pos));
    end
    
    % calculate horizontal total motion and speed:
    speed = zeros(1, nNuclei);
    distances = zeros(1, nNuclei);
    for n = 1:nNuclei
        nuc = nucleus{n};
        pos = direc(1)*nuc(:,2) + direc(2)*nuc(:,3);
        tim = time_interval * nuc(:,1);
        % estimate the mean speed from the linear fit:
        p = polyfit(tim, pos, 1);
        speed(n) = p(1);
        % estimate the net speed over the extreme positions: 
        %speed(n) = ( pos(end) - pos(1) ) / ( tim(end) - tim(1) );
        % calculate mean distance to tip:
        indices = nucleus{n}(:,1);
        dpos = mean( nucleus{n}(:,2:3) - nucleus{1}(indices,2:3) );
        distances(n) = direc(1)*dpos(1) + direc(2)*dpos(2);
    end

    % plot projected nuclei positions
    if ( make_plots )
        figure('Name', filename);
        hold on;
        for n = 1:nNuclei
            nuc = nucleus{n};
            pos = direc(1)*nuc(:,2) + direc(2)*nuc(:,3);
            tim = time_interval * nuc(:,1);
            plot(tim, pos, 'o', 'color', color(n,:), 'LineWidth', 2);
            p = polyfit(tim, pos, 1);
            plot(tim, polyval(p, tim), '-', 'color', color(n,:));
        end
        title(filename);
        xlabel('Time (sec)');
        ylabel('Y-coordinate (\mum)');
     end
    
    % calculate offset, but exclude index==1, which is the hyphae tip
    res = cell(max_interval, 1);
    for d = 1:max_interval
        dis = [];
        for n = 2:nNuclei
            nuc = nucleus{n};
            
            dx = nuc(1+d:end,2) - nuc(1:end-d,2);
            dy = nuc(1+d:end,3) - nuc(1:end-d,3);
            
            dxy = direc(1) * dx + direc(2) * dy;
            %dxy = direction(1) * dx + direction(2) * dy - d * delta;
            
            %msd = dx.^2 + dy.^2;
            %msd = dx.^2 + dy.^2 - ( d * delta_growth ).^2;
            
            dis = vertcat(dis, dxy);
        end
        res{d} = dis;
    end
end


end
