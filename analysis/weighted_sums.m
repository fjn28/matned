function [cen, mom, ell, sv] = weighted_sums( im, cen, debug )

% [cen, mom, ell, sv] = weighted_sums( im, cen, debug )
%
% If not provided, compute the barycenter 'cen' of the pixels,
% and the second moments:
%     mom = [ mxx, myy, mxy ]
% Also provide the ellipse that fit the data:
%     ell = [ a, l, w ], with:
%        a  angle in radian of maximum moment
%        l  major dimension = sqrt( mom(ang) / sv ) 
%        w  minor dimension = sqrt( mom(ang+pi/2) / sv ) 
%     sv  = sum of all pixel values 
%
% Note: for an ellipse filled with 1s, we recover exactly the axes
% and the sized, but this does not hold for an outlined (unfilled) ellipse.
%
% F. Nedelec, revised April 2008

if nargin < 1
    error('weighted_sums() requires one argument: the image');
end

im = double(im);
sz = size(im);
sv = sum(sum( im));

if sv <= 0
   warning('weighted_sums() called with negative image');
   if nargin < 2 || isempty(cen)
       cen = ( sz+[1,1] ) / 2;  %the center of the image
   end
   mom = [ 0 0 0 ];
   ell = [ 0 0 0 ];
   return;
end

ssy = sum(im, 2);
xx  = 1:sz(1);
sx  = xx * ssy;

ssx = sum(im, 1);
yy  = (1:sz(2))';
sy  = ssx * yy;

if nargin < 2 || isempty(cen)
    %set the center if not specified as argument
    cen = [ sx/sv, sy/sv ];
end

if nargout > 1

    sxx = (xx.^2) * ssy;
    syy = ssx * (yy.^2);
    sxy = xx * im * yy;

    %check that formula when the center is not the barycenter:
    mxx = sxx - sv * cen(1)^2;
    myy = syy - sv * cen(2)^2;
    mxy = sxy - sv * cen(1)*cen(2);
    mom = [ mxx, myy, mxy ];
    
end

if nargout > 2
    
    %calculate the ellipse angle and axes:
    mmm = ( mxx - myy )^2 + 4 * mxy^2;

    if ( mxy ~= 0 )
        an = atan( ( myy - mxx + sqrt(mmm) ) / ( 2 * mxy ) );
    else
        if ( mxx > myy )
            an = 0;
        else
            an = pi/2;
        end
    end


    mmax = cos(an)^2 * mxx + sin(an)^2 * myy + 2*cos(an)*sin(an) * mxy;
    mmin = sin(an)^2 * mxx + cos(an)^2 * myy - 2*cos(an)*sin(an) * mxy;

    ell = [ an, 2*sqrt(mmax/sv), 2*sqrt(mmin/sv) ];

end


if nargin > 2
    
   MS = 14;
   show_image( im );
   %plot center
   plot(cen(2), cen(1), 'bo', 'MarkerSize', MS, 'Linewidth', 2);
   
   if nargout > 2
       %plot pairs for major axis
       dx = ell(2) * cos(an);
       dy = ell(2) * sin(an);
       plot([cen(2)+dy, cen(2)-dy], [cen(1)+dx, cen(1)-dx],'bo',...
           'MarkerSize', MS, 'MarkerFaceColor', 'y', 'Linewidth', 2);

       %plot pairs for minor axis
       dx = -ell(3) * sin(an);
       dy =  ell(3) * cos(an);
       plot([cen(2)+dy, cen(2)-dy], [cen(1)+dx, cen(1)-dx],'bo',...
           'MarkerSize', MS, 'MarkerFaceColor', 'y', 'Linewidth', 2);

       %draws an ellipse surrounding the features on top of the picture:
       %draw_ellipse( cen, ell );
   end
   
end



end