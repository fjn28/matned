function kymo = kymograph( movie )

nbframes = length( movie );

show( movie(1).data );
[X,Y,C,xi,yi] = improfile;

kymolength = size(X,1);
kymo = zeros(  nbframes, kymolength );

for frame=1:nbframes
   [X,Y,C,xi,yi] = improfile( movie(frame).data, xi, yi );
   kymo(frame, :) = C';
end

close;
show(kymo);