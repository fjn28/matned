function [ ang, fit, dif, len ] = fourier_curve( cff, n )

% function [ ang, fit, dif, len ] = fourier_curve( cff, n )
%
% Sum the fourier coefficients to obtain the fit and derivative of the fit,
% using '2*n' sectors in [-pi, pi]
%
% 'fit' are the values of the function at angles 'ang'
% 'dif' are the values of the derivative at angles 'ang'
%
% To plot the result:
%    plot(fit.*cos(ang), fit.*sin(ang));
%    axis equal
%
% F. Nedelec, 17.02.2016

if nargin < 2
    n = 100;
end

% delta-angle:

dan = pi/n;
ang = (1-n:n) * dan;

% calculate fit:

fit = cff(1,1) / 2;
dif = 0;

for n = 1:size(cff,1)-1
    fit = fit +       cff(n+1,1) * cos(n*ang) + cff(n+1,2) * sin(n*ang);
    dif = dif + n * (-cff(n+1,1) * sin(n*ang) + cff(n+1,2) * cos(n*ang));
end

% calculate length of the curve:

len = dan * sum( sqrt( fit.^2 + dif.^2 ) );

end