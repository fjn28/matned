function cff = fourier_coefficients( data, nFou, make_plot )

% function cff = fourier_coefficients( angle, weight, nFou )
%
% Return the fourier coefficients corresponding to the data point
% described in the two arrays [ angle, weight ]
% cff(n+1, 1)  corresponds to cos(n*theta)
% cff(n+1, 2)  corresponds to sin(n*theta)
%
% F. Nedelec, Feb. 2008, updated 2015

if ( nargin < 2 )
    error('fourier_coefficients takes at least 2 arguments');
end

if ( nargin < 3 )
    make_plot = 0;
end

if size(data, 2) < 2
    error('the first argument data should be a Nx2 or Nx3 matrix');
end

%% calculate and sort data

angle  = atan2(data(:,2), data(:,1));
radius = sqrt( data(:,1).^2 + data(:,2).^2 );

[ angle, permutation ] = sort(angle);
radius = radius(permutation);

d_angle = diff(angle);
d_angle(end+1) = angle(1) - angle(end) + 2*pi;

% calculate coefficients:

cff = zeros(nFou, 2);
cff(1,1) = sum( radius .* d_angle ) / pi;
cff(1,2) = 0;

for n = 1:size(cff,1)-1
    cff(n+1,1) = sum( radius .* cos( n * angle ) .* d_angle ) / pi;
    cff(n+1,2) = sum( radius .* sin( n * angle ) .* d_angle ) / pi;
end


if make_plot
    
    figure;
    plot(radius.*cos(angle), radius.*sin(angle), 'yo');
    axis equal;
    hold on;
    
    % calculate and plot fit:
    ang = 0:pi/100:2*pi;
    fit = cff(1,1) / 2;
    for n = 1:size(cff,1)-1
        fit = fit + cff(n+1,1) * cos(n*ang) + cff(n+1,2) * sin(n*ang);
        plot(fit.*cos(ang), fit.*sin(ang), '-');
    end

end

end