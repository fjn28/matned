function [ left, right ] = imsplit( im, a, b )

% [ left, right ] = imsplit( im, a, b, side )
%
% split <im> along a line defined by points <a> and <b>


left  = zeros( size(im) );
right = zeros( size(im) );

if ( round( b(2) ) == round( a(2) ) )   %horizontal line:
   
   y = round( a(2) );
   if ( y > 0 )
      yy = min( y, size(im,2) );
      left( 1:size(im,1), 1:yy ) = im( 1:size(im,1), 1:yy );
   end
   if ( y < size(im,2) )
      yy = max( y, 0 );
      right( 1:size(im,1), yy+1:size(im,2) ) = im( 1:size(im,1), yy+1:size(im,2) );
   end
else                                    %general case:
   
   slope = ( b(1) - a(1) ) / ( b(2) - a(2) );
   for y = 1:size(im,2)    
      x = a(1) + ( y - a(2) ) * slope;
      x = round(x);
      if ( x > 0 )
         xx = min( x, size(im,1) );
         left( 1:xx, y ) = im( 1:xx, y );
      end
      if ( x < size(im,1) )
         xx = max( x, 0 );
         right( xx+1:size(im,1), y ) = im( xx+1:size(im,1), y );
      end
   end
   
end
