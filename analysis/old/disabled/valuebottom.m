function [ base, value ] = valuebottom ( im, nbpixel )

h  = imhist( im );
ch = cumsum( h );

base = max( find( ch <= nbpixel ) ) - 1;

if ( isempty( base ) ) base = min( find( h > 0 )); end;

%the mean value of the pixels below base:

value = [0:base] * h(1:base+1);
value = value ./ sum( h(1:base+1) );

return
