function base = imbase( im, debug )
% base = imbase( im, debug )
%
% find the pixel-value of the background,
% defined by the first minimum from the left in the pixel value histogram.
% plot histogram is debug is given
% F. Nedelec

if ( isfield( im, 'data') ) im = im.data; end

h = imhist( im );


%================= calculate a gaussian filter:

%filt = [ 1 1 1 2 3 4 3 2 1 1 1 ];
%filt = [ 1 1 1 1 1 2 2 3 4 5 6 5 4 3 2 2 1 1 1 1 1 ];

width = 40;
filt = exp( -( [ -2*width:2*width ] / width ) .^ 2 );
filt = filt ./ sum( filt );

%================= convolve by the filter to get a smooth profile:

hs = conv( h, filt );

%----- hs( x + shift ) corresponds to h( x ):
shift = 2 * width + 1;

%======================= define bounds for the maximum point
min_nb_pixels = size(im,1) * size(im,2) / 20;

ch = cumsum( hs );
lower = min( find( ch >= min_nb_pixels ) );
lower = max( shift, lower );

[ mv, upper ] = max( hs );

%======================= find the first maximum

base = lower;
while ( hs( base + 1 ) >= hs( base ) ) & ( base < upper )
    base = base + 1 ; 
end

if ( isempty( base ) ) 
    base = min(min( im )); 
else
    base = base - shift; 
end

if ( nargin > 1 )
    %graphical output for debugging purpose:

    figure('Name','imbase debug : histogram');
    clf;
    bar( [1:length(h)]-1, h, 'g' );
    hold on;
    plot( [1:length(hs)]-shift, hs, 'k-', 'linewidth', 1);
    
    plot( [base, base], [0, mv], 'k-', 'linewidth', 2);
    plot( [lower, lower]-shift, [0, mv/2], 'b-');
    plot( [upper, upper]-shift, [0, mv/2], 'r-');
    axis([ 0, size(h,1), 0, max(h)]);
    
    %TODO: include picture thumbnail. axes([ 0.5, );
end

