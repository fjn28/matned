function im = immax( im1, im2, d, mode )

% im = imadd( im1, im2, d, mode )
%
% compute the maximum of <im1> and  <im2> translated by <d>
% <mode> defines the clipping
%     2  : (default) keep the size of <im1>
%     1  : keep all pixels
%     0  : only keep intersection

if ( nargin < 4 ) mode = 2; end
if ( nargin < 3 ) d = [ 0 0 ]; end

d = round( d );

switch  mode 
case 0
   
   l  = max( [1 1], [1 1] + d );
   u  = min( size(im1), d + size(im2) );
   if ( all( l <= u ) )
      im = max( crop( im1, [ l u ], 0 ), crop( im2, [ l-d,  u-d ], 0 ));
   end
   return;

case 1
   
   l  = min( [1 1], [1 1] + d );
   u  = max( size(im1), d + size(im2) );
   im = max( crop( im1, [ l u ], 1 ), crop( im2, [ l-d,  u-d ], 1 ));
   return;

case 2
   
   im = im1;
   l  = max( [1 1], [1 1] + d );
   u  = min( size(im1), d + size(im2) );
   if ( all( l <= u ) )
      im( l(1):u(1), l(2):u(2) ) = max( im( l(1):u(1), l(2):u(2) ),...
         crop( im2, [ l-d,  u-d ], 0 ));
   end
   return;

end

