function [ dis, ang ] = ellipse_indices(dim, quad)

% Calculate signed distance and projected angle of each pixels, with
% respect to an ellipse defined by `q`.
% 
% Syntax:  [ dis, ang ] = ellipse_indices( dim, quad )
%
% Arguments:
%       dim   : size of image ( dim = [height, width] )
%       quad  : 6 component of quadratic form
%               q(1)*X^2 + q(2)*X*Y + q(3)*Y^2 + q(4)*X + q(5)*Y + q(6) = 0
%
% Results:
%       dis   : signed distance to the ellipse
%       ang   : projected angle
%
%   dis and abs are of size (dim)
%
% The origin of the angle is abitrary.
%
% Francois Nedelec, EMBL - June 2014

if nargin < 2
    error('ellipse_indices() requires 2 arguments');
end

if length(dim) ~= 2
    error('First argument should specify 2 dimensions'),
end

if length(quad) ~= 6
    error('Second argument should specify 6 scalar values in horizontal vector');
end


%% Distance

val = quadratic_value(dim, quad);
% Warning: this is not the true distance to the ellipse ...
dis = sqrt(abs(val)) .* sign(val);

%% Angle

pY = ones(dim(1), 1) * (1:dim(2));
pX = (1:dim(1))' * ones(1, dim(2));

[cen, axs, angle] = quadratic_to_ellipse(quad);

%apply inverse rotation:
ca = cos(-angle);
sa = sin(-angle);

rX = ( pX - cen(1) ) * ca - ( pY - cen(2) ) * sa;
rY = ( pX - cen(1) ) * sa + ( pY - cen(2) ) * ca;

%show_image(rX);
%show_image(rY);

ang = atan2( rX/axs(1), rY/axs(2) ) + pi;
%show_image(ang);


end

